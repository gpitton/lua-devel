// Value handles printing for values.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package printer

import (
	rt "bitbucket.org/pcas/golua/runtime"
	"bitbucket.org/pcastools/stringsbuilder"
	"fmt"
	"io"
	"strconv"
	"strings"
	"unicode"
)

// maxStringLen is the maximum number bytes of a string to print before considering truncating.
const maxStringLen = 1024

// nilItem represents the nil value for printing.
type nilItem int

// circularItem represents a circular reference for printing.
type circularItem int

// boolItem represents a bool primitive for printing.
type boolItem bool

// intItem represents an int primitive for printing.
type intItem int64

// floatItem represents a float primitive for printing.
type floatItem float64

// stringItem represents a string primitive for printing.
type stringItem string

// closureItem represents a Closure for printing.
type closureItem string

// emptyItem represents a sequence of empty values for printing.
type emptyItem int64

// truncatedArrayItem represents a truncated array for printing.
type truncatedArrayItem int

// keyItem represents a key primitive for printing.
type keyItem string

// unknownItem represents an unknown value type for printing.
type unknownItem string

// Standard constant values.
const (
	NilItem         = nilItem(0)
	CircularItem    = circularItem(0)
	TrueItem        = boolItem(true)
	FalseItem       = boolItem(false)
	EmptyStringItem = stringItem("")
	EmptyKeyItem    = keyItem("")
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// writeTruncatedBytes writes "... plus N more bytes of output" to b.
func writeTruncatedBytes(b io.StringWriter, N int) (err error) {
	if _, err = b.WriteString("... plus "); err != nil {
		return
	} else if _, err = b.WriteString(strconv.Itoa(N)); err != nil {
		return
	}
	_, err = b.WriteString(" more bytes of output")
	return
}

// writeTruncatedAndEscapedString writes the truncated (if necessary) and escaped string to b. Returns the number of (unescaped) bytes written -- that is, the number of bytes read from s before truncating.
func writeTruncatedAndEscapedString(b *strings.Builder, s string) (int, error) {
	// Are we going to truncate the string?
	truncate := len(s) > maxStringLen+128
	// Start copying over the string, escaping any special characters
	for i, r := range s {
		// Write the next rune
		var err error
		switch r {
		case '\\':
			_, err = b.WriteString("\\f")
		case '\'':
			_, err = b.WriteString("\\'")
		case '\n':
			_, err = b.WriteString("\\n")
		case '\r':
			_, err = b.WriteString("\\r")
		case '\t':
			_, err = b.WriteString("\\t")
		case '\b':
			_, err = b.WriteString("\\b")
		case '\f':
			_, err = b.WriteString("\\f")
		default:
			_, err = b.WriteRune(r)
		}
		// Handle any errors and check if we're done
		if err != nil {
			return 0, err
		} else if truncate && i >= maxStringLen {
			return i + 1, nil
		}
	}
	return len(s), nil
}

// truncateAndEscapeString truncates (if necessary), escapes, and quotes the given string ready for printing. Escapes \, ', \n, \r, \t, \b, and \f in s.
func truncateAndEscapeString(b *strings.Builder, s string) error {
	// Write the opening quote
	if err := b.WriteByte('\''); err != nil {
		return err
	}
	// Start copying over the string, escaping any special characters
	N, err := writeTruncatedAndEscapedString(b, s)
	if err != nil {
		return err
	}
	// If we truncated the string, note this
	if N < len(s) {
		if err := writeTruncatedBytes(b, len(s)-N); err != nil {
			return err
		}
	}
	// Write the closing quote
	return b.WriteByte('\'')
}

/////////////////////////////////////////////////////////////////////////
// nilItem functions
/////////////////////////////////////////////////////////////////////////

// IsInline returns true iff the item will be printed inline.
func (nilItem) IsInline() bool {
	return true
}

// ForceInline forces the item to be printed inline.
func (nilItem) ForceInline() {}

// WriteItem writes the item to w, with initial cursor location cur, using the printer p. Returns the new cursor location.
func (nilItem) WriteItem(w io.Writer, cur int, p *Printer) (int, error) {
	if _, err := p.GetColor(ColorNil).Fprint(w, "nil"); err != nil {
		return 0, err
	}
	return cur + 3, nil
}

// Height returns the maximum depth of an item in the tree, rooted at the given item.
func (nilItem) Height() int {
	return 1
}

// String returns a string representation of the item.
func (nilItem) String() string {
	return "nil"
}

/////////////////////////////////////////////////////////////////////////
// circularItem functions
/////////////////////////////////////////////////////////////////////////

// IsInline returns true iff the item will be printed inline.
func (circularItem) IsInline() bool {
	return true
}

// ForceInline forces the item to be printed inline.
func (circularItem) ForceInline() {}

// WriteItem writes the item to w, with initial cursor location cur, using the printer p. Returns the new cursor location.
func (circularItem) WriteItem(w io.Writer, cur int, p *Printer) (int, error) {
	if _, err := p.GetColor(ColorCircular).Fprint(w, "[Circular]"); err != nil {
		return 0, err
	}
	return cur + 10, nil
}

// Height returns the maximum depth of an item in the tree, rooted at the given item.
func (circularItem) Height() int {
	return 1
}

// String returns a string representation of the item.
func (circularItem) String() string {
	return "[Circular]"
}

/////////////////////////////////////////////////////////////////////////
// boolItem functions
/////////////////////////////////////////////////////////////////////////

// IsInline returns true iff the item will be printed inline.
func (boolItem) IsInline() bool {
	return true
}

// ForceInline forces the item to be printed inline.
func (boolItem) ForceInline() {}

// WriteItem writes the item to w, with initial cursor location cur, using the printer p. Returns the new cursor location.
func (b boolItem) WriteItem(w io.Writer, cur int, p *Printer) (int, error) {
	s := b.String()
	if _, err := p.GetColor(ColorBool).Fprint(w, s); err != nil {
		return 0, err
	}
	return cur + len(s), nil
}

// Height returns the maximum depth of an item in the tree, rooted at the given item.
func (boolItem) Height() int {
	return 1
}

// String returns a string representation of the item.
func (b boolItem) String() string {
	if b {
		return "true"
	}
	return "false"
}

/////////////////////////////////////////////////////////////////////////
// intItem functions
/////////////////////////////////////////////////////////////////////////

// IsInline returns true iff the item will be printed inline.
func (intItem) IsInline() bool {
	return true
}

// ForceInline forces the item to be printed inline.
func (intItem) ForceInline() {}

// WriteItem writes the item to w, with initial cursor location cur, using the printer p. Returns the new cursor location.
func (n intItem) WriteItem(w io.Writer, cur int, p *Printer) (int, error) {
	s := n.String()
	if _, err := p.GetColor(ColorInt).Fprint(w, s); err != nil {
		return 0, err
	}
	return cur + len(s), nil
}

// Height returns the maximum depth of an item in the tree, rooted at the given item.
func (intItem) Height() int {
	return 1
}

// String returns a string representation of the item.
func (n intItem) String() string {
	return strconv.FormatInt(int64(n), 10)
}

/////////////////////////////////////////////////////////////////////////
// floatItem functions
/////////////////////////////////////////////////////////////////////////

// IsInline returns true iff the item will be printed inline.
func (floatItem) IsInline() bool {
	return true
}

// ForceInline forces the item to be printed inline.
func (floatItem) ForceInline() {}

// WriteItem writes the item to w, with initial cursor location cur, using the printer p. Returns the new cursor location.
func (f floatItem) WriteItem(w io.Writer, cur int, p *Printer) (int, error) {
	s := f.String()
	if _, err := p.GetColor(ColorFloat).Fprint(w, s); err != nil {
		return 0, err
	}
	return cur + len(s), nil
}

// Height returns the maximum depth of an item in the tree, rooted at the given item.
func (floatItem) Height() int {
	return 1
}

// String returns a string representation of the item.
func (f floatItem) String() string {
	return strconv.FormatFloat(float64(f), 'g', -1, 64)
}

/////////////////////////////////////////////////////////////////////////
// stringItem functions
/////////////////////////////////////////////////////////////////////////

// IsInline returns true iff the item will be printed inline.
func (stringItem) IsInline() bool {
	return true
}

// ForceInline forces the item to be printed inline.
func (stringItem) ForceInline() {}

// WriteItem writes the item to w, with initial cursor location cur, using the printer p. Returns the new cursor location.
func (s stringItem) WriteItem(w io.Writer, cur int, p *Printer) (int, error) {
	str := s.String()
	if _, err := p.GetColor(ColorString).Fprint(w, str); err != nil {
		return 0, err
	}
	return cur + len(str), nil
}

// Height returns the maximum depth of an item in the tree, rooted at the given item.
func (stringItem) Height() int {
	return 1
}

// String returns a string representation of the item.
func (s stringItem) String() string {
	if len(s) == 0 {
		return "''"
	}
	b := stringsbuilder.New()
	defer stringsbuilder.Reuse(b)
	truncateAndEscapeString(b, string(s)) // Ignore any error
	return b.String()
}

/////////////////////////////////////////////////////////////////////////
// closureItem functions
/////////////////////////////////////////////////////////////////////////

// IsInline returns true iff the item will be printed inline.
func (closureItem) IsInline() bool {
	return true
}

// ForceInline forces the item to be printed inline.
func (closureItem) ForceInline() {}

// WriteItem writes the item to w, with initial cursor location cur, using the printer p. Returns the new cursor location.
func (s closureItem) WriteItem(w io.Writer, cur int, p *Printer) (int, error) {
	if _, err := p.GetColor(ColorClosure).Fprint(w, string(s)); err != nil {
		return 0, err
	}
	return cur + len(s), nil
}

// Height returns the maximum depth of an item in the tree, rooted at the given item.
func (closureItem) Height() int {
	return 1
}

// String returns a string representation of the item.
func (s closureItem) String() string {
	return string(s)
}

/////////////////////////////////////////////////////////////////////////
// keyItem functions
/////////////////////////////////////////////////////////////////////////

// IsInline returns true iff the item will be printed inline.
func (keyItem) IsInline() bool {
	return true
}

// ForceInline forces the item to be printed inline.
func (keyItem) ForceInline() {}

// WriteItem writes the item to w, with initial cursor location cur, using the printer p. Returns the new cursor location.
func (s keyItem) WriteItem(w io.Writer, cur int, p *Printer) (int, error) {
	str := string(s)
	var c Color
	if len(str) == 0 || !unicode.IsLetter([]rune(str)[0]) {
		b := stringsbuilder.New()
		if err := truncateAndEscapeString(b, str); err != nil {
			return 0, err
		}
		str = b.String()
		stringsbuilder.Reuse(b)
		c = ColorString
	}
	if _, err := p.GetColor(c).Fprint(w, str); err != nil {
		return 0, err
	}
	return cur + len(str), nil
}

// Height returns the maximum depth of an item in the tree, rooted at the given item.
func (keyItem) Height() int {
	return 1
}

// String returns a string representation of the item.
func (s keyItem) String() string {
	if len(s) == 0 {
		return "''"
	}
	return string(s)
}

/////////////////////////////////////////////////////////////////////////
// emptyItem functions
/////////////////////////////////////////////////////////////////////////

// IsInline returns true iff the item will be printed inline.
func (emptyItem) IsInline() bool {
	return true
}

// ForceInline forces the item to be printed inline.
func (emptyItem) ForceInline() {}

// WriteItem writes the item to w, with initial cursor location cur, using the printer p. Returns the new cursor location.
func (n emptyItem) WriteItem(w io.Writer, cur int, p *Printer) (int, error) {
	s := n.String()
	if _, err := p.GetColor(ColorEmptyItem).Fprint(w, s); err != nil {
		return 0, err
	}
	return cur + len(s), nil
}

// Height returns the maximum depth of an item in the tree, rooted at the given item.
func (emptyItem) Height() int {
	return 1
}

// String returns a string representation of the item.
func (n emptyItem) String() string {
	if n == 1 {
		return "<1 empty item>"
	}
	return "<" + strconv.FormatInt(int64(n), 10) + " empty items>"
}

/////////////////////////////////////////////////////////////////////////
// truncatedArrayItem functions
/////////////////////////////////////////////////////////////////////////

// IsInline returns true iff the item will be printed inline.
func (truncatedArrayItem) IsInline() bool {
	return true
}

// ForceInline forces the item to be printed inline.
func (truncatedArrayItem) ForceInline() {}

// WriteItem writes the item to w, with initial cursor location cur, using the printer p. Returns the new cursor location.
func (n truncatedArrayItem) WriteItem(w io.Writer, cur int, p *Printer) (int, error) {
	s := n.String()
	if _, err := p.GetColor(ColorTruncatedArray).Fprint(w, s); err != nil {
		return 0, err
	}
	return cur + len(s), nil
}

// Height returns the maximum depth of an item in the tree, rooted at the given item.
func (truncatedArrayItem) Height() int {
	return 1
}

// String returns a string representation of the item.
func (n truncatedArrayItem) String() string {
	if n == 1 {
		return "... 1 more item"
	}
	return "... " + strconv.Itoa(int(n)) + " more items"
}

/////////////////////////////////////////////////////////////////////////
// unknownItem functions
/////////////////////////////////////////////////////////////////////////

// newUnknownItem returns an Item for printing a value of unknown type. This is a placeholder and should never happen. If you encounter an unknown type, add support for it.
func newUnknownItem(v rt.Value) Item {
	return unknownItem(fmt.Sprintf("unknown(%+v)", v))
}

// IsInline returns true iff the item will be printed inline.
func (unknownItem) IsInline() bool {
	return true
}

// ForceInline forces the item to be printed inline.
func (unknownItem) ForceInline() {}

// WriteItem writes the item to w, with initial cursor location cur, using the printer p. Returns the new cursor location.
func (v unknownItem) WriteItem(w io.Writer, cur int, p *Printer) (int, error) {
	if _, err := p.GetColor(ColorUnknown).Fprint(w, string(v)); err != nil {
		return 0, err
	}
	return cur + len(v), nil
}

// Height returns the maximum depth of an item in the tree, rooted at the given item.
func (unknownItem) Height() int {
	return 1
}

// String returns a string representation of the item.
func (v unknownItem) String() string {
	return string(v)
}
