// Printer provides a nice printer for the REPL.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package printer

import (
	rt "bitbucket.org/pcas/golua/runtime"
	"bitbucket.org/pcastools/bytesbuffer"
	"fmt"
	"github.com/fatih/color"
	"io"
	"strconv"
	"strings"
)

// Color is used to represent a colour constant.
type Color int

// The valid colours.
const (
	ColorNone = Color(iota)
	ColorBold
	ColorUnderline
	ColorError
	ColorNil
	ColorInt
	ColorFloat
	ColorBool
	ColorString
	ColorTable
	ColorClosure
	ColorThread
	ColorUserData
	ColorCircular
	ColorEmptyItem
	ColorTruncatedArray
	ColorUnknown
)

// NoColor is the default lack of a color returned by getColor. Treat this value
// as a constant.
var NoColor = color.New()

// Item is the interface satisfied by an object that can be pretty-printed.
type Item interface {
	fmt.Stringer
	// IsInline returns true iff the item will be printed inline.
	IsInline() bool
	// ForceInline forces the item to be printed inline.
	ForceInline()
	// WriteItem writes the item to w, with initial cursor location cur, using the printer p. Returns the new cursor location.
	WriteItem(w io.Writer, cur int, p *Printer) (int, error)
	// Height returns the maximum depth of an item in the tree, rooted at the given item.
	Height() int
}

// Printer provides a way to print values to a io.Writer.
type Printer struct {
	vm      *rt.Runtime            // The underlying vm
	cs      map[Color]*color.Color // Map of colour classes to colours
	width   int                    // The target display width
	depth   int                    // The recursion depth
	indent  int                    // The indentation counter
	visited []rt.Value             // The visited stack
}

// temporaryer is the interface satisfied by an object with a Temporary method.
type temporaryer interface {
	Temporary() bool
}

// printerError represents a common printer error.
type printerError int

// ErrRetryNoInline indicates that printing failed because an object was printed inline and failed to fit. This is a temporary error: printing should be retried until no more ErrRetryNoInline are returned.
const ErrRetryNoInline = printerError(0)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// createDefaults returns thew default colours set.
func createDefaults() map[Color]*color.Color {
	// Create the default colour set
	cs := map[Color]*color.Color{
		ColorNone:           color.New(),
		ColorBold:           color.New(color.Bold),
		ColorUnderline:      color.New(color.Underline),
		ColorError:          color.New(color.FgRed),
		ColorNil:            color.New(color.Bold),
		ColorInt:            color.New(color.FgBlue),
		ColorFloat:          color.New(color.FgBlue),
		ColorBool:           color.New(color.FgYellow),
		ColorString:         color.New(color.FgGreen),
		ColorTable:          color.New(color.FgMagenta),
		ColorClosure:        color.New(color.FgCyan),
		ColorThread:         color.New(color.FgMagenta),
		ColorUserData:       color.New(color.FgMagenta),
		ColorCircular:       color.New(color.FgCyan),
		ColorEmptyItem:      color.New(color.FgHiBlack),
		ColorTruncatedArray: color.New(),
		ColorUnknown:        color.New(),
	}
	// Should we disable colour by default?
	if color.NoColor {
		for _, c := range cs {
			c.DisableColor()
		}
	}
	// Return the defaults
	return cs
}

// valueToString attempts to convert the given value to a string via the __tostring metamethod.
func valueToString(t *rt.Thread, v rt.Value) (string, bool) {
	next := rt.NewTerminationWith(1, false)
	err, ok := rt.Metacall(t, v, "__tostring", []rt.Value{v}, next)
	if err != nil || !ok {
		return "", false
	}
	s, ok := rt.AsString(next.Get(0))
	if !ok {
		return "", false
	}
	return strings.ReplaceAll(strings.ReplaceAll(string(s), "\n", " "), "\t", " "), true
}

/////////////////////////////////////////////////////////////////////////
// printerError functions
/////////////////////////////////////////////////////////////////////////

// Error returns a string description of the error.
func (e printerError) Error() (s string) {
	switch e {
	case ErrRetryNoInline:
		s = "Inline printing overflowed"
	default:
		s = "Unknown error [" + strconv.Itoa(int(e)) + "]"
	}
	return
}

// Temporary returns true iff this is a temporary error.
func (e printerError) Temporary() bool {
	return e == ErrRetryNoInline
}

/////////////////////////////////////////////////////////////////////////
// Printer functions
/////////////////////////////////////////////////////////////////////////

// New returns a new printer for the given vm.
func New(vm *rt.Runtime) *Printer {
	return &Printer{vm: vm}
}

// MainThread returns the main thread in the runtime.
func (p *Printer) MainThread() *rt.Thread {
	return p.vm.MainThread()
}

// GetScreenWidth returns the target display width (in characters). This will be used for line-wrapping. A width <= 0 indicates an "infinitely" wide target display.
func (p *Printer) GetScreenWidth() int {
	if p == nil {
		return 0
	}
	return p.width
}

// SetScreenWidth sets the target display width (in characters). This will be used for line-wrapping. A width <= 0 indicates an :infinitely" wide target display.
func (p *Printer) SetScreenWidth(w int) {
	if p != nil {
		p.width = w
	}
}

// SetColor enables (true) or disables (false) colourised output.
func (p *Printer) SetColor(enable bool) {
	if p != nil {
		if p.cs == nil {
			p.cs = createDefaults()
		}
		if enable {
			for _, c := range p.cs {
				c.EnableColor()
			}
		} else {
			for _, c := range p.cs {
				c.DisableColor()
			}
		}
	}
}

// GetColor returns the colour for the given class.
func (p *Printer) GetColor(t Color) *color.Color {
	if p != nil {
		if p.cs == nil {
			p.cs = createDefaults()
		}
		if c, ok := p.cs[t]; ok {
			return c
		}
	}
	return NoColor
}

// PushDepth increases the recursion depth. Every call to PushDepth should be balanced by a call to PopDepth. Returns the new depth.
func (p *Printer) PushDepth() int {
	if p != nil {
		p.depth++
	}
	return p.Depth()
}

// PopDepth decreases the depth. Returns the new depth.
func (p *Printer) PopDepth() int {
	if p != nil {
		p.depth--
	}
	return p.Depth()
}

// Depth returns the current depth.
func (p *Printer) Depth() int {
	if p == nil {
		return 0
	}
	return p.depth
}

// PushIndent increases the indentation depth. Every call to PushIndent should be balanced by a call to PopIndent. Returns the new indent.
func (p *Printer) PushIndent() string {
	if p != nil {
		p.indent++
	}
	return p.Indent()
}

// PopIndent decreases the indentation depth. Returns the new indent.
func (p *Printer) PopIndent() string {
	if p != nil {
		p.indent--
	}
	return p.Indent()
}

// Indent returns the current indent.
func (p *Printer) Indent() string {
	if p == nil {
		return ""
	}
	return strings.Repeat("  ", p.indent)
}

// PushVisited pushes the value onto the visited stack. Every call to PushVisited should be balanced by a call to PopVisited.
func (p *Printer) PushVisited(v rt.Value) {
	if p != nil {
		if p.visited == nil {
			p.visited = make([]rt.Value, 0, 5)
		}
		p.visited = append(p.visited, v)
	}
}

// PopVisited pops a value off the visited stack.
func (p *Printer) PopVisited() {
	if p != nil {
		if n := len(p.visited); n != 0 {
			p.visited = p.visited[:n-1]
		}
	}
}

// HaveVisited returns true iff the given value is currently in the visited stack.
func (p *Printer) HaveVisited(v rt.Value) bool {
	if p != nil {
		for _, u := range p.visited {
			if u == v {
				return true
			}
		}
	}
	return false
}

// NewItem returns a new item that prints the given value.
func (p *Printer) NewItem(v rt.Value) Item {
	// Increase the depth
	p.PushDepth()
	defer p.PopDepth()
	// First we handle values that don't allow recursion
	if v == nil {
		return NilItem
	}
	switch x := v.(type) {
	case rt.String:
		return stringItem(string(x))
	case rt.Int:
		return intItem(int64(x))
	case rt.Float:
		return floatItem(float64(x))
	case rt.Bool:
		return boolItem(bool(x))
	case *rt.Closure:
		return closureItem(x.Name())
	case *rt.GoFunction:
		return closureItem(x.Name())
	case *rt.Thread:
		return newThreadItem(x)
	case *rt.LightUserData:
		return newLightUserDataItem(x)
	case *rt.UserData:
		return newUserDataItem(x, p)
	}
	// Check that we haven't already visited this value and push this value
	// onto the visited stack
	if p.HaveVisited(v) {
		return CircularItem
	}
	p.PushVisited(v)
	defer p.PopVisited()
	// Handle the remaining cases
	switch x := v.(type) {
	case *rt.Table:
		return newTableItem(x, p)
	default:
		return newUnknownItem(v)
	}
}

// WriteTo writes the item to w. It returns the number of bytes written and any write error encountered.
func (p *Printer) WriteTo(w io.Writer, x Item) (int64, error) {
	// Create a bytes buffer
	b := bytesbuffer.New()
	defer bytesbuffer.Reuse(b)
	// Print to the buffer
	retry := true
	for retry {
		b.Reset()
		retry = false
		if _, err := x.WriteItem(b, 0, p); err != nil {
			// If the error is temporary we should retry
			if e, ok := err.(temporaryer); ok {
				retry = e.Temporary()
			}
			if !retry {
				return 0, err
			}
		}
	}
	// Write the contents of the buffer to w
	return b.WriteTo(w)
}
