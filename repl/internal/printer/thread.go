// Thread handles printing for threads.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package printer

import (
	rt "bitbucket.org/pcas/golua/runtime"
	"fmt"
	"io"
)

// threadItem represents a Thread for printing.
type threadItem string

/////////////////////////////////////////////////////////////////////////
// threadItem functions
/////////////////////////////////////////////////////////////////////////

// newThreadItem returns a new Item for printing the given Thread.
func newThreadItem(t *rt.Thread) Item {
	var s string
	if t.IsMain() {
		s = "<main thread; status=" + t.Status().String() + ">"
	} else {
		s = fmt.Sprintf("<thread %p; status=%s>", t, t.Status())
	}
	return threadItem(s)
}

// IsInline returns true iff the item will be printed inline.
func (threadItem) IsInline() bool {
	return true
}

// ForceInline forces the item to be printed inline.
func (threadItem) ForceInline() {}

// WriteItem writes the item to w, with initial cursor location cur, using the printer p. Returns the new cursor location.
func (t threadItem) WriteItem(w io.Writer, cur int, p *Printer) (int, error) {
	if _, err := p.GetColor(ColorThread).Fprint(w, string(t)); err != nil {
		return 0, err
	}
	return cur + len(t), nil
}

// Height returns the maximum depth of an item in the tree, rooted at the given item.
func (threadItem) Height() int {
	return 1
}

// String returns a string representation of the item.
func (t threadItem) String() string {
	return string(t)
}
