// Userdata handles printing for user data.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package printer

import (
	rt "bitbucket.org/pcas/golua/runtime"
	"fmt"
	"io"
	"strings"
)

// userDataItem represents UserData and LightUserData for printing.
type userDataItem string

/////////////////////////////////////////////////////////////////////////
// userDataItem functions
/////////////////////////////////////////////////////////////////////////

// newLightUserDataItem returns a new Item for printing the given LightUserData.
func newLightUserDataItem(v *rt.LightUserData) Item {
	s := fmt.Sprintf("%v", v.Data)
	if len(s) > 30 {
		s = s[:27] + "..."
	}
	s = strings.ReplaceAll(strings.ReplaceAll(s, "\n", " "), "\t", " ")
	return userDataItem("<LightUserData: " + s + ">")
}

// newUserDataItem returns a new Item for printing the given UserData.
func newUserDataItem(v *rt.UserData, p *Printer) Item {
	s, ok := valueToString(p.MainThread(), v)
	if !ok {
		s = fmt.Sprintf("%v", v.Value())
		if len(s) > 30 {
			s = s[:27] + "..."
		}
		s = "<UserData: " +
			strings.ReplaceAll(strings.ReplaceAll(s, "\n", " "), "\t", " ") +
			">"
	}
	return userDataItem(s)
}

// IsInline returns true iff the item will be printed inline.
func (userDataItem) IsInline() bool {
	return true
}

// ForceInline forces the item to be printed inline.
func (userDataItem) ForceInline() {}

// WriteItem writes the item to w, with initial cursor location cur, using the printer p. Returns the new cursor location.
func (v userDataItem) WriteItem(w io.Writer, cur int, p *Printer) (int, error) {
	if _, err := p.GetColor(ColorUserData).Fprint(w, string(v)); err != nil {
		return 0, err
	}
	return cur + len(v), nil
}

// Height returns the maximum depth of an item in the tree, rooted at the given item.
func (userDataItem) Height() int {
	return 1
}

// String returns a string representation of the item.
func (v userDataItem) String() string {
	return string(v)
}
