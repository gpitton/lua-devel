// Table handles printing of Table values.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package printer

import (
	rt "bitbucket.org/pcas/golua/runtime"
	"bitbucket.org/pcastools/bytesbuffer"
	"bitbucket.org/pcastools/stringsbuilder"
	"bytes"
	"fmt"
	"io"
	"sort"
)

// KeyValueItem represents a key-value item for printing.
type KeyValueItem struct {
	Key   string // The key
	Value Item   // The item for the value
}

// tablePrintItem represents a table with its own printing metamethod.
type tablePrintItem string

// tableItem represents a table for printing.
type tableItem struct {
	forceNoInline bool   // Force non-inlined printing?
	forceInline   bool   // Force inline printing?
	items         []Item // The items in the table
}

// maxArrayLen is the maximum number of (indexed) items to print in an array.
const maxArrayLen = 100

// maxMapLen is the maximum number of (associative array) items to print in an map.
const maxMapLen = 100

// intKeys represents a slice of values that can be converted to positive integers.
type intKeys []rt.Value

// stringKeys represents a slice of values that should be kept as strings.
type stringKeys []rt.Value

/////////////////////////////////////////////////////////////////////////
// intKeys functions
/////////////////////////////////////////////////////////////////////////

// Len is the number of elements in the collection.
func (S intKeys) Len() int {
	return len(S)
}

// Less reports whether the element with index i should sort before the element with index j.
func (S intKeys) Less(i, j int) bool {
	ni, _ := rt.ToInt(S[i])
	nj, _ := rt.ToInt(S[j])
	return ni < nj
}

// Swap swaps the elements with indexes i and j.
func (S intKeys) Swap(i, j int) {
	S[i], S[j] = S[j], S[i]
}

/////////////////////////////////////////////////////////////////////////
// stringKeys functions
/////////////////////////////////////////////////////////////////////////

// Len is the number of elements in the collection.
func (S stringKeys) Len() int {
	return len(S)
}

// Less reports whether the element with index i should sort before the element with index j.
func (S stringKeys) Less(i, j int) bool {
	ni, _ := rt.AsString(S[i])
	nj, _ := rt.AsString(S[j])
	return ni < nj
}

// Swap swaps the elements with indexes i and j.
func (S stringKeys) Swap(i, j int) {
	S[i], S[j] = S[j], S[i]
}

/////////////////////////////////////////////////////////////////////////
// tablePrintItem functions
/////////////////////////////////////////////////////////////////////////

// IsInline returns true iff the item will be printed inline.
func (tablePrintItem) IsInline() bool {
	return true
}

// ForceInline forces the item to be printed inline.
func (tablePrintItem) ForceInline() {}

// WriteItem writes the item to w, with initial cursor location cur, using the printer p. Returns the new cursor location.
func (t tablePrintItem) WriteItem(w io.Writer, cur int, p *Printer) (int, error) {
	if _, err := p.GetColor(ColorTable).Fprint(w, string(t)); err != nil {
		return 0, err
	}
	return cur + len(t), nil
}

// Height returns the maximum depth of an item in the tree, rooted at the given item.
func (tablePrintItem) Height() int {
	return 1
}

// String returns a string representation of the item.
func (t tablePrintItem) String() string {
	return string(t)
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// partitionToPositiveIntAndStringKeys partitions the keys of the given table to a pair of slices. Any strings that can be converted to a positive int are placed in the first returned slice; otherwise they are placed in the second returned slice. The slices will be sorted.
func partitionToPositiveIntAndStringKeys(t *rt.Table) (intKeys, stringKeys) {
	T, U := make(intKeys, 0), make(stringKeys, 0)
	k, _, _ := t.Next(nil)
	for k != nil {
		if n, ok := rt.ToInt(k); ok && n > 0 {
			T = append(T, k)
		} else {
			U = append(U, k)
		}
		k, _, _ = t.Next(k)
	}
	sort.Sort(T)
	sort.Sort(U)
	return T, U
}

// objectToItemSlice returns a slice of items given by converting the elements of t with keys T. May truncate the returned slice.
func objectToItemSlice(t *rt.Table, T intKeys, p *Printer) []Item {
	// Are we going to truncate T?
	origT := len(T)
	if origT > maxArrayLen+1 {
		T = T[:maxArrayLen]
	}
	// Convert the elements in T to items
	items := make([]Item, 0, len(T))
	first := true
	lastIdx := int64(1)
	for _, k := range T {
		n, _ := rt.ToInt(k)
		i := int64(n)
		if first {
			first = false
		} else if delta := i - lastIdx - 1; delta != 0 {
			// There are some empty items in the array
			items = append(items, emptyItem(delta))
		}
		lastIdx = i
		// Create the corresponding item
		items = append(items, p.NewItem(t.Get(k)))
	}
	// Note if the array was truncated
	if delta := origT - len(T); delta > 0 {
		items = append(items, truncatedArrayItem(delta))
	}
	// Return the slice
	return items
}

// objectToKeyValueSlice returns a slice of key-value items given by converting the elements of t with keys U. May truncate the returned slice.
func objectToKeyValueSlice(t *rt.Table, U stringKeys, p *Printer) []Item {
	// Are we going to truncate U?
	origU := len(U)
	if origU > maxMapLen+1 {
		U = U[:maxMapLen]
	}
	// Convert the elements in U to key-value items
	items := make([]Item, 0, len(U))
	for _, k := range U {
		s, _ := rt.AsString(k)
		// Create the corresponding item
		items = append(items, &KeyValueItem{
			Key:   string(s),
			Value: p.NewItem(t.Get(k)),
		})
	}
	// Note if the result was truncated
	if delta := origU - len(U); delta > 0 {
		items = append(items, truncatedArrayItem(delta))
	}
	// Return the slice
	return items
}

// printItemsInlineList prints the given items to b in forced inline format.
func printItemsInlineList(b *bytes.Buffer, items []Item, cur int, p *Printer) (int, error) {
	// We begin by outputting a space
	b.WriteByte(' ')
	cur++
	// Start outputting the items
	last := len(items) - 1
	for i, v := range items {
		// Print the item
		var err error
		cur, err = v.WriteItem(b, cur, p)
		if err != nil {
			return 0, err
		}
		// If this isn't the final item, output a comma and a space
		if i != last {
			b.WriteByte(',')
			b.WriteByte(' ')
			cur += 2
		}
	}
	// Return success
	return cur, nil
}

// printItemsIndentedList prints the given items to b, one item per line as in display format.
func printItemsIndentedList(b *bytes.Buffer, items []Item, p *Printer) error {
	// Create a bytes buffer
	vb := bytesbuffer.New()
	defer bytesbuffer.Reuse(vb)
	// Push an indent onto the stack
	indent := p.PushIndent()
	defer p.PopIndent()
	// We begin by outputting a new line
	b.WriteByte('\n')
	b.WriteString(indent)
	cur := len(indent)
	// Start outputting the items
	last := len(items) - 1
	for i, v := range items {
		// Print the item to the buffer vb
		newcur, err := v.WriteItem(vb, cur, p)
		if err != nil {
			// Whilst the error is an ErrRetryNoInline, retry
			for err == ErrRetryNoInline {
				vb.Reset()
				newcur, err = v.WriteItem(vb, cur, p)
			}
			// If we still have an error, give up
			if err != nil {
				return err
			}
		}
		// Write the contents of vb to b
		if _, err := vb.WriteTo(b); err != nil {
			return err
		}
		// If this isn't the final item, output a comma and start a new line
		if i != last {
			b.WriteByte(',')
			b.WriteByte('\n')
			b.WriteString(indent)
			cur = len(indent)
		} else {
			cur = newcur
		}
	}
	// Return success
	return nil
}

/////////////////////////////////////////////////////////////////////////
// KeyValueItem functions
/////////////////////////////////////////////////////////////////////////

// IsInline returns true iff the item will be printed inline.
func (v *KeyValueItem) IsInline() bool {
	return v.GetValue().IsInline()
}

// ForceInline forces the item to be printed inline.
func (v *KeyValueItem) ForceInline() {
	v.GetValue().ForceInline()
}

// WriteItem writes the item to w, with initial cursor location cur, using the printer p. Returns the new cursor location.
func (v *KeyValueItem) WriteItem(w io.Writer, cur int, p *Printer) (int, error) {
	var err error
	if cur, err = v.GetKey().WriteItem(w, cur, p); err != nil {
		return 0, err
	} else if _, err = fmt.Fprint(w, "="); err != nil {
		return 0, err
	}
	return v.GetValue().WriteItem(w, cur+1, p)
}

// Height returns the maximum depth of an item in the tree, rooted at the given item.
func (v *KeyValueItem) Height() int {
	hk, hv := v.GetKey().Height(), v.GetValue().Height()
	if hk > hv {
		return hk
	}
	return hv
}

// GetKey returns the key for this key-value item, as an item.
func (v *KeyValueItem) GetKey() Item {
	if v == nil || len(v.Key) == 0 {
		return EmptyKeyItem
	}
	return keyItem(v.Key)
}

// GetValue returns the value for this key-value item.
func (v *KeyValueItem) GetValue() Item {
	if v == nil || v.Value == nil {
		return NilItem
	}
	return v.Value
}

// String returns a string representation of the item.
func (v *KeyValueItem) String() string {
	b := stringsbuilder.New()
	defer stringsbuilder.Reuse(b)
	b.WriteString(v.GetKey().String())
	b.WriteString(": ")
	b.WriteString(v.GetValue().String())
	return b.String()
}

/////////////////////////////////////////////////////////////////////////
// tableItem functions
/////////////////////////////////////////////////////////////////////////

// newTableItem returns a printer for the given table.
func newTableItem(t *rt.Table, p *Printer) Item {
	// Does the table provide its own printing? If so, we use that.
	if s, ok := valueToString(p.MainThread(), t); ok {
		return tablePrintItem(s)
	}
	// Partition the keys
	T, U := partitionToPositiveIntAndStringKeys(t)
	// Create the corresponding slices of items
	Titems := objectToItemSlice(t, T, p)
	Uitems := objectToKeyValueSlice(t, U, p)
	// Combine the items into a single slice
	items := make([]Item, len(Titems)+len(Uitems))
	copy(items, Titems)
	copy(items[len(Titems):], Uitems)
	// Return the table
	return &tableItem{
		items: items,
	}
}

// IsInline returns true iff the item will be printed inline.
func (c *tableItem) IsInline() bool {
	// Get the easy cases out of the way
	if c.IsEmpty() || c.forceInline {
		return true
	} else if c.forceNoInline || c.Height() > 2 {
		return false
	}
	// Do any of the items have a preference?
	for _, v := range c.items {
		if !v.IsInline() {
			return false
		}
	}
	// We default to inline printing
	return true
}

// ForceInline forces the item to be printed inline.
func (c *tableItem) ForceInline() {
	c.forceInline, c.forceNoInline = true, false
	for _, v := range c.items {
		v.ForceInline()
	}
}

// WriteItem writes the item to w, with initial cursor location cur, using the printer p. Returns the new cursor location.
func (c *tableItem) WriteItem(w io.Writer, cur int, p *Printer) (int, error) {
	// Create a bytes buffer
	b := bytesbuffer.New()
	defer bytesbuffer.Reuse(b)
	// Print the items to the bytes buffer
	var err error
	if c.IsInline() {
		if c.forceInline {
			cur, err = c.writeForceInline(b, cur, p)
		} else {
			cur, err = c.writeInline(b, cur, p)
		}
	} else {
		cur, err = c.writeDisplay(b, cur, p)
	}
	// Handle any errors and write the contents of the buffer to w
	if err != nil {
		// If necessary, force no inlining on the next attempt
		if err == ErrRetryNoInline {
			c.forceNoInline = true
		}
		return 0, err
	} else if _, err = b.WriteTo(w); err != nil {
		return 0, err
	}
	return cur, nil
}

// writeInline writes the table to b in inline format.
func (c *tableItem) writeInline(b *bytes.Buffer, cur int, p *Printer) (int, error) {
	// Make a note of the screen width
	width := p.GetScreenWidth()
	// Write the opening brace
	cur++
	if width > 0 && cur >= width {
		return 0, ErrRetryNoInline
	}
	col := p.GetColor(ColorTable)
	col.Fprint(b, "{")
	// If the table is empty, output the closing byte and return
	if c.IsEmpty() {
		cur++
		if width > 0 && cur >= width {
			return 0, ErrRetryNoInline
		}
		col.Fprint(b, "}")
		return cur, nil
	}
	// Push an indent onto the stack
	indent := p.PushIndent()
	defer p.PopIndent()
	// Create a bytes buffer
	vb := bytesbuffer.New()
	defer bytesbuffer.Reuse(vb)
	// Start outputting the items -- keep track of how many items end up being
	// on a line by themselves
	wasNewLine := false
	singleLine := 0
	last := len(c.items) - 1
	for i, v := range c.items {
		// If this isn't the first item, and if there is less that 3 spaces
		// left on the line, then there's no chance of fitting the item here, so
		// start a new line
		newline := false
		if i != 0 && cur+3 >= width {
			newline = true
			b.WriteByte('\n')
			b.WriteString(indent)
			cur = len(indent)
		} else {
			// Write out a space
			cur++
			vb.WriteByte(' ')
		}
		// First we try printing the item after the current cursor position
		newcur, err := v.WriteItem(vb, cur, p)
		if err != nil {
			return 0, err
		}
		// If this is anything but the final item, we also need to allow for a
		// comma; the last item needs to allow for a space and the closing
		// brace.
		if i == last {
			newcur += 2
		} else {
			newcur++
		}
		// Does this fit on the line?
		if !(width > 0 && newcur >= width) {
			// Write the contents of the buffer to b
			if _, err := vb.WriteTo(b); err != nil {
				return 0, err
			} else if i == last {
				b.WriteByte(' ')
				col.Fprint(b, "}")
			} else {
				b.WriteByte(',')
			}
		} else if newline {
			return 0, ErrRetryNoInline
		} else {
			// Reset the buffer
			vb.Reset()
			// Redo the printing, this time starting on a new line. We can
			// write directly to b this time.
			b.WriteByte('\n')
			b.WriteString(indent)
			if newcur, err = v.WriteItem(b, len(indent), p); err != nil {
				return 0, err
			} else if i == last {
				newcur += 2
				b.WriteByte(' ')
				col.Fprint(b, "}")
			} else {
				newcur++
				b.WriteByte(',')
			}
			// If this still failed to fit on the line, we abandon inline
			// printing
			if width > 0 && cur >= width {
				return 0, ErrRetryNoInline
			}
			newline = true
		}
		// Note if this line has a single item on it
		if wasNewLine && newline {
			singleLine++
		}
		wasNewLine = newline
		// Update the cursor location
		cur = newcur
	}
	// If enough items are on a line by themselves, we should think about
	// whether inline is the best way of presenting this data
	if wasNewLine {
		singleLine++
	}
	if singleLine > 3 && singleLine > len(c.items)/2 {
		return 0, ErrRetryNoInline
	}
	return cur, nil
}

// writeForceInline writes the table to b in forced inline format.
func (c *tableItem) writeForceInline(b *bytes.Buffer, cur int, p *Printer) (int, error) {
	// Write the opening brace
	col := p.GetColor(ColorTable)
	col.Fprint(b, "{")
	cur++
	// If the slice is empty, output the closing byte and return
	if c.IsEmpty() {
		col.Fprint(b, "}")
		return cur + 1, nil
	}
	// Output the items
	var err error
	if cur, err = printItemsInlineList(b, c.items, cur, p); err != nil {
		return 0, err
	}
	// Output the closing space and brace
	b.WriteByte(' ')
	col.Fprint(b, "}")
	return cur + 2, nil
}

// writeDisplay writes the table to b in display format.
func (c *tableItem) writeDisplay(b *bytes.Buffer, cur int, p *Printer) (int, error) {
	// Write the opening brace
	col := p.GetColor(ColorTable)
	col.Fprint(b, "{")
	// If the slice is empty, output the closing byte and return
	if c.IsEmpty() {
		col.Fprint(b, "}")
		return cur + 2, nil
	}
	// Output the items
	if err := printItemsIndentedList(b, c.items, p); err != nil {
		return 0, err
	}
	// Output the closing brace
	indent := p.Indent()
	b.WriteByte('\n')
	b.WriteString(indent)
	col.Fprint(b, "}")
	return len(indent) + 1, nil
}

// Height returns the maximum depth of an item in the tree, rooted at the given item.
func (c *tableItem) Height() int {
	height := 1
	for _, v := range c.items {
		if h := v.Height(); h >= height {
			height = h + 1
		}
	}
	return height
}

// IsEmpty returns true iff the table is empty.
func (c *tableItem) IsEmpty() bool {
	return len(c.items) == 0
}

// String returns a string representation of the table.
func (c *tableItem) String() string {
	// Get the empty table out of the way
	if c.IsEmpty() {
		return "{}"
	}
	// Construct the string
	b := stringsbuilder.New()
	defer stringsbuilder.Reuse(b)
	b.WriteByte('{')
	b.WriteByte(' ')
	first := true
	for _, v := range c.items {
		if first {
			first = false
		} else {
			b.WriteString(", ")
		}
		b.WriteString(v.String())
	}
	b.WriteByte(' ')
	b.WriteByte('}')
	return b.String()
}
