// Autocompleter implements autocomplete for REPL.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package repl

import (
	rt "bitbucket.org/pcas/golua/runtime"
	"context"
	"regexp"
	"sort"
	"strings"
)

// autoCompleter implements autocomplete for readline.
type autoCompleter struct {
	r *Repl // The REPL
}

// lastExpressionRegex is used to match against a sequence of . or : separated objects.
var lastExpressionRegex = regexp.MustCompile(`[_"a-zA-Z0-9]([_"a-zA-Z0-9\.:]*[_"a-zA-Z0-9])?[\.:]?$`)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// keysForSlice returns the matching keys for the given slice. This will exclue any keys that do not begin with 'match', which will be trimmed from the start of each key. The trimmed keys will be returned sorted in lex order.
func keysForSlice(S []string, match string) []string {
	// Collect together the keys
	keys := make([]string, 0, len(S))
	for _, k := range S {
		if strings.HasPrefix(k, match) {
			keys = append(keys, strings.TrimPrefix(k, match))
		}
	}
	// Sort the keys and return
	sort.Strings(keys)
	return keys
}

// keysForMetatable recursively returns the matching keys in __index for the given metatable. This will exclue any keys that do not begin with 'match', which will be trimmed from the start of each key. The trimmed keys will be returned sorted in lex order.
func keysForMetatable(t *rt.Table, match string) []string {
	// Is there anything to do?
	if t == nil {
		return nil
	}
	// Recover the __index and see what we can do
	var keys []string
	if v := t.Get(rt.String("__index")); v != nil {
		switch x := v.(type) {
		case *rt.Table:
			keys = keysForTable(x, match)
		case *rt.UserData:
			keys = keysForMetatable(x.Metatable(), match)
		}
	}
	return keys
}

// keysForTable returns the matching keys for the given table. This will exclue any keys that do not begin with 'match', which will be trimmed from the start of each key. The trimmed keys will be returned sorted in lex order.
func keysForTable(t *rt.Table, match string) []string {
	// Collect together the keys
	keys := make([]string, 0)
	k, _, _ := t.Next(nil)
	for k != nil {
		s, _ := rt.AsString(k)
		str := string(s)
		if strings.HasPrefix(str, match) {
			keys = append(keys, strings.TrimPrefix(str, match))
		}
		k, _, _ = t.Next(k)
	}
	// Add any relevant keys from the metatable
	keys = append(keys, keysForMetatable(t.Metatable(), match)...)
	// Sort the keys and return
	sort.Strings(keys)
	return keys
}

/////////////////////////////////////////////////////////////////////////
// autoCompleter functions
/////////////////////////////////////////////////////////////////////////

// matchGlobalSymbols matches line against the global symbols.
func (a *autoCompleter) matchGlobalSymbols(line string) ([][]rune, int) {
	// Collect the matching keys
	keys := keysForTable(a.r.vm.GlobalEnv(), line)
	// Convert the keys to a slice of slices of runes and return
	res := make([][]rune, 0, len(keys))
	for _, k := range keys {
		res = append(res, []rune(k))
	}
	return res, len(line)
}

// matchValue matches against the keys of the value with given name.
func (a *autoCompleter) matchValue(name string, last string) ([][]rune, int) {
	// Attempt to recover the value referred to by the name
	clos, err := a.r.compile([]string{"return " + name})
	if err != nil {
		return nil, 0
	}
	values, err := a.r.runClosure(context.Background(), clos)
	if err != nil || len(values) == 0 {
		return nil, 0
	}
	// Collect the matching keys
	var keys []string
	switch x := values[0].(type) {
	case *rt.Table:
		keys = keysForTable(x, last)
	case *rt.UserData:
		keys = keysForMetatable(x.Metatable(), last)
	}
	// Convert the keys to a slice of slices of runes and return
	res := make([][]rune, 0, len(keys))
	for _, k := range keys {
		res = append(res, []rune(k))
	}
	return res, len(last)
}

// Do returns the possible matches to the current line. Along with a slice of matching candidates, also returns how long they shared the same characters in line.
func (a *autoCompleter) Do(line []rune, pos int) ([][]rune, int) {
	// Trim the line to the current position
	s := string(line[:pos])
	// Get the special case of line beginning with a . out of the way
	if len(s) != 0 && s[0] == '.' && !strings.Contains(s, " ") {
		// Collect the keys
		keys := keysForSlice(a.r.Registry().Names(), s[1:])
		// Convert the keys to a slice of slices of runes and return
		res := make([][]rune, 0, len(keys))
		for _, k := range keys {
			res = append(res, []rune(k))
		}
		return res, len(s)
	}
	// Trim the line to the current position and extract the last expression
	lastExpression := lastExpressionRegex.FindString(s)
	// Find the last index of the dot or colon
	idx := strings.LastIndexAny(lastExpression, ".:")
	if idx == -1 {
		return a.matchGlobalSymbols(lastExpression)
	}
	// Match against the value given by the part up to the final dot
	return a.matchValue(lastExpression[:idx], lastExpression[idx+1:])
}
