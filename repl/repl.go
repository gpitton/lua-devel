// Repl handles the read-evaluate-print loop.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.

You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package repl

import (
	"bitbucket.org/pcas/golua/module"
	rt "bitbucket.org/pcas/golua/runtime"
	"bitbucket.org/pcas/golua/token"
	"context"
	"fmt"
	"github.com/chzyer/readline"
	"github.com/juju/ansiterm/tabwriter"
	"github.com/pkg/errors"
	"io"
	"os"
	"os/signal"
	"path/filepath"
	"strings"
	"time"
)

// The default prompts
const (
	DefaultPrompt         = "> "
	DefaultBufferedPrompt = ">> "
)

// Repl is a REPL Lua interpreter.
type Repl struct {
	vm             *rt.Runtime        // The Lua runtime
	rl             *readline.Instance // The readline instance
	reg            Registry           // The dot commands
	p              *Printer           // The printer
	d              time.Duration      // The REPL runtime
	prompt         string             // The prompt
	bufferedPrompt string             // The buffered prompt
	buf            []string           // The line buffer
	history        []string           // The session history
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// createConfig returns the basic configuration settings for readline.
func createConfig(stdout io.Writer) *readline.Config {
	// Build the path to the history file
	homeDir := os.Getenv("HOME")
	if homeDir == "" {
		homeDir, _ = filepath.Abs(".")
	}
	historyFileName := "." + filepath.Base(os.Args[0]) + "_history"
	historyFile := filepath.Join(homeDir, historyFileName)
	// Return the config
	return &readline.Config{
		HistoryFile:     historyFile,
		InterruptPrompt: "\n",
		EOFPrompt:       "\n",
		Stdout:          stdout,
	}
}

// registerDotCommands registers the standard dot commands with the REPL.
func registerDotCommands(r *Repl) {
	reg := r.Registry()
	reg.Register( // break
		"break",
		"Break out of the current buffer.",
		func(l string) error {
			r.ClearBuffer()
			return nil
		},
	)
	reg.Register( // clear
		"clear",
		"Break out of the current buffer and clear the screen.",
		func(l string) error {
			r.ClearBuffer()
			_, err := readline.ClearScreen(r.rl)
			return err
		},
	)
	reg.Register( // walltime
		"walltime",
		"Output the total walltime for this session.",
		func(l string) error {
			t := r.Runtime().Truncate(time.Millisecond)
			fmt.Fprintln(r.Stdout(), t)
			return nil
		},
	)
	reg.Register( // history
		"history",
		"Output the history for this session.",
		func(l string) error {
			return printHistory(r)
		},
	)
	reg.Register( // exit
		"exit",
		"Exit.",
		func(l string) error {
			return io.EOF
		},
	)
	reg.Register( // help
		"help",
		"Display this help text.",
		func(l string) error {
			err := printDotCommandsHelp(r)
			if err == nil {
				err = printBuiltInModulesHelp(r)
			}
			return err
		},
	)
}

// printDotCommandsHelp prints the help for the dot-commands.
func printDotCommandsHelp(r *Repl) error {
	// Make a note of the registry and printer
	reg := r.Registry()
	p := r.Printer()
	// Output the general help text
	if _, err := fmt.Fprintf(
		r.Stdout(),
		"%s\n%s%s\n\n%s\n%s\n\n",
		p.WrapString("For general help with Lua see the Lua 5.3 reference manual:"),
		strings.Repeat(" ", 14),
		p.Underline("https://www.lua.org/manual/5.3/"),
		p.WrapString("Interrupt long computations by pressing ^C."),
		p.WrapString("Reverse-search history by pressing ^R, forward-search history by pressing ^S."),
	); err != nil {
		return err
	}
	// Output the list of dot-commands
	tw := tabwriter.NewWriter(r.Stdout(), 14, 1, 2, ' ', 0)
	for _, name := range reg.Names() {
		if c, ok := reg.Get(name); ok {
			// Create the name (special case some names)
			s := p.Bold("." + name)
			switch name {
			case "break":
				s += " or ^C"
			case "exit":
				s += " or ^D"
			}
			// Output the name and description
			_, err := fmt.Fprintf(tw, "%s\t%s\n", s, c.Description())
			if err != nil {
				return err
			}
		}
	}
	return tw.Flush()
}

// printModuleInfo prints the help text for the given module.
func printModuleInfo(w io.Writer, info module.Info, p *Printer) error {
	_, err := fmt.Fprintf(w, "%s\t%s\n", p.Bold(info.Name), info.Description)
	if len(info.URL) != 0 && err == nil {
		_, err = fmt.Fprintf(w, "\t%s\n", p.Underline(info.URL))
	}
	return err
}

// printBuiltInModulesHelp prints the help for the built-in modules.
func printBuiltInModulesHelp(r *Repl) error {
	// Fetch the registered modules
	ms := module.Registered()
	// Make a note of the printer
	p := r.Printer()
	// Output the heading
	if _, err := fmt.Fprintf(r.Stdout(), "\n%s\n\n", p.WrapString("The following standard packages are preloaded:")); err != nil {
		return err
	}
	// First output the list of preloaded modules
	tw := tabwriter.NewWriter(r.Stdout(), 14, 1, 2, ' ', 0)
	for _, info := range ms {
		if info.Preload {
			if err := printModuleInfo(tw, info, p); err != nil {
				return err
			}
		}
	}
	if err := tw.Flush(); err != nil {
		return err
	}
	// Output the heading
	if _, err := fmt.Fprintf(r.Stdout(), "\n%s\n\n", strings.Replace(p.WrapString("The following built-in packages are available via require:"), "require", p.Bold("require"), 1)); err != nil {
		return err
	}
	// Output the list of non-preloaded modules
	for _, info := range ms {
		if !info.Preload {
			if err := printModuleInfo(tw, info, p); err != nil {
				return err
			}
		}
	}
	return tw.Flush()
}

// printHistory prints the history for this session.
func printHistory(r *Repl) error {
	w := r.Stdout()
	for _, s := range r.History() {
		if _, err := fmt.Fprintln(w, s); err != nil {
			return err
		}
	}
	return nil
}

// generateName returns a name for the given block. This name will appear in stack traces, so has to be informative.
func generateName(b []string) string {
	n := len(b)
	if n == 1 {
		s := strings.TrimSpace(b[0])
		if len(s) > 40 {
			delta := len(s) - 40
			s = s[:len(s)-delta-5] + "..." + s[len(s)-5:]
		}
		return "'" + s + "'"
	}
	start := strings.TrimSpace(b[0])
	finish := strings.TrimSpace(b[n-1])
	if m := len(start) + len(finish); m > 40 {
		delta := m - 40
		if len(finish) > delta {
			finish = finish[delta:]
		} else {
			delta -= len(finish) - 1
			finish = finish[len(finish)-1:]
			start = start[:len(start)-delta]
		}
	}
	return "'" + start + "..." + finish + "'"
}

// ignoreTokenError returns true iff the error with given token type should be ignored during REPL line parsing.
func ignoreTokenError(t token.Type) bool {
	switch t {
	case token.EOF, token.KwIf:
		return true
	default:
		return false
	}
}

/////////////////////////////////////////////////////////////////////////
// Repl functions
/////////////////////////////////////////////////////////////////////////

// NewREPL returns a new Lua REPL wrapping vm and writing to the given io.Writer.
func NewREPL(vm *rt.Runtime, w io.Writer) (*Repl, error) {
	// Create the REPL
	r := &Repl{
		vm:             vm,
		p:              NewPrinter(vm),
		prompt:         DefaultPrompt,
		bufferedPrompt: DefaultBufferedPrompt,
	}
	// Create the readline config
	cfg := createConfig(w)
	cfg.Prompt = DefaultPrompt
	cfg.AutoComplete = &autoCompleter{r: r}
	// Create and set the readline instance
	rl, err := readline.NewEx(cfg)
	if err != nil {
		return nil, err
	}
	r.rl = rl
	// Register the default dot commands
	registerDotCommands(r)
	// Set the initial screen width and whether to colourise output
	if r.IsTerminal() {
		if p := r.Printer(); p != nil {
			p.SetScreenWidth(readline.GetScreenWidth())
		}
		r.SetColor(true)
		// Update the screen width as necessary
		readline.DefaultOnWidthChanged(func() {
			if p := r.Printer(); p != nil {
				p.SetScreenWidth(readline.GetScreenWidth())
			}
		})
	} else {
		if p := r.Printer(); p != nil {
			p.SetScreenWidth(0)
		}
		r.SetColor(false)
	}
	// Return the REPL
	return r, nil
}

// Printer returns the printer for this REPL.
func (r *Repl) Printer() *Printer {
	if r == nil {
		return nil
	}
	return r.p
}

// Registry returns the dot-command registry for this REPL.
func (r *Repl) Registry() *Registry {
	if r == nil {
		return nil
	}
	return &r.reg
}

// Runtime returns the total runtime for this REPL.
func (r *Repl) Runtime() time.Duration {
	if r == nil {
		return 0
	}
	return r.d
}

// Close closes the REPL, preventing further user interaction.
func (r *Repl) Close() error {
	if r == nil {
		return nil
	}
	return r.rl.Close()
}

// SetColor enables (true) or disables (false) colourised output.
func (r *Repl) SetColor(enable bool) {
	if p := r.Printer(); p != nil {
		p.SetColor(enable)
	}
}

// Stdout returns the stdout.
func (r *Repl) Stdout() io.Writer {
	if r == nil {
		return os.Stdout
	}
	return r.rl.Stdout()
}

// IsTerminal returns true iff standard output is a terminal.
func (r *Repl) IsTerminal() bool {
	return r != nil && readline.DefaultIsTerminal()
}

// PrintlnError prints an error to standard output. A newline is appended to the output. It returns the number of bytes written and any write error encountered.
func (r *Repl) PrintlnError(err error) (int, error) {
	n, err := r.Printer().FprintError(r.Stdout(), err)
	if err == nil {
		var m int
		m, err = fmt.Fprint(r.Stdout(), "\n")
		n += m
	}
	return n, err
}

// printlnValue prints the values to standard output. A newline is appended to the output. It returns the number of bytes written and any write error encountered.
func (r *Repl) printlnValue(value ...rt.Value) (int, error) {
	n, err := r.Printer().FprintValue(r.Stdout(), value...)
	if err == nil {
		var m int
		m, err = fmt.Fprint(r.Stdout(), "\n")
		n += m
	}
	return n, err
}

// History returns the history for this session.
func (r *Repl) History() []string {
	if r == nil {
		return nil
	}
	return r.history
}

// ClearBuffer clears any buffered lines of user input and updates the prompt.
func (r *Repl) ClearBuffer() {
	if r != nil && len(r.buf) != 0 {
		r.buf = nil
		r.rl.SetPrompt(r.prompt)
	}
}

// compile pares, compiles and loads a Lua chunk from source and returns the closure.
func (r *Repl) compile(source []string) (*rt.Closure, error) {
	// Make a note of the global environment and generate a name for the closure
	env := r.vm.GlobalEnv()
	name := generateName(source)
	// Generate the source
	src := []byte(strings.Join(source, "\n")) // TODO
	// This is a trick to be able to evaluate lua expressions in the repl
	clos, err := rt.CompileAndLoadLuaChunk(name, append([]byte("\nreturn "), src...), env)
	if err == nil {
		return clos, nil
	}
	// Try again but without appending the return
	return rt.CompileAndLoadLuaChunk(name, src, env)
}

// runClosure runs the given closure, returning the resulting values and any error.
func (r *Repl) runClosure(ctx context.Context, clos *rt.Closure) ([]rt.Value, error) {
	// Make a note of the main thread
	t := r.vm.MainThread()
	// We install a temporary signal handler to trigger an interrupt on ^C
	c, doneC := make(chan os.Signal, 1), make(chan struct{})
	signal.Notify(c, os.Interrupt)
	defer func() {
		signal.Stop(c)
		close(c)
		<-doneC
		rt.ClearInterrupt(t.Runtime)
	}()
	// Watch for an interrupt or for the context to fire
	go func() {
		defer close(doneC)
		ok := true
		select {
		case _, ok = <-c:
		case <-ctx.Done():
		}
		if ok {
			rt.Interrupt(t.Runtime)
		}
	}()
	// Evaluate the closure
	term := rt.NewTerminationWith(0, true)
	tt := time.Now()
	err := rt.Call(t, clos, nil, term)
	r.d += time.Since(tt)
	if err != nil {
		return nil, err
	}
	return term.Etc(), nil
}

// Readline reads the next line of user input. An io.EOF will be returned if the REPL should exit.
func (r *Repl) Readline() (string, error) {
	// Sanity check
	if r == nil {
		return "", errors.New("uninitialised REPL")
	}
	// Start reading
	for {
		// Read the next line of user input
		if l, err := r.rl.Readline(); err != nil {
			// Return any error other than an interrupt
			if err != readline.ErrInterrupt {
				return "", err
			}
			// Clear the buffer
			r.ClearBuffer()
		} else if len(l) != 0 {
			return l, nil
		}
	}
}

// Next handles the next line for the REPL.
func (r *Repl) Next(ctx context.Context) error {
	// Read the next line of input
	l, err := r.Readline()
	if err != nil {
		return err
	}
	// Handle any dot commands
	if ok, err := r.Registry().Run(l); err != nil || ok {
		return err
	}
	// Append the line to the line buffer
	inBuffer := len(r.buf) != 0
	r.buf = append(r.buf, l)
	// Does the line buffer compile?
	clos, err := r.compile(r.buf)
	if err != nil {
		// We ignore certain errors
		if e, ok := err.(*rt.SyntaxError); ok && !ignoreTokenError(e.Err.Got.Type) {
			r.ClearBuffer()
			r.PrintlnError(err) // Ignore any error
		} else if !inBuffer {
			r.rl.SetPrompt(r.bufferedPrompt)
		}
		return nil
	}
	// The buffer compiles -- add the buffer to the history, reset the prompt,
	// and clear the buffer
	r.history = append(r.history, r.buf...)
	if inBuffer {
		r.rl.SetPrompt(r.prompt)
	}
	r.buf = nil
	// Evaluate the code and output the result
	var returnerr error
	if values, err := r.runClosure(ctx, clos); err != nil {
		_, returnerr = r.PrintlnError(err)
	} else if len(values) != 0 {
		_, returnerr = r.printlnValue(values...)
	}
	return returnerr
}
