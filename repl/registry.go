// Registry defines a registry for special REPL-only commands.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.

You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package repl

import (
	"github.com/pkg/errors"
	"sort"
	"strings"
	"unicode"
)

// CmdFunc is a function for a command. The function will be passed any text entered by the user following the command name.
type CmdFunc func(string) error

// Cmd defines a command.
type Cmd struct {
	name string  // The command name (excluding the leading '.')
	desc string  // A description of this command
	run  CmdFunc // The associated function
}

// Registry is a register of available commands.
type Registry struct {
	r map[string]*Cmd
}

/////////////////////////////////////////////////////////////////////////
// Cmd functions
/////////////////////////////////////////////////////////////////////////

// String returns the command name (excluding the leading '.').
func (c *Cmd) String() string {
	if c == nil {
		return ""
	}
	return c.name
}

// Description returns a description of the command.
func (c *Cmd) Description() string {
	if c == nil {
		return ""
	}
	return c.desc
}

// Run runs the command.
func (c *Cmd) Run(l string) (err error) {
	if c != nil && c.run != nil {
		defer func() {
			if e := recover(); e != nil {
				err = errors.Errorf("panic in %s: %s", c, e)
			}
		}()
		err = c.run(l)
	}
	return
}

/////////////////////////////////////////////////////////////////////////
// Registry functions
/////////////////////////////////////////////////////////////////////////

// Register registers the a new command with given name (excluding the leading '.'), description, and function. If a command with this name was already registered it will be replaced with the new command. Returns the new command.
func (reg *Registry) Register(name string, desc string, run CmdFunc) *Cmd {
	c := &Cmd{
		name: name,
		desc: desc,
		run:  run,
	}
	if reg.r == nil {
		reg.r = make(map[string]*Cmd)
	}
	reg.r[name] = c
	return c
}

// Names returns a slice of registered command names (excluding the leading '.'), sorted in lex order.
func (reg *Registry) Names() []string {
	if reg == nil {
		return []string{}
	}
	names := make([]string, 0, len(reg.r))
	for name := range reg.r {
		names = append(names, name)
	}
	sort.Strings(names)
	return names
}

// Get returns the command corresponding to the given name (excluding the leading '.').
func (reg *Registry) Get(name string) (c *Cmd, ok bool) {
	if reg != nil {
		c, ok = reg.r[name]
	}
	return
}

// Run inspects the given line l of user input and sees whether it corresponds to a registered command. If so, the command will be run. Returns true if this line was handled by a command, along with any error returned by the command. Returns false if this line did not match a registered command.
func (reg *Registry) Run(l string) (bool, error) {
	// Sanity check
	if reg == nil {
		return false, nil
	}
	// Trim the line and check that it starts with a '.'
	l = strings.TrimSpace(l)
	if len(l) == 0 || l[0] != '.' {
		return false, nil
	}
	l = l[1:]
	// Extract what would be the command name from l
	var name string
	if idx := strings.Index(l, " "); idx != -1 {
		name = l[:idx]
		l = strings.TrimLeftFunc(l[idx:], unicode.IsSpace)
	} else {
		name = l
		l = ""
	}
	// If the name matches against a command, run the command
	if c, ok := reg.r[name]; ok {
		return true, c.Run(l)
	}
	return false, nil
}
