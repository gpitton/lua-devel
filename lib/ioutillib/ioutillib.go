// Ioutillib implements some I/O utility functions.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.

You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package ioutillib

import (
	"bitbucket.org/pcas/golua/lib/iobindings"
	"bitbucket.org/pcas/golua/module"
	rt "bitbucket.org/pcas/golua/runtime"
	"bufio"
	"bytes"
	"context"
	"errors"
	"fmt"
	"io"
	"runtime"
	"strings"
)

// registryKeyType is a private struct used to define a unique key in the registry.
type registryKeyType struct{}

var registryKey = registryKeyType{}

// registryData is the data we store in the registry, indexed by registryKey.
type registryData struct {
	readerMt         *rt.Table // The metatable for an io.Reader
	writerMt         *rt.Table // The metatable for an io.Writer
	limitedReaderMt  *rt.Table // The metatable for an io.LimitedReader
	bufferMt         *rt.Table // The metatable for a bytes.Buffer
	bufferedReaderMt *rt.Table // The metatable for a bufio.Reader
	bufferedWriterMt *rt.Table // The metatable for a bufio.Writer
	stringBuilderMt  *rt.Table // The metatable for a strings.Builder
	stringReaderMt   *rt.Table // The metatable for a strings.Reader
}

// readerFunc implements an io.Reader where, prior to each Read, the Interrupt channel is inspected for cancellation.
type readerFunc func([]byte) (int, error)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// getRegistryData returns the package data stored in the registry.
func getRegistryData(t *rt.Thread) *registryData {
	return t.Registry(registryKey).(*registryData)
}

// limitedReaderMethod wraps the GoFunction f with a check that arg(0) is an *io.LimitedReader.
func limitedReaderMethod(f func(*rt.Thread, *rt.GoCont) (rt.Cont, error)) func(*rt.Thread, *rt.GoCont) (rt.Cont, error) {
	return func(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
		if err := c.Check1Arg(); err != nil {
			return nil, err
		} else if _, err := LimitedReaderArg(c, 0); err != nil {
			return nil, err
		}
		return f(t, c)
	}
}

// bufferMethod wraps the GoFunction f with a check that arg(0) is a *bytes.Buffer.
func bufferMethod(f func(*rt.Thread, *rt.GoCont) (rt.Cont, error)) func(*rt.Thread, *rt.GoCont) (rt.Cont, error) {
	return func(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
		if err := c.Check1Arg(); err != nil {
			return nil, err
		} else if _, err := BufferArg(c, 0); err != nil {
			return nil, err
		}
		return f(t, c)
	}
}

// bufferedReaderMethod wraps the GoFunction f with a check that arg(0) is a *bufio.Reader.
func bufferedReaderMethod(f func(*rt.Thread, *rt.GoCont) (rt.Cont, error)) func(*rt.Thread, *rt.GoCont) (rt.Cont, error) {
	return func(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
		if err := c.Check1Arg(); err != nil {
			return nil, err
		} else if _, err := BufferedReaderArg(c, 0); err != nil {
			return nil, err
		}
		return f(t, c)
	}
}

// bufferedWriterMethod wraps the GoFunction f with a check that arg(0) is a *bufio.Writer.
func bufferedWriterMethod(f func(*rt.Thread, *rt.GoCont) (rt.Cont, error)) func(*rt.Thread, *rt.GoCont) (rt.Cont, error) {
	return func(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
		if err := c.Check1Arg(); err != nil {
			return nil, err
		} else if _, err := BufferedWriterArg(c, 0); err != nil {
			return nil, err
		}
		return f(t, c)
	}
}

// stringBuilderMethod wraps the GoFunction f with a check that arg(0) is a *strings.Builder.
func stringBuilderMethod(f func(*rt.Thread, *rt.GoCont) (rt.Cont, error)) func(*rt.Thread, *rt.GoCont) (rt.Cont, error) {
	return func(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
		if err := c.Check1Arg(); err != nil {
			return nil, err
		} else if _, err := StringBuilderArg(c, 0); err != nil {
			return nil, err
		}
		return f(t, c)
	}
}

// stringReaderMethod wraps the GoFunction f with a check that arg(0) is a *strings.Reader.
func stringReaderMethod(f func(*rt.Thread, *rt.GoCont) (rt.Cont, error)) func(*rt.Thread, *rt.GoCont) (rt.Cont, error) {
	return func(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
		if err := c.Check1Arg(); err != nil {
			return nil, err
		} else if _, err := StringReaderArg(c, 0); err != nil {
			return nil, err
		}
		return f(t, c)
	}
}

/////////////////////////////////////////////////////////////////////////
// readerFunc functions
/////////////////////////////////////////////////////////////////////////

// newReaderFunc wraps the given io.Reader r so that, before each call to r.Read, the context is inspected.
func newReaderFunc(ctx context.Context, r io.Reader) io.Reader {
	return readerFunc(func(p []byte) (int, error) {
		select {
		case <-ctx.Done():
			return 0, ctx.Err()
		default:
			return r.Read(p)
		}
	})
}

// Read calls the readerFunc.
func (rf readerFunc) Read(p []byte) (n int, err error) {
	return rf(p)
}

/////////////////////////////////////////////////////////////////////////
// Lua bindings
/////////////////////////////////////////////////////////////////////////

// init registers the module ready for loading into a Lua runtime.
func init() {
	module.Register(module.Info{
		Name:        "ioutil",
		Description: "Implements some I/O utility functions.",
		Load: func(r *rt.Runtime) (*rt.Table, error) {
			// Save the registry data
			r.SetRegistry(registryKey, &registryData{
				readerMt:         createReaderMetatable(),
				writerMt:         createWriterMetatable(),
				limitedReaderMt:  createLimitedReaderMetatable(),
				bufferMt:         createBufferMetatable(),
				bufferedReaderMt: createBufferedReaderMetatable(),
				bufferedWriterMt: createBufferedWriterMetatable(),
				stringBuilderMt:  createStringBuilderMetatable(),
				stringReaderMt:   createStringReaderMetatable(),
			})
			// Create the package table
			pkg := rt.NewTable()
			rt.SetEnvGoFunc(pkg, "isreader", isreader, 1, false)
			rt.SetEnvGoFunc(pkg, "iswriter", iswriter, 1, false)
			rt.SetEnvGoFuncContext(pkg, "copy", copyf, 2, false)
			rt.SetEnvGoFuncContext(pkg, "copyn", copyn, 3, false)
			rt.SetEnvGoFunc(pkg, "limitreader", limitreader, 2, false)
			rt.SetEnvGoFunc(pkg, "teereader", teereader, 2, false)
			rt.SetEnvGoFunc(pkg, "multireader", multireader, 0, true)
			rt.SetEnvGoFunc(pkg, "multiwriter", multiwriter, 0, true)
			rt.SetEnvGoFunc(pkg, "buffer", buffer, 0, false)
			rt.SetEnvGoFunc(pkg, "bufferedreader", bufferedreader, 1, false)
			rt.SetEnvGoFunc(pkg, "bufferedwriter", bufferedwriter, 1, false)
			rt.SetEnvGoFunc(pkg, "stringbuilder", stringbuilder, 0, false)
			rt.SetEnvGoFunc(pkg, "stringreader", stringreader, 1, false)
			return pkg, nil
		},
	})
}

// createReaderMetatable creates the metatable for an io.Reader.
func createReaderMetatable() *rt.Table {
	// Create the methods for a reader
	methods := rt.NewTable()
	rt.SetEnvGoFunc(methods, "read", iobindings.Read, 1, true)
	rt.SetEnvGoFunc(methods, "lines", iobindings.Lines, 1, true)
	// Create the metatable for a reader
	meta := rt.NewTable()
	rt.SetEnv(meta, "__index", methods)
	rt.SetEnvGoFunc(meta, "__tostring", readertostring, 1, false)
	return meta
}

// createWriterMetatable creates the metatable for an io.Writer.
func createWriterMetatable() *rt.Table {
	// Create the methods for a writer
	methods := rt.NewTable()
	rt.SetEnvGoFunc(methods, "write", iobindings.Write, 1, true)
	// Create the metatable for a writer
	meta := rt.NewTable()
	rt.SetEnv(meta, "__index", methods)
	rt.SetEnvGoFunc(meta, "__tostring", writertostring, 1, false)
	return meta
}

// createLimitedReaderMetatable creates the metatable for an io.LimitedReader.
func createLimitedReaderMetatable() *rt.Table {
	// Create the methods for a reader
	methods := rt.NewTable()
	rt.SetEnvGoFunc(methods, "read", limitedReaderMethod(iobindings.Read), 1, true)
	rt.SetEnvGoFunc(methods, "lines", limitedReaderMethod(iobindings.Lines), 1, true)
	rt.SetEnvGoFunc(methods, "n", lrn, 1, false)
	// Create the metatable for a reader
	meta := rt.NewTable()
	rt.SetEnv(meta, "__index", methods)
	rt.SetEnvGoFunc(meta, "__tostring", limitedReaderMethod(readertostring), 1, false)
	return meta
}

// createBufferMetatable creates the metatable for a bytes.Buffer.
func createBufferMetatable() *rt.Table {
	// Create the methods
	methods := rt.NewTable()
	rt.SetEnvGoFunc(methods, "read", bufferMethod(iobindings.Read), 1, true)
	rt.SetEnvGoFunc(methods, "lines", bufferMethod(iobindings.Lines), 1, true)
	rt.SetEnvGoFunc(methods, "write", bufferMethod(iobindings.Write), 1, true)
	rt.SetEnvGoFunc(methods, "reset", breset, 1, false)
	rt.SetEnvGoFunc(methods, "len", blen, 1, false)
	// Create the metatable
	meta := rt.NewTable()
	rt.SetEnv(meta, "__index", methods)
	rt.SetEnvGoFunc(meta, "__tostring", bufferMethod(readertostring), 1, false)
	return meta
}

// createBufferedReaderMetatable creates the metatable for a bufio.Reader.
func createBufferedReaderMetatable() *rt.Table {
	// Create the methods for a reader
	methods := rt.NewTable()
	rt.SetEnvGoFunc(methods, "read", bufferedReaderMethod(iobindings.Read), 1, true)
	rt.SetEnvGoFunc(methods, "lines", bufferedReaderMethod(iobindings.Lines), 1, true)
	// Create the metatable for a reader
	meta := rt.NewTable()
	rt.SetEnv(meta, "__index", methods)
	rt.SetEnvGoFunc(meta, "__tostring", bufferedReaderMethod(readertostring), 1, false)
	return meta
}

// createBufferedWriterMetatable creates the metatable for a bufio.Writer.
func createBufferedWriterMetatable() *rt.Table {
	// Create the methods for a writer
	methods := rt.NewTable()
	rt.SetEnvGoFunc(methods, "write", bufferedWriterMethod(iobindings.Write), 1, true)
	rt.SetEnvGoFunc(methods, "flush", bufferedWriterMethod(iobindings.Flush), 1, true)
	// Create the metatable for a writer
	meta := rt.NewTable()
	rt.SetEnv(meta, "__index", methods)
	rt.SetEnvGoFunc(meta, "__tostring", bufferedWriterMethod(writertostring), 1, false)
	return meta
}

// createStringBuilderMetatable creates the metatable for a strings.Builder.
func createStringBuilderMetatable() *rt.Table {
	// Create the methods
	methods := rt.NewTable()
	rt.SetEnvGoFunc(methods, "write", stringBuilderMethod(iobindings.Write), 1, true)
	rt.SetEnvGoFunc(methods, "reset", sbreset, 1, false)
	rt.SetEnvGoFunc(methods, "len", sblen, 1, false)
	rt.SetEnvGoFunc(methods, "string", sbstring, 1, false)
	// Create the metatable
	meta := rt.NewTable()
	rt.SetEnv(meta, "__index", methods)
	rt.SetEnvGoFunc(meta, "__tostring", stringBuilderMethod(writertostring), 1, false)
	return meta
}

// createStringReaderMetatable creates the metatable for a strings.Reader.
func createStringReaderMetatable() *rt.Table {
	// Create the methods
	methods := rt.NewTable()
	rt.SetEnvGoFunc(methods, "read", stringReaderMethod(iobindings.Read), 1, true)
	rt.SetEnvGoFunc(methods, "lines", stringReaderMethod(iobindings.Lines), 1, true)
	rt.SetEnvGoFunc(methods, "reset", srreset, 2, false)
	rt.SetEnvGoFunc(methods, "len", srlen, 1, false)
	// Create the metatable
	meta := rt.NewTable()
	rt.SetEnv(meta, "__index", methods)
	rt.SetEnvGoFunc(meta, "__tostring", stringReaderMethod(readertostring), 1, false)
	return meta
}

// isreader returns true iff the argument is a reader.
// Args: x
func isreader(_ *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	if err := c.Check1Arg(); err != nil {
		return nil, err
	}
	_, ok := iobindings.ValueToReader(c.Arg(0))
	return c.PushingNext(rt.Bool(ok)), nil
}

// iswriter returns true iff the argument is a writer.
// Args: x
func iswriter(_ *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	if err := c.Check1Arg(); err != nil {
		return nil, err
	}
	_, ok := iobindings.ValueToWriter(c.Arg(0))
	return c.PushingNext(rt.Bool(ok)), nil
}

// copyf copies from 'src' (which must be a reader) to 'dst' (which must be a writer) until either EOF is reached on src or an error occurs.
//
// On success it returns the number of bytes copied. If an error occurs during copy then it returns nil followed by an error message and possible error code.
// Args: dst src
func copyf(ctx context.Context, _ *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	// Recover dst and src
	if err := c.CheckNArgs(2); err != nil {
		return nil, err
	}
	dst, err := iobindings.WriterArg(c, 0)
	if err != nil {
		return nil, err
	}
	src, err := iobindings.ReaderArg(c, 1)
	if err != nil {
		return nil, err
	}
	// Perform the copy
	n, err := io.Copy(dst, newReaderFunc(ctx, src))
	return iobindings.PushingNextResult(c, rt.Int(n), err)
}

// copyn copies from 'src' (which must be a reader) to 'dst' (which must be a writer) until n bytes have been copied, EOF is reached on src, or an error occurs.
//
// On success it returns the number of bytes copied. If an error occurs during copy then it returns nil followed by an error message and possible error code.
// Args: dst src n
func copyn(ctx context.Context, _ *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	// Recover dst, src, and n
	if err := c.CheckNArgs(3); err != nil {
		return nil, err
	}
	dst, err := iobindings.WriterArg(c, 0)
	if err != nil {
		return nil, err
	}
	src, err := iobindings.ReaderArg(c, 1)
	if err != nil {
		return nil, err
	}
	n, err := c.IntArg(2)
	if err != nil {
		return nil, err
	}
	// Sanity check
	if n < 0 {
		return nil, errors.New("#3 must be a non-negative integer")
	}
	// Perform the copy
	k, err := io.CopyN(dst, newReaderFunc(ctx, src), int64(n))
	return iobindings.PushingNextResult(c, rt.Int(k), err)
}

// limitreader returns a reader that reads from r (which must be a reader) but stops with EOF after n bytes.
// Args: r n
func limitreader(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	// Recover r and n
	if err := c.CheckNArgs(2); err != nil {
		return nil, err
	}
	r, err := iobindings.ReaderArg(c, 0)
	if err != nil {
		return nil, err
	}
	n, err := c.IntArg(1)
	if err != nil {
		return nil, err
	}
	// Sanity check
	if n < 0 {
		return nil, errors.New("#2 must be a non-negative integer")
	}
	// Return the limit reader
	return c.PushingNext(rt.NewUserData(io.LimitReader(r, int64(n)), getRegistryData(t).limitedReaderMt)), nil
}

// lrn returns the number of bytes remaining to be read from the limit reader.
// Args: r
func lrn(_ *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	if err := c.Check1Arg(); err != nil {
		return nil, err
	}
	r, err := LimitedReaderArg(c, 0)
	if err != nil {
		return nil, err
	}
	return c.PushingNext(rt.Int(r.N)), nil
}

// teereader returns a reader that writes to w (which must be a writer) what it reads from r (which must be a reader). All reads from r performed through it are matched with corresponding writes to w. There is no internal buffering - the write must complete before the read completes.
// Args: r w
func teereader(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	// Recover r and w
	if err := c.CheckNArgs(2); err != nil {
		return nil, err
	}
	r, err := iobindings.ReaderArg(c, 0)
	if err != nil {
		return nil, err
	}
	w, err := iobindings.WriterArg(c, 1)
	if err != nil {
		return nil, err
	}
	// Return the tee reader
	return c.PushingNext(rt.NewUserData(io.TeeReader(r, w), getRegistryData(t).readerMt)), nil
}

// multireader returns a reader that is the logical concatenation of the provided input readers. They are read sequentially.
// Args: [r1 [... rn]]
func multireader(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	// Recover the readers
	etc := c.Etc()
	rs := make([]io.Reader, 0, len(etc))
	for i, v := range etc {
		r, ok := iobindings.ValueToReader(v)
		if !ok {
			return nil, fmt.Errorf("#%d must describe an io.Reader", i+1)
		}
		rs = append(rs, r)
	}
	// Return the multireader
	return c.PushingNext(rt.NewUserData(io.MultiReader(rs...), getRegistryData(t).readerMt)), nil
}

// multiwriter creates a writer that duplicates its writes to all the provided writers.
//
// Each write is written to each listed writer, one at a time. If a listed writer returns an error, that overall write operation stops and returns the error; it does not continue down the list.
// Args: [w1 [... wn]]
func multiwriter(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	// Recover the writers
	etc := c.Etc()
	ws := make([]io.Writer, 0, len(etc))
	for i, v := range etc {
		w, ok := iobindings.ValueToWriter(v)
		if !ok {
			return nil, fmt.Errorf("#%d must describe an io.Writer", i+1)
		}
		ws = append(ws, w)
	}
	// Return the multiwriter
	return c.PushingNext(rt.NewUserData(io.MultiWriter(ws...), getRegistryData(t).writerMt)), nil
}

// buffer returns a buffer that allows both reading and writing.
// Args:
func buffer(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	return c.PushingNext(rt.NewUserData(&bytes.Buffer{}, getRegistryData(t).bufferMt)), nil
}

// blen returns the number of bytes of the unread portion of the buffer.
// Args: b
func blen(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	if err := c.Check1Arg(); err != nil {
		return nil, err
	}
	b, err := BufferArg(c, 0)
	if err != nil {
		return nil, err
	}
	return c.PushingNext(rt.Int(b.Len())), nil
}

// breset resets the buffer b to be empty. Returns the buffer.
// Args: b
func breset(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	if err := c.Check1Arg(); err != nil {
		return nil, err
	}
	b, err := BufferArg(c, 0)
	if err != nil {
		return nil, err
	}
	b.Reset()
	return c.PushingNext(c.Arg(0)), nil
}

// bufferedreader returns a reader that reads from r (which must be a reader), backed by a buffer.
// Args: r
func bufferedreader(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	if err := c.Check1Arg(); err != nil {
		return nil, err
	}
	r, err := iobindings.ReaderArg(c, 0)
	if err != nil {
		return nil, err
	}
	return c.PushingNext(rt.NewUserData(bufio.NewReader(r), getRegistryData(t).bufferedReaderMt)), nil
}

// bufferedwriter returns a writer that writes to w (which must be a writer), backed by a buffer. After all data has been written, the user should call the flush method to guarantee all data has been forwarded to the underlying writer.
// Args: w
func bufferedwriter(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	if err := c.Check1Arg(); err != nil {
		return nil, err
	}
	w, err := iobindings.WriterArg(c, 0)
	if err != nil {
		return nil, err
	}
	b := bufio.NewWriter(w)
	runtime.SetFinalizer(b, func(b *bufio.Writer) { b.Flush() })
	return c.PushingNext(rt.NewUserData(b, getRegistryData(t).bufferedWriterMt)), nil
}

// stringbuilder returns a new string builder.
// Args:
func stringbuilder(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	return c.PushingNext(rt.NewUserData(&strings.Builder{}, getRegistryData(t).stringBuilderMt)), nil
}

// sblen returns the number of accumulated bytes.
// Args: b
func sblen(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	if err := c.Check1Arg(); err != nil {
		return nil, err
	}
	b, err := StringBuilderArg(c, 0)
	if err != nil {
		return nil, err
	}
	return c.PushingNext(rt.Int(b.Len())), nil
}

// sbreset resets the string builder b. Returns the string builder.
// Args: b
func sbreset(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	if err := c.Check1Arg(); err != nil {
		return nil, err
	}
	b, err := StringBuilderArg(c, 0)
	if err != nil {
		return nil, err
	}
	b.Reset()
	return c.PushingNext(c.Arg(0)), nil
}

// sbstring returns the accumulated string.
// Args: b
func sbstring(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	if err := c.Check1Arg(); err != nil {
		return nil, err
	}
	b, err := StringBuilderArg(c, 0)
	if err != nil {
		return nil, err
	}
	return c.PushingNext(rt.String(b.String())), nil
}

// stringreader returns a reader reading from the given string.
// Args: s
func stringreader(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	if err := c.Check1Arg(); err != nil {
		return nil, err
	}
	s, err := c.StringArg(0)
	if err != nil {
		return nil, err
	}
	return c.PushingNext(rt.NewUserData(strings.NewReader(string(s)), getRegistryData(t).stringReaderMt)), nil
}

// srlen returns the number of bytes of the unread portion of the string.
// Args: r
func srlen(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	if err := c.Check1Arg(); err != nil {
		return nil, err
	}
	r, err := StringReaderArg(c, 0)
	if err != nil {
		return nil, err
	}
	return c.PushingNext(rt.Int(r.Len())), nil
}

// srreset resets the string reader r to be reading from the string s. Returns the string reader.
// Args: r s
func srreset(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	if err := c.CheckNArgs(2); err != nil {
		return nil, err
	}
	r, err := StringReaderArg(c, 0)
	if err != nil {
		return nil, err
	}
	s, err := c.StringArg(1)
	if err != nil {
		return nil, err
	}
	r.Reset(string(s))
	return c.PushingNext(c.Arg(0)), nil
}

// readertostring returns a generic string for a reader.
// Args: r
func readertostring(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	if err := c.Check1Arg(); err != nil {
		return nil, err
	}
	r, err := iobindings.ReaderArg(c, 0)
	if err != nil {
		return nil, err
	}
	var s string
	switch x := r.(type) {
	case *io.LimitedReader:
		s = fmt.Sprintf("limitedreader(%d)", x.N)
	case *bytes.Buffer:
		s = "buffer"
	case *bufio.Reader:
		s = "bufferedreader"
	case *strings.Reader:
		s = "stringreader"
	case fmt.Stringer:
		s = x.String()
	default:
		s = "reader"
	}
	return c.PushingNext(rt.String(s)), nil
}

// writertostring returns a generic string for a writer.
// Args: r
func writertostring(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	if err := c.Check1Arg(); err != nil {
		return nil, err
	}
	w, err := iobindings.WriterArg(c, 0)
	if err != nil {
		return nil, err
	}
	var s string
	switch x := w.(type) {
	case *bytes.Buffer:
		s = "buffer"
	case *bufio.Writer:
		s = "bufferedwriter"
	case *strings.Builder:
		s = "stringbuilder"
	case fmt.Stringer:
		s = x.String()
	default:
		s = "writer"
	}
	return c.PushingNext(rt.String(s)), nil
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// LimitedReaderArg turns a continuation argument into an *io.LimitedReader.
func LimitedReaderArg(c *rt.GoCont, n int) (*io.LimitedReader, error) {
	r, ok := ValueToLimitedReader(c.Arg(n))
	if !ok {
		return nil, fmt.Errorf("#%d must be a bufio.Reader", n+1)
	}
	return r, nil
}

// ValueToLimitedReader turns a Lua value to an *io.LimitedReader if possible.
func ValueToLimitedReader(v rt.Value) (*io.LimitedReader, bool) {
	u, ok := v.(*rt.UserData)
	if !ok {
		return nil, false
	}
	r, ok := u.Value().(*io.LimitedReader)
	return r, ok
}

// BufferArg turns a continuation argument into a *bytes.Buffer.
func BufferArg(c *rt.GoCont, n int) (*bytes.Buffer, error) {
	b, ok := ValueToBuffer(c.Arg(n))
	if !ok {
		return nil, fmt.Errorf("#%d must be a bytes.Buffer", n+1)
	}
	return b, nil
}

// ValueToBuffer turns a Lua value to a *bytes.Buffer if possible.
func ValueToBuffer(v rt.Value) (*bytes.Buffer, bool) {
	u, ok := v.(*rt.UserData)
	if !ok {
		return nil, false
	}
	b, ok := u.Value().(*bytes.Buffer)
	return b, ok
}

// BufferedReaderArg turns a continuation argument into a *bufio.Reader.
func BufferedReaderArg(c *rt.GoCont, n int) (*bufio.Reader, error) {
	r, ok := ValueToBufferedReader(c.Arg(n))
	if !ok {
		return nil, fmt.Errorf("#%d must be a bufio.Reader", n+1)
	}
	return r, nil
}

// ValueToBufferedReader turns a Lua value to a *bufio.Reader if possible.
func ValueToBufferedReader(v rt.Value) (*bufio.Reader, bool) {
	u, ok := v.(*rt.UserData)
	if !ok {
		return nil, false
	}
	r, ok := u.Value().(*bufio.Reader)
	return r, ok
}

// BufferedWriterArg turns a continuation argument into a *bufio.Writer.
func BufferedWriterArg(c *rt.GoCont, n int) (*bufio.Writer, error) {
	w, ok := ValueToBufferedWriter(c.Arg(n))
	if !ok {
		return nil, fmt.Errorf("#%d must be a bufio.Writer", n+1)
	}
	return w, nil
}

// ValueToBufferedWriter turns a Lua value to a *bufio.Writer if possible.
func ValueToBufferedWriter(v rt.Value) (*bufio.Writer, bool) {
	u, ok := v.(*rt.UserData)
	if !ok {
		return nil, false
	}
	w, ok := u.Value().(*bufio.Writer)
	return w, ok
}

// StringBuilderArg turns a continuation argument into a *strings.Builder.
func StringBuilderArg(c *rt.GoCont, n int) (*strings.Builder, error) {
	b, ok := ValueToStringBuilder(c.Arg(n))
	if !ok {
		return nil, fmt.Errorf("#%d must be a strings.Builder", n+1)
	}
	return b, nil
}

// ValueToStringBuilder turns a Lua value to a *strings.Builder if possible.
func ValueToStringBuilder(v rt.Value) (*strings.Builder, bool) {
	u, ok := v.(*rt.UserData)
	if !ok {
		return nil, false
	}
	b, ok := u.Value().(*strings.Builder)
	return b, ok
}

// StringReaderArg turns a continuation argument into a *strings.Reader.
func StringReaderArg(c *rt.GoCont, n int) (*strings.Reader, error) {
	r, ok := ValueToStringReader(c.Arg(n))
	if !ok {
		return nil, fmt.Errorf("#%d must be a strings.Reader", n+1)
	}
	return r, nil
}

// ValueToStringReader turns a Lua value to a *strings.Reader if possible.
func ValueToStringReader(v rt.Value) (*strings.Reader, bool) {
	u, ok := v.(*rt.UserData)
	if !ok {
		return nil, false
	}
	r, ok := u.Value().(*strings.Reader)
	return r, ok
}
