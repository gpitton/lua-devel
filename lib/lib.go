// Lib imports the standard library.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.

You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package lib

import (
	// Blank imports for the library packages
	_ "bitbucket.org/pcas/golua/lib"
	_ "bitbucket.org/pcas/lua/lib/base64lib"
	_ "bitbucket.org/pcas/lua/lib/binarylib"
	_ "bitbucket.org/pcas/lua/lib/compressorlib"
	_ "bitbucket.org/pcas/lua/lib/cryptolib"
	_ "bitbucket.org/pcas/lua/lib/filelib"
	_ "bitbucket.org/pcas/lua/lib/fslib"
	_ "bitbucket.org/pcas/lua/lib/fslib/filesystem"
	_ "bitbucket.org/pcas/lua/lib/fslib/fsd"
	_ "bitbucket.org/pcas/lua/lib/fslib/seaweed"
	_ "bitbucket.org/pcas/lua/lib/ioutillib"
	_ "bitbucket.org/pcas/lua/lib/jsonlib"
	_ "bitbucket.org/pcas/lua/lib/keyvaluelib"
	_ "bitbucket.org/pcas/lua/lib/keyvaluelib/kvdb"
	_ "bitbucket.org/pcas/lua/lib/keyvaluelib/mongodb"
	_ "bitbucket.org/pcas/lua/lib/keyvaluelib/postgres"
	_ "bitbucket.org/pcas/lua/lib/logginglib"
	_ "bitbucket.org/pcas/lua/lib/monitoringlib"
	_ "bitbucket.org/pcas/lua/lib/osutillib"
	_ "bitbucket.org/pcas/lua/lib/pathlib"
	_ "bitbucket.org/pcas/lua/lib/stringutillib"
	_ "bitbucket.org/pcas/lua/lib/tableutillib"
	_ "bitbucket.org/pcas/lua/lib/ulidlib"
	_ "bitbucket.org/pcas/lua/lib/urllib"
)
