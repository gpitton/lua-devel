// Ulidlib provides implementation of Unique Lexicographically Sortable Identifier (ULID).

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.

You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package ulidlib

import (
	"bitbucket.org/pcas/golua/lib/iobindings"
	"bitbucket.org/pcas/golua/lib/oslib"
	"bitbucket.org/pcas/golua/module"
	rt "bitbucket.org/pcas/golua/runtime"
	"bitbucket.org/pcastools/ulid"
	"errors"
	"fmt"
	"math"
)

// registryKeyType is a private struct used to define a unique key in the registry.
type registryKeyType struct{}

var registryKey = registryKeyType{}

// registryData is the data we store in the registry, indexed by registryKey.
type registryData struct {
	metatable *rt.Table // The metatable for a ULID
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// ulidMethod wraps the GoFunction f with a check that arg(0) is a ulid.ULID.
func ulidMethod(f func(*rt.Thread, *rt.GoCont) (rt.Cont, error)) func(*rt.Thread, *rt.GoCont) (rt.Cont, error) {
	return func(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
		if err := c.Check1Arg(); err != nil {
			return nil, err
		} else if _, err := ULIDArg(c, 0); err != nil {
			return nil, err
		}
		return f(t, c)
	}
}

// tableToBytes returns a byte slice from the given table.
func tableToBytes(t *rt.Table) ([]byte, error) {
	b := make([]byte, 0)
	i := 1
	for {
		v := t.Get(rt.Int(i))
		if rt.IsNil(v) {
			break
		}
		n, ok := rt.ToInt(v)
		if !ok {
			break
		} else if n < 0 || n > math.MaxUint8 {
			return nil, fmt.Errorf("element %d is out-of-range", i)
		}
		b = append(b, byte(n))
		i++
	}
	return b, nil
}

/////////////////////////////////////////////////////////////////////////
// registryData functions
/////////////////////////////////////////////////////////////////////////

// getRegistryData returns the package data stored in the registry.
func getRegistryData(t *rt.Thread) *registryData {
	return t.Registry(registryKey).(*registryData)
}

/////////////////////////////////////////////////////////////////////////
// Lua bindings
/////////////////////////////////////////////////////////////////////////

// init registers the module ready for loading into a Lua runtime.
func init() {
	module.Register(module.Info{
		Name:        "ulid",
		Description: "Provides Unique Lexicographically Sortable Identifiers (ULIDs).",
		Load: func(r *rt.Runtime) (*rt.Table, error) {
			// Save the registry data
			r.SetRegistry(registryKey, &registryData{
				metatable: createULIDMetatable(),
			})
			// Create the package table
			pkg := rt.NewTable()
			rt.SetEnvGoFunc(pkg, "new", newULID, 0, false)
			rt.SetEnvGoFunc(pkg, "parse", parse, 1, false)
			rt.SetEnvGoFunc(pkg, "compare", compare, 2, false)
			rt.SetEnvGoFunc(pkg, "equal", equal, 2, false)
			return pkg, nil
		},
	})
}

// createULIDMetatable creates the metatable for a ulid.ULID.
func createULIDMetatable() *rt.Table {
	// Create the methods for a ULID
	methods := rt.NewTable()
	rt.SetEnvGoFunc(methods, "bytes", bytes, 1, false)
	rt.SetEnvGoFunc(methods, "string", ulidMethod(iobindings.ToString), 1, false)
	rt.SetEnvGoFunc(methods, "time", totime, 1, false)
	// Create the metatable for a ULID
	meta := rt.NewTable()
	rt.SetEnv(meta, "__index", methods)
	rt.SetEnvGoFunc(meta, "__tostring", ulidMethod(iobindings.ToString), 1, false)
	return meta
}

// newULID returns a random generated ULID.
// Args:
func newULID(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	id, err := ulid.New()
	if err != nil {
		return nil, err
	}
	return c.PushingNext(rt.NewUserData(id, getRegistryData(t).metatable)), nil
}

// parse parses the given argument to a ULID, returning the ULID on success, nil and a description of the error otherwise. Valid argument types are a string, a table (interpreted as an array of bytes), or a ULID.
// Args: input
func parse(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	if err := c.Check1Arg(); err != nil {
		return nil, err
	}
	var id ulid.ULID
	var err error
	v := c.Arg(0)
	switch x := v.(type) {
	case rt.String:
		id, err = ulid.FromString(string(x))
	case *rt.Table:
		var b []byte
		if b, err = tableToBytes(x); err == nil {
			id, err = ulid.FromBytes(b)
		}
	case *rt.UserData:
		var ok bool
		if id, ok = ValueToULID(x); !ok {
			err = errors.New("#1 must be a string, a table, or a ULID")
		}
	default:
		err = errors.New("#1 must be a string, a table, or a ULID")
	}
	if err != nil {
		return iobindings.PushingError(c, err)
	}
	return c.PushingNext(rt.NewUserData(id, getRegistryData(t).metatable)), nil
}

// compare returns an integer comparing id1 and id2 lexicographically. The result will be 0 if id1 == id2, -1 if id1 < id2, and +1 if id1 > id2.
// Args: id1 id2
func compare(_ *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	if err := c.CheckNArgs(2); err != nil {
		return nil, err
	}
	id1, err := ULIDArg(c, 0)
	if err != nil {
		return nil, err
	}
	id2, err := ULIDArg(c, 1)
	if err != nil {
		return nil, err
	}
	return c.PushingNext(rt.Int(ulid.Compare(id1, id2))), nil
}

// equal returns true iff id1 == id2.
// Args: id1 id2
func equal(_ *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	if err := c.CheckNArgs(2); err != nil {
		return nil, err
	}
	id1, err := ULIDArg(c, 0)
	if err != nil {
		return nil, err
	}
	id2, err := ULIDArg(c, 1)
	if err != nil {
		return nil, err
	}
	return c.PushingNext(rt.Bool(ulid.Equal(id1, id2))), nil
}

// bytes returns the array of byte values corresponding to the given ULID.
// Args: id
func bytes(_ *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	if err := c.Check1Arg(); err != nil {
		return nil, err
	}
	id, err := ULIDArg(c, 0)
	if err != nil {
		return nil, err
	}
	t := rt.NewTable()
	for i, b := range id.Bytes() {
		t.Set(rt.Int(i+1), rt.Int(b))
	}
	return c.PushingNext(t), nil
}

// totime returns the time component of the given ULID.
// Args: id
func totime(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	if err := c.Check1Arg(); err != nil {
		return nil, err
	}
	id, err := ULIDArg(c, 0)
	if err != nil {
		return nil, err
	}
	when := id.Time().In(oslib.Location(t))
	return c.PushingNext(oslib.TimeToTable(when)), nil
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// ULIDArg turns a continuation argument into a ulid.ULID.
func ULIDArg(c *rt.GoCont, n int) (ulid.ULID, error) {
	id, ok := ValueToULID(c.Arg(n))
	if !ok {
		return id, fmt.Errorf("#%d must be a ULID", n+1)
	}
	return id, nil
}

// ValueToULID turns a lua value to a ulid.ULID, if possible.
func ValueToULID(v rt.Value) (ulid.ULID, bool) {
	u, ok := v.(*rt.UserData)
	if !ok {
		return ulid.ULID{}, false
	}
	id, ok := u.Value().(ulid.ULID)
	return id, ok
}
