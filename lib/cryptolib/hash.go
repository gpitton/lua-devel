// Hash implements the Lua view of a hash.Hash.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.

You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package cryptolib

import (
	"bitbucket.org/pcas/golua/lib/iobindings"
	rt "bitbucket.org/pcas/golua/runtime"
	"fmt"
	"hash"
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// hashMethod wraps the GoFunction f with a check that arg(0) is a hash.Hash.
func hashMethod(f func(*rt.Thread, *rt.GoCont) (rt.Cont, error)) func(*rt.Thread, *rt.GoCont) (rt.Cont, error) {
	return func(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
		if err := c.Check1Arg(); err != nil {
			return nil, err
		} else if _, err := HashArg(c, 0); err != nil {
			return nil, err
		}
		return f(t, c)
	}
}

// toTable converts the given slice of bytes to a table.
func toTable(S []byte) *rt.Table {
	t := rt.NewTable()
	for i, b := range S {
		t.Set(rt.Int(i+1), rt.Int(b))
	}
	return t
}

/////////////////////////////////////////////////////////////////////////
// Lua bindings
/////////////////////////////////////////////////////////////////////////

// createHashMetatable creates the metatable for a hash.
func createHashMetatable() *rt.Table {
	// Create the methods for a hash
	methods := rt.NewTable()
	rt.SetEnvGoFunc(methods, "reset", reset, 1, false)
	rt.SetEnvGoFunc(methods, "write", hashMethod(iobindings.Write), 1, true)
	rt.SetEnvGoFunc(methods, "sum", sum, 2, false)
	// Create the metatable for a hash
	meta := rt.NewTable()
	rt.SetEnv(meta, "__index", methods)
	rt.SetEnvGoFunc(meta, "__tostring", tostring, 1, false)
	return meta
}

// reset resets the hash object to a clean slate. Returns the object so that it can be reused in nested calls.
// Args: hash
func reset(_ *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	if err := c.Check1Arg(); err != nil {
		return nil, err
	}
	h, err := HashArg(c, 0)
	if err != nil {
		return nil, err
	}
	h.Reset()
	return c.PushingNext(c.Arg(0)), nil
}

// sum generates the message digest for the loaded data. The optional raw flag is a boolean indicating whether the output should be a direct binary equivalent of the message digest (true), or formatted as a hexadecimal string (false, the default).
// Args: hash [raw]
func sum(_ *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	if err := c.Check1Arg(); err != nil {
		return nil, err
	}
	h, err := HashArg(c, 0)
	if err != nil {
		return nil, err
	}
	raw := c.NArgs() > 1 && rt.Truth(c.Arg(1))
	b := h.Sum(nil)
	if raw {
		return c.PushingNext(toTable(b)), nil
	}
	return c.PushingNext(rt.String(fmt.Sprintf("%x", b))), nil
}

// tostring returns a string description of the hash.
// Args: hash
func tostring(_ *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	if err := c.Check1Arg(); err != nil {
		return nil, err
	}
	h, err := HashArg(c, 0)
	if err != nil {
		return nil, err
	}
	return c.PushingNext(rt.String(fmt.Sprintf("hash{%x}", h.Sum(nil)))), nil
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// HashArg turns a continuation argument into a hash.Hash.
func HashArg(c *rt.GoCont, n int) (hash.Hash, error) {
	h, ok := ValueToHash(c.Arg(n))
	if !ok {
		return nil, fmt.Errorf("#%d must be a hash", n+1)
	}
	return h, nil
}

// ValueToHash turns a lua value to a hash.Hash, if possible.
func ValueToHash(v rt.Value) (hash.Hash, bool) {
	u, ok := v.(*rt.UserData)
	if !ok {
		return nil, false
	}
	h, ok := u.Value().(hash.Hash)
	return h, ok
}
