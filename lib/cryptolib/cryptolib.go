// Cryptolib provides hashing functionality.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.

You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package cryptolib

import (
	"bitbucket.org/pcas/golua/module"
	rt "bitbucket.org/pcas/golua/runtime"
	"crypto"
	"fmt"
	"hash"
	"sort"
	"strings"
	// The hashes we need
	_ "crypto/md5"
	_ "crypto/sha1"
	_ "crypto/sha256"
	_ "crypto/sha512"
)

// registryKeyType is a private struct used to define a unique key in the registry.
type registryKeyType struct{}

var registryKey = registryKeyType{}

// registryData is the data we store in the registry, indexed by registryKey.
type registryData struct {
	metatable *rt.Table // The metatable for a hash
}

// HashMap is a map from hash names to hashes.
var HashMap = map[string]crypto.Hash{
	"md5":    crypto.MD5,
	"sha1":   crypto.SHA1,
	"sha256": crypto.SHA256,
	"sha512": crypto.SHA512,
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// getRegistryData returns the package data stored in the registry.
func getRegistryData(t *rt.Thread) *registryData {
	return t.Registry(registryKey).(*registryData)
}

// createDtypes returns a table listing the valid dtypes.
func createDtypes() *rt.Table {
	S := make([]string, 0, len(HashMap))
	for k := range HashMap {
		S = append(S, k)
	}
	sort.Strings(S)
	t := rt.NewTable()
	for i, k := range S {
		t.Set(rt.Int(i+1), rt.String(k))
	}
	return t
}

// recoverHash returns the hash.Hash with name given by the n-th continuation argument.
func recoverHash(c *rt.GoCont, n int) (hash.Hash, error) {
	s, err := c.StringArg(n)
	if err != nil {
		return nil, err
	}
	h, ok := HashMap[strings.ToLower(string(s))]
	if !ok {
		return nil, fmt.Errorf("#%d is not a valid dtype", n+1)
	}
	return h.New(), nil
}

/////////////////////////////////////////////////////////////////////////
// Lua bindings
/////////////////////////////////////////////////////////////////////////

// init registers the module ready for loading into a Lua runtime.
func init() {
	module.Register(module.Info{
		Name:        "crypto",
		Description: "Standard hashing digests (MD5, SHA-1, and more).",
		Load: func(r *rt.Runtime) (*rt.Table, error) {
			// Save the registry data
			r.SetRegistry(registryKey, &registryData{
				metatable: createHashMetatable(),
			})
			// Create the package table
			pkg := rt.NewTable()
			rt.SetEnv(pkg, "dtypes", createDtypes())
			rt.SetEnvGoFunc(pkg, "new", newhash, 1, false)
			return pkg, nil
		},
	})
}

// newhash returns a new message digest object using the algorithm specified by dtype.
// Args: dtype
func newhash(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	if err := c.Check1Arg(); err != nil {
		return nil, err
	}
	h, err := recoverHash(c, 0)
	if err != nil {
		return nil, err
	}
	return c.PushingNext(rt.NewUserData(h, getRegistryData(t).metatable)), nil
}
