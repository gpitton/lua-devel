// Filelib provides file system tools.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.

You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package filelib

import (
	"bitbucket.org/pcas/golua/lib/iobindings"
	"bitbucket.org/pcas/golua/module"
	rt "bitbucket.org/pcas/golua/runtime"
	"bitbucket.org/pcastools/file"
	"bufio"
	"errors"
	"fmt"
	"io"
	"math"
	"os"
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// wrapPathArg wraps the given function, turning it into a GoFunction.
func wrapPathArg(f func(string, *rt.GoCont) (rt.Cont, error)) func(*rt.Thread, *rt.GoCont) (rt.Cont, error) {
	return func(_ *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
		if err := c.Check1Arg(); err != nil {
			return nil, err
		}
		path, err := c.StringArg(0)
		if err != nil {
			return nil, err
		} else if len(path) == 0 {
			return nil, errors.New("#1 must not be the empty string")
		}
		return f(string(path), c)
	}
}

// modeArg recovers a file mode from the n-th arg.
func modeArg(c *rt.GoCont, n int) (os.FileMode, error) {
	m, err := c.IntArg(n)
	if err != nil {
		return 0, err
	} else if m < 0 || m > math.MaxUint32 {
		return 0, fmt.Errorf("#%d must be an integer in the range 0 to %d", n+1, math.MaxUint32)
	}
	return os.FileMode(m), nil
}

/////////////////////////////////////////////////////////////////////////
// Lua bindings
/////////////////////////////////////////////////////////////////////////

// init registers the module ready for loading into a Lua runtime.
func init() {
	module.Register(module.Info{
		Name:        "file",
		Description: "File system tools.",
		Load: func(r *rt.Runtime) (*rt.Table, error) {
			pkg := rt.NewTable()
			rt.SetEnvGoFunc(pkg, "truncate", wrapPathArg(truncate), 2, false)
			rt.SetEnvGoFunc(pkg, "chmod", wrapPathArg(chmod), 2, false)
			rt.SetEnvGoFunc(pkg, "rmdir", wrapPathArg(rmdir), 1, false)
			rt.SetEnvGoFunc(pkg, "rmdirall", wrapPathArg(rmdirall), 1, false)
			rt.SetEnvGoFunc(pkg, "mkdir", wrapPathArg(mkdir), 2, false)
			rt.SetEnvGoFunc(pkg, "mkdirall", wrapPathArg(mkdirall), 2, false)
			rt.SetEnvGoFunc(pkg, "readdir", readdir, 1, false)
			rt.SetEnvGoFunc(pkg, "readfile", readfile, 1, false)
			rt.SetEnvGoFunc(pkg, "writefile", wrapPathArg(writefile), 1, true)
			rt.SetEnvGoFunc(pkg, "appendfile", wrapPathArg(appendfile), 1, true)
			rt.SetEnvGoFunc(pkg, "exists", wrapPathArg(exists), 1, false)
			rt.SetEnvGoFunc(pkg, "isdir", wrapPathArg(isdir), 1, false)
			rt.SetEnvGoFunc(pkg, "isfile", wrapPathArg(isfile), 1, false)
			rt.SetEnvGoFunc(pkg, "emptydir", emptydir, 1, false)
			return pkg, nil
		},
	})
}

// truncate changes the size of the named file. If the file is a symbolic link, it changes the size of the link's target.
// Args: path size
func truncate(path string, c *rt.GoCont) (rt.Cont, error) {
	if err := c.CheckNArgs(2); err != nil {
		return nil, err
	}
	size, err := c.IntArg(1)
	if err != nil {
		return nil, err
	} else if size < 0 {
		return nil, errors.New("#2 must be a non-negative integer")
	}
	return iobindings.PushingNextResult(c, rt.Bool(true), os.Truncate(path, int64(size)))
}

// chmod changes the mode of the named file or directory.
// Args: path mode
func chmod(path string, c *rt.GoCont) (rt.Cont, error) {
	if err := c.CheckNArgs(2); err != nil {
		return nil, err
	}
	mode, err := modeArg(c, 1)
	if err != nil {
		return nil, err
	}
	return iobindings.PushingNextResult(c, rt.Bool(true), os.Chmod(path, mode))
}

// rmdir removes (deletes) the directory at the given path, provided that it is empty. If you need to remove a non-empty directory, see rmdirall.
// Args: path
func rmdir(path string, c *rt.GoCont) (rt.Cont, error) {
	return iobindings.PushingNextResult(c, rt.Bool(true), file.RemoveDir(path))
}

// rmdirall removes (deletes) the directory at the given path, along with any files or directories that it contains.
//
// Warning: This will recursively delete the contents of the directory. Use with care.
// Args: path
func rmdirall(path string, c *rt.GoCont) (rt.Cont, error) {
	return iobindings.PushingNextResult(c, rt.Bool(true), file.RemoveDirAll(path))
}

// mkdir creates a new directory with the specified path. The permission can be specified via the second (optional) argument. If you need to create missing intermediate directories, see mkdirall.
// Args: path [mode]
func mkdir(path string, c *rt.GoCont) (rt.Cont, error) {
	mode := os.FileMode(0755)
	if c.NArgs() > 1 {
		var err error
		if mode, err = modeArg(c, 1); err != nil {
			return nil, err
		}
	}
	return iobindings.PushingNextResult(c, rt.Bool(true), os.Mkdir(path, mode))
}

// mkdirall creates a new directory with the specified path, creating any intermediate directories as required. The permission can be specified via the second (optional) argument.
// Args: path [mode]
func mkdirall(path string, c *rt.GoCont) (rt.Cont, error) {
	mode := os.FileMode(0755)
	if c.NArgs() > 1 {
		var err error
		if mode, err = modeArg(c, 1); err != nil {
			return nil, err
		}
	}
	return iobindings.PushingNextResult(c, rt.Bool(true), os.MkdirAll(path, mode))
}

// readdir reads and returns an array of names from the directory with given path, or the current working directory if no path is specified.
// Args: [path]
func readdir(_ *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	// Recover the path
	var path string
	if c.NArgs() > 0 && !rt.IsNil(c.Arg(0)) {
		s, err := c.StringArg(0)
		if err != nil {
			return nil, err
		} else if len(s) == 0 {
			return nil, errors.New("#1 must not be the empty string")
		}
		path = string(s)
	} else {
		var err error
		if path, err = os.Getwd(); err != nil {
			return nil, err
		}
	}
	// Open the path
	d, err := os.Open(path)
	if err != nil {
		return iobindings.PushingError(c, err)
	}
	defer d.Close()
	// Check that this is a directory
	if st, err := d.Stat(); err != nil {
		return iobindings.PushingError(c, err)
	} else if !st.IsDir() {
		return iobindings.PushingError(c, errors.New(path+": not a directory"))
	}
	// Read the directory listing
	names, err := d.Readdirnames(0)
	if err != nil {
		return iobindings.PushingNextResult(c, nil, err)
	}
	// Package the listing up as an array and return
	t := rt.NewTable()
	for i, name := range names {
		t.Set(rt.Int(i+1), rt.String(name))
	}
	return c.PushingNext(t), nil
}

// readfile returns the contents of the file with given path.
//
// Warning: Calling readfile on a large amount of data can potentially exhaust the available memory.
// Args: path
func readfile(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	// Recover the path
	if err := c.Check1Arg(); err != nil {
		return nil, err
	}
	path, err := c.StringArg(0)
	if err != nil {
		return nil, err
	} else if len(path) == 0 {
		return nil, errors.New("#1 must not be the empty string")
	}
	// Open the file for reading
	fh, e := os.Open(string(path))
	if e != nil {
		return iobindings.PushingError(c, e)
	}
	defer fh.Close()
	// Read the contents of the file
	s, e := iobindings.ReadAll(bufio.NewReader(fh), t)
	return iobindings.PushingNextResult(c, rt.String(s), e)
}

// writefile writes to the file with given path. If the file does not exist, writefile creates it; otherwise writefile truncates it before writing.
// Args: path [val1 [... valn]]
func writefile(path string, c *rt.GoCont) (rt.Cont, error) {
	// Open the file for writing
	fh, err := os.Create(path)
	if err != nil {
		return iobindings.PushingError(c, err)
	}
	// Write the values
	if err := iobindings.WriteValues(fh, c.Etc()); err != nil {
		fh.Close()
		return iobindings.PushingError(c, err)
	}
	// Close the file
	return iobindings.PushingNextResult(c, rt.Bool(true), fh.Close())
}

// appendfile writes to the end of the file with given path. If the file does not exist, appendfile creates it.
// Args: path [val1 [... valn]]
func appendfile(path string, c *rt.GoCont) (rt.Cont, error) {
	// Open the file for writing
	fh, err := os.OpenFile(path, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0666)
	if err != nil {
		return iobindings.PushingError(c, err)
	}
	// Write the values
	if err := iobindings.WriteValues(fh, c.Etc()); err != nil {
		fh.Close()
		return iobindings.PushingError(c, err)
	}
	// Close the file
	return iobindings.PushingNextResult(c, rt.Bool(true), fh.Close())
}

// exists returns true if a file or directory exists with the given path.
// Args: path
func exists(path string, c *rt.GoCont) (rt.Cont, error) {
	ok, err := file.Exists(path)
	return iobindings.PushingNextResult(c, rt.Bool(ok), err)
}

// isdir returns true if a directory exists with the given path.
// Args: path
func isdir(path string, c *rt.GoCont) (rt.Cont, error) {
	ok, err := file.IsDir(path)
	return iobindings.PushingNextResult(c, rt.Bool(ok), err)
}

// isfile returns true if a file exists with the given path.
// Args: path
func isfile(path string, c *rt.GoCont) (rt.Cont, error) {
	ok, err := file.IsRegular(path)
	return iobindings.PushingNextResult(c, rt.Bool(ok), err)
}

// emptydir returns true if a directory exists with the given path and is empty. If no path is given then the current working directory is used.
// Args: [path]
func emptydir(_ *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	// Recover the path
	var path string
	if c.NArgs() > 0 && !rt.IsNil(c.Arg(0)) {
		s, err := c.StringArg(0)
		if err != nil {
			return nil, err
		} else if len(s) == 0 {
			return nil, errors.New("#1 must not be the empty string")
		}
		path = string(s)
	} else {
		var err error
		if path, err = os.Getwd(); err != nil {
			return nil, err
		}
	}
	// Open the path
	d, err := os.Open(path)
	if err != nil {
		return iobindings.PushingError(c, err)
	}
	defer d.Close()
	// Check that this is a directory
	if st, err := d.Stat(); err != nil {
		return iobindings.PushingError(c, err)
	} else if !st.IsDir() {
		return iobindings.PushingError(c, errors.New(path+": not a directory"))
	}
	// Is this directory empty?
	names, err := d.Readdirnames(1)
	if err != nil && err != io.EOF {
		return iobindings.PushingNextResult(c, nil, err)
	}
	return c.PushingNext(rt.Bool(len(names) == 0)), nil
}
