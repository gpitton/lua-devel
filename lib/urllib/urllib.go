// Urllib provides URL parsing.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.

You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package urllib

import (
	"bitbucket.org/pcas/golua/lib/iobindings"
	"bitbucket.org/pcas/golua/module"
	rt "bitbucket.org/pcas/golua/runtime"
	"errors"
	"fmt"
	"net/url"
	"strconv"
)

// registryKeyType is a private struct used to define a unique key in the registry.
type registryKeyType struct{}

var registryKey = registryKeyType{}

// registryData is the data we store in the registry, indexed by registryKey.
type registryData struct {
	metatable *rt.Table // The metatable for a *url.URL
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// urlMethod wraps the GoFunction f with a check that arg(0) is a *url.URL.
func urlMethod(f func(*rt.Thread, *rt.GoCont) (rt.Cont, error)) func(*rt.Thread, *rt.GoCont) (rt.Cont, error) {
	return func(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
		if err := c.Check1Arg(); err != nil {
			return nil, err
		} else if _, err := URLArg(c, 0); err != nil {
			return nil, err
		}
		return f(t, c)
	}
}

// wrapFunc wraps a function, converting it into a GoFunction.
func wrapFunc(f func(u *url.URL) rt.Value) func(*rt.Thread, *rt.GoCont) (rt.Cont, error) {
	return func(_ *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
		if err := c.Check1Arg(); err != nil {
			return nil, err
		}
		u, err := URLArg(c, 0)
		if err != nil {
			return nil, err
		}
		return c.PushingNext(f(u)), nil
	}
}

/////////////////////////////////////////////////////////////////////////
// registryData functions
/////////////////////////////////////////////////////////////////////////

// getRegistryData returns the package data stored in the registry.
func getRegistryData(t *rt.Thread) *registryData {
	return t.Registry(registryKey).(*registryData)
}

/////////////////////////////////////////////////////////////////////////
// Lua bindings
/////////////////////////////////////////////////////////////////////////

// init registers the module ready for loading into a Lua runtime.
func init() {
	module.Register(module.Info{
		Name:        "url",
		Description: "Parses URLs and implements query escaping.",
		Load: func(r *rt.Runtime) (*rt.Table, error) {
			// Save the registry data
			r.SetRegistry(registryKey, &registryData{
				metatable: createMetatable(),
			})
			// Create the package table
			pkg := rt.NewTable()
			rt.SetEnvGoFunc(pkg, "parse", parse, 1, false)
			return pkg, nil
		},
	})
}

// createMetatable creates the metatable.
func createMetatable() *rt.Table {
	// Create the methods
	methods := rt.NewTable()
	rt.SetEnvGoFunc(methods, "scheme", wrapFunc(scheme), 1, false)
	rt.SetEnvGoFunc(methods, "hostname", wrapFunc(hostname), 1, false)
	rt.SetEnvGoFunc(methods, "hasport", wrapFunc(hasport), 1, false)
	rt.SetEnvGoFunc(methods, "port", wrapFunc(port), 1, false)
	rt.SetEnvGoFunc(methods, "path", wrapFunc(path), 1, false)
	rt.SetEnvGoFunc(methods, "hasusername", wrapFunc(hasusername), 1, false)
	rt.SetEnvGoFunc(methods, "username", wrapFunc(username), 1, false)
	rt.SetEnvGoFunc(methods, "haspassword", wrapFunc(haspassword), 1, false)
	rt.SetEnvGoFunc(methods, "password", wrapFunc(password), 1, false)
	rt.SetEnvGoFunc(methods, "fragment", wrapFunc(fragment), 1, false)
	rt.SetEnvGoFunc(methods, "query", wrapFunc(query), 1, false)
	rt.SetEnvGoFunc(methods, "string", urlMethod(iobindings.ToString), 1, false)
	// Create the metatable
	meta := rt.NewTable()
	rt.SetEnv(meta, "__index", methods)
	rt.SetEnvGoFunc(meta, "__tostring", urlMethod(iobindings.ToString), 1, false)
	return meta
}

// parse parses a value into a URL, returning the URL on success, nil and a description of the error otherwise.
// Args: input
func parse(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	if err := c.Check1Arg(); err != nil {
		return nil, err
	}
	var u *url.URL
	var err error
	v := c.Arg(0)
	switch x := v.(type) {
	case rt.String:
		u, err = url.Parse(string(x))
	case *rt.UserData:
		var ok bool
		if u, ok = ValueToURL(x); !ok {
			err = errors.New("#1 must be a string or a URL")
		}
	default:
		err = errors.New("#1 must be a string or a URL")
	}
	if err != nil {
		return iobindings.PushingError(c, err)
	}
	return c.PushingNext(rt.NewUserData(u, getRegistryData(t).metatable)), nil
}

// scheme returns the scheme for u.
func scheme(u *url.URL) rt.Value {
	return rt.String(u.Scheme)
}

// hostname returns the hostname for u.
func hostname(u *url.URL) rt.Value {
	return rt.String(u.Hostname())
}

// hasport returns true iff u has a port set.
func hasport(u *url.URL) rt.Value {
	p := u.Port()
	if len(p) == 0 {
		return rt.Bool(false)
	}
	_, err := strconv.Atoi(p)
	return rt.Bool(err == nil)
}

// port returns the port for u, or 0 if not set.
func port(u *url.URL) rt.Value {
	p := u.Port()
	if len(p) == 0 {
		return rt.Int(0)
	}
	n, err := strconv.Atoi(p)
	if err != nil {
		return rt.Int(0)
	}
	return rt.Int(n)
}

// path returns the (escaped) path for u.
func path(u *url.URL) rt.Value {
	return rt.String(u.EscapedPath())
}

// hasusername returns true iff u has a username set.
func hasusername(u *url.URL) rt.Value {
	return rt.Bool(u.User != nil)
}

// username returns the username set on u, or an empty string if no username is set. Since an empty username may also be set, you should use hasusername to determine whether a username was set.
func username(u *url.URL) rt.Value {
	if u.User == nil {
		return rt.String("")
	}
	return rt.String(u.User.Username())
}

// haspassword returns true iff u has a password set.
func haspassword(u *url.URL) rt.Value {
	if u.User == nil {
		return rt.Bool(false)
	}
	_, ok := u.User.Password()
	return rt.Bool(ok)
}

// password returns the password set on u, or an empty string if no password is set. Since an empty password may also be set, you should use haspassword to determine whether a password was set.
func password(u *url.URL) rt.Value {
	if u.User == nil {
		return rt.String("")
	}
	s, _ := u.User.Password()
	return rt.String(s)
}

// fragment return the fragment for references set on u, without '#'.
func fragment(u *url.URL) rt.Value {
	return rt.String(u.Fragment)
}

// query returns a table of query value pairs set on u. It silently discards malformed pairs.
func query(u *url.URL) rt.Value {
	t := rt.NewTable()
	for k, v := range u.Query() {
		tv := rt.NewTable()
		for i, s := range v {
			tv.Set(rt.Int(i+1), rt.String(s))
		}
		t.Set(rt.String(k), tv)
	}
	return t
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// URLArg turns a continuation argument into a *url.URL.
func URLArg(c *rt.GoCont, n int) (*url.URL, error) {
	u, ok := ValueToURL(c.Arg(n))
	if !ok {
		return nil, fmt.Errorf("#%d must be a URL", n+1)
	}
	return u, nil
}

// ValueToURL turns a lua value to a *url.URL, if possible.
func ValueToURL(v rt.Value) (*url.URL, bool) {
	u, ok := v.(*rt.UserData)
	if !ok {
		return nil, false
	}
	uu, ok := u.Value().(*url.URL)
	return uu, ok
}
