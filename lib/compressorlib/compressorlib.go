// Compressorlib provides stream compression.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.

You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package compressorlib

import (
	"bitbucket.org/pcas/golua/lib/iobindings"
	"bitbucket.org/pcas/golua/module"
	rt "bitbucket.org/pcas/golua/runtime"
	"bitbucket.org/pcastools/compress"
	"fmt"
	"sort"
	"strings"
	// The compressors we need
	_ "bitbucket.org/pcastools/compress/gzip"
	_ "bitbucket.org/pcastools/compress/snappy"
	_ "bitbucket.org/pcastools/compress/zlib"
)

// registryKeyType is a private struct used to define a unique key in the registry.
type registryKeyType struct{}

var registryKey = registryKeyType{}

// registryData is the data we store in the registry, indexed by registryKey.
type registryData struct {
	compressMt   *rt.Table // The metatable for a compressor
	decompressMt *rt.Table // The metatable for a decompressor
}

// compressionMap is a map from compression names to compression types.
var compressionMap = map[string]compress.Type{
	"snappy": compress.Snappy,
	"gzip":   compress.Gzip,
	"zlib":   compress.Zlib,
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// getRegistryData returns the package data stored in the registry.
func getRegistryData(t *rt.Thread) *registryData {
	return t.Registry(registryKey).(*registryData)
}

// createCtypes returns a table listing the valid ctypes.
func createCtypes() *rt.Table {
	S := make([]string, 0, len(compressionMap))
	for k := range compressionMap {
		S = append(S, k)
	}
	sort.Strings(S)
	t := rt.NewTable()
	for i, k := range S {
		t.Set(rt.Int(i+1), rt.String(k))
	}
	return t
}

// recoverCompressorType returns the compress.Type with name given by the n-th continuation argument.
func recoverCompressorType(c *rt.GoCont, n int) (compress.Type, error) {
	s, err := c.StringArg(n)
	if err != nil {
		return 0, err
	}
	ct, ok := compressionMap[strings.ToLower(string(s))]
	if !ok {
		return 0, fmt.Errorf("#%d is not a valid ctype", n+1)
	}
	return ct, nil
}

/////////////////////////////////////////////////////////////////////////
// Lua bindings
/////////////////////////////////////////////////////////////////////////

// init registers the module ready for loading into a Lua runtime.
func init() {
	module.Register(module.Info{
		Name:        "compressor",
		Description: "Standard compression formats (Snappy, Gzip, and Zlib).",
		Load: func(r *rt.Runtime) (*rt.Table, error) {
			// Save the registry data
			r.SetRegistry(registryKey, &registryData{
				compressMt:   createCompressorMetatable(),
				decompressMt: createDecompressorMetatable(),
			})
			// Create the package table
			pkg := rt.NewTable()
			rt.SetEnv(pkg, "ctypes", createCtypes())
			rt.SetEnvGoFunc(pkg, "compress", compressf, 2, false)
			rt.SetEnvGoFunc(pkg, "decompress", decompressf, 2, false)
			return pkg, nil
		},
	})
}

// compressf returns a new compressor wrapping the given writer using the algorithm specified by ctype. The caller must call close on the compressor when finished to ensure that any buffered data is flushed to the writer.
// Args: ctype writer
func compressf(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	// Sanity check
	if err := c.CheckNArgs(2); err != nil {
		return nil, err
	}
	// Recover the compressor type
	ct, err := recoverCompressorType(c, 0)
	if err != nil {
		return nil, err
	}
	// Recover the writer
	w, err := iobindings.WriterArg(c, 1)
	if err != nil {
		return nil, err
	}
	// Create the compressor
	cw, err := NewCompressor(ct, w)
	if err != nil {
		return nil, err
	}
	return c.PushingNext(rt.NewUserData(cw, getRegistryData(t).compressMt)), nil
}

// decompressf returns a new decompressor wrapping the given reader using the algorithm specified by ctype.
// Args: ctype reader
func decompressf(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	// Sanity check
	if err := c.CheckNArgs(2); err != nil {
		return nil, err
	}
	// Recover the compressor type
	ct, err := recoverCompressorType(c, 0)
	if err != nil {
		return nil, err
	}
	// Recover the reader
	r, err := iobindings.ReaderArg(c, 1)
	if err != nil {
		return nil, err
	}
	// Create the decompressor
	cr, err := NewDecompressor(ct, r)
	if err != nil {
		return nil, err
	}
	return c.PushingNext(rt.NewUserData(cr, getRegistryData(t).decompressMt)), nil
}
