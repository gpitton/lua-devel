// Monitor describes a cache of monitord client connections.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.

You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package monitoringlib

import (
	"bitbucket.org/pcas/golua/lib/iobindings"
	"bitbucket.org/pcas/golua/lib/oslib"
	rt "bitbucket.org/pcas/golua/runtime"
	"bitbucket.org/pcas/logger/monitor"
	"bitbucket.org/pcas/logger/monitor/monitord"
	"bitbucket.org/pcastools/cleanup"
	"bitbucket.org/pcastools/contextutil"
	"bitbucket.org/pcastools/refcount"
	"context"
	"errors"
	"fmt"
	"io"
	"runtime"
	"sort"
	"time"
)

// cache is a cache of monitord client connections.
var cache = &refcount.Cache{}

// offsetKey is the key for the offset value in a table.
const offsetKey = rt.String("offset")

// deletionDelay is the delay before removing an unused client connection from the cache.
const deletionDelay = 10 * time.Second

// Monitor describes a monitor.Connection.
type Monitor struct {
	uri      string              // The connection URI
	timeout  time.Duration       // The timeout
	conn     monitor.Connection  // The underlying connection
	ref      *refcount.Reference // The associated reference
	isClosed bool                // Is the connection closed?
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// init sets up the cache of monitord client connections.
func init() {
	// Set the Delete function on the cache
	cache.Delete = func(x interface{}) {
		if c, ok := x.(io.Closer); ok {
			c.Close()
		}
	}
	// Set a Delay on deleting elements from the cache
	cache.Delay = deletionDelay
	// Register a cleanup function to ensure that every element remaining in the
	// cache is closed
	cleanup.Add(func() error {
		cache.Range(func(key interface{}, ref *refcount.Reference) bool {
			if c, ok := ref.Value().(io.Closer); ok {
				c.Close()
			}
			return true
		})
		return nil
	})
}

// monitorMethod wraps the GoFunction f with a check that arg(0) is a *Monitor.
func monitorMethod(f func(*rt.Thread, *rt.GoCont) (rt.Cont, error)) func(*rt.Thread, *rt.GoCont) (rt.Cont, error) {
	return func(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
		if err := c.Check1Arg(); err != nil {
			return nil, err
		} else if _, err := MonitorArg(c, 0); err != nil {
			return nil, err
		}
		return f(t, c)
	}
}

// createConnection returns a refcount.CreateFunc that will connection to the monitord server with the given client configuration and connection timeout.
func createConnection(r *rt.Runtime, cfg *monitord.ClientConfig, timeout time.Duration) refcount.CreateFunc {
	return func() (interface{}, error) {
		// Create the context
		var cancel context.CancelFunc
		ctx := context.Background()
		if timeout < 0 {
			ctx, cancel = context.WithCancel(ctx)
		} else {
			ctx, cancel = context.WithTimeout(ctx, timeout)
		}
		defer cancel()
		// Link the context to the interrupt channel
		defer contextutil.NewChannelLink(r.Interrupt(), cancel).Stop()
		// Build a client connection
		return monitord.NewClient(ctx, cfg)
	}
}

// parseStartTime attempts to convert the n-th argument to a start time.
func parseStartTime(t *rt.Thread, c *rt.GoCont, n int) (*monitor.Time, error) {
	v := c.Arg(n)
	if rt.IsNil(v) {
		return nil, nil
	}
	// Is v a table?
	if x, ok := v.(*rt.Table); ok {
		// Does the table have an 'offset' value?
		if u := x.Get(offsetKey); !rt.IsNil(u) {
			if n, ok := rt.ToInt(u); ok {
				return monitor.NewOffset(int64(n)), nil
			}
			return nil, fmt.Errorf("unable to convert #%d to an offset: '%s' must be an integer", n+1, offsetKey)
		}
		// Does the table parse to a timestamp?
		when, err := oslib.TableToTime(t, x)
		if err != nil {
			return nil, fmt.Errorf("unable to convert #%d to a start time: %w", n+1, err)
		}
		return monitor.NewTime(when), nil
	}
	// Is v an integer?
	if x, ok := rt.ToInt(v); ok {
		// We assume this is a UNIX time
		when := time.Unix(int64(x), 0).In(oslib.Location(t))
		return monitor.NewTime(when), nil
	}
	// Is v a string?
	if x, ok := rt.AsString(v); ok {
		ok, when := monitor.ParseStartTime(string(x), oslib.Location(t))
		if !ok {
			return nil, fmt.Errorf("unable to convert #%d to a start time", n+1)
		}
		return when, nil
	}
	// No luck
	return nil, fmt.Errorf("#%d must be nil, a table, an integer, or a string", n+1)
}

// parseEndTime attempts to convert the n-th argument to an end time.
func parseEndTime(t *rt.Thread, c *rt.GoCont, n int) (*monitor.Time, error) {
	v := c.Arg(n)
	if rt.IsNil(v) {
		return nil, nil
	}
	// If v a table?
	if x, ok := v.(*rt.Table); ok {
		// Does the table have an 'offset' value?
		if u := x.Get(offsetKey); !rt.IsNil(u) {
			if n, ok := rt.ToInt(u); ok {
				return monitor.NewOffset(int64(n)), nil
			}
			return nil, fmt.Errorf("unable to convert #%d to an offset: '%s' must be an integer", n+1, offsetKey)
		}
		// Does the table parse to a timestamp?
		when, err := oslib.TableToTime(t, x)
		if err != nil {
			return nil, fmt.Errorf("unable to convert #%d to an end time: %w", n+1, err)
		}
		return monitor.NewTime(when), nil
	}
	// Is v an integer?
	if x, ok := rt.ToInt(v); ok {
		// We assume this is a UNIX time
		when := time.Unix(int64(x), 0).In(oslib.Location(t))
		return monitor.NewTime(when), nil
	}
	// Is v a string?
	if x, ok := rt.AsString(v); ok {
		ok, when := monitor.ParseEndTime(string(x), oslib.Location(t))
		if !ok {
			return nil, fmt.Errorf("unable to convert #%d to an end time", n+1)
		}
		return when, nil
	}
	// No luck
	return nil, fmt.Errorf("#%d must be nil, a table, an integer, or a string", n+1)
}

// keyToString extracts the value associated with the given key in the given table, and returns it as a string.
func keyToString(t *rt.Table, key string) (string, error) {
	x := t.Get(rt.String(key))
	if rt.IsNil(x) {
		return "", nil
	}
	s, ok := rt.AsString(x)
	if !ok {
		return "", fmt.Errorf("value of '%s' must be nil or a string", key)
	}
	return string(s), nil
}

// parseNameMatchesTable attempts to convert the given table to a monitor.NameMatches.
func parseNameMatchesTable(t *rt.Table) (monitor.NameMatches, error) {
	cond := monitor.NameMatches{}
	// Is there anything to do?
	if t == nil {
		return cond, nil
	}
	// Substring
	s, err := keyToString(t, "substring")
	if err != nil {
		return cond, err
	}
	cond.Substring = s
	// NotSubstring
	s, err = keyToString(t, "not_substring")
	if err != nil {
		return cond, err
	}
	cond.NotSubstring = s
	// Regexp
	s, err = keyToString(t, "regexp")
	if err != nil {
		return cond, err
	}
	cond.Regexp = s
	// NotRegexp
	s, err = keyToString(t, "not_regexp")
	if err != nil {
		return cond, err
	}
	cond.NotRegexp = s
	// Looks good
	return cond, nil
}

// parseNameMatches attempts to convert the n-th argument to a monitor.NameMatches.
func parseNameMatches(c *rt.GoCont, n int) (monitor.NameMatches, error) {
	v := c.Arg(n)
	if rt.IsNil(v) {
		return monitor.NameMatches{}, nil
	}
	if t, ok := v.(*rt.Table); ok {
		cond, err := parseNameMatchesTable(t)
		if err != nil {
			return monitor.NameMatches{}, fmt.Errorf("unable to convert #%d to a matches table: %w", n+1, err)
		}
		return cond, nil
	}
	if s, ok := rt.AsString(v); ok {
		return monitor.NameMatches{
			Substring: string(s),
		}, nil
	}
	return monitor.NameMatches{}, fmt.Errorf("#%d must be nil, a string, or a table", n+1)
}

// parseMatchesTable attempts to convert the given table to a monitor.Matches.
func parseMatchesTable(t *rt.Table) (monitor.Matches, error) {
	cond := monitor.Matches{}
	// Is there anything to do?
	if t == nil {
		return cond, nil
	}
	// MessageSubstring
	s, err := keyToString(t, "message_substring")
	if err != nil {
		return cond, err
	}
	cond.MessageSubstring = s
	// MessageNotSubstring
	s, err = keyToString(t, "message_not_substring")
	if err != nil {
		return cond, err
	}
	cond.MessageNotSubstring = s
	// MessageRegexp
	s, err = keyToString(t, "message_regexp")
	if err != nil {
		return cond, err
	}
	cond.MessageRegexp = s
	// MessageNotRegexp
	s, err = keyToString(t, "message_not_regexp")
	if err != nil {
		return cond, err
	}
	cond.MessageNotRegexp = s
	// IdentifierSubstring
	s, err = keyToString(t, "identifier_substring")
	if err != nil {
		return cond, err
	}
	cond.IdentifierSubstring = s
	// IdentifierNotSubstring
	s, err = keyToString(t, "identifier_not_substring")
	if err != nil {
		return cond, err
	}
	cond.IdentifierNotSubstring = s
	// IdentifierRegexp
	s, err = keyToString(t, "identifier_regexp")
	if err != nil {
		return cond, err
	}
	cond.IdentifierRegexp = s
	// IdentifierNotRegexp
	s, err = keyToString(t, "identifier_not_regexp")
	if err != nil {
		return cond, err
	}
	cond.IdentifierNotRegexp = s
	// Looks good
	return cond, nil
}

// parseMatches attempts to convert the n-th argument to a monitor.Matches.
func parseMatches(c *rt.GoCont, n int) (monitor.Matches, error) {
	v := c.Arg(n)
	if rt.IsNil(v) {
		return monitor.Matches{}, nil
	}
	if t, ok := v.(*rt.Table); ok {
		cond, err := parseMatchesTable(t)
		if err != nil {
			return monitor.Matches{}, fmt.Errorf("unable to convert #%d to a matches table: %w", n+1, err)
		}
		return cond, nil
	}
	if s, ok := rt.AsString(v); ok {
		return monitor.Matches{
			MessageSubstring: string(s),
		}, nil
	}
	return monitor.Matches{}, fmt.Errorf("#%d must be nil, a string, or a table", n+1)
}

/////////////////////////////////////////////////////////////////////////
// Monitor functions
/////////////////////////////////////////////////////////////////////////

// connectToMonitordServer establishes a client connection to monitord using the given client configuration. A finaliser is set on the monitor to ensure that it is closed.
func connectToMonitordServer(t *rt.Thread, cfg *monitord.ClientConfig) (*Monitor, error) {
	// Recover the timeout value
	timeout := getRegistryData(t).GetTimeout()
	// Fetch (or establish) the connection from the cache
	ref, err := cache.LoadOrCreate(cfg.URI(), createConnection(t.Runtime, cfg, timeout))
	if err != nil {
		return nil, err
	}
	// Create the wrapped-up reference and set the finaliser
	c := &Monitor{
		uri:     cfg.URI(),
		timeout: timeout,
		conn:    ref.Value().(monitor.Connection),
		ref:     ref,
	}
	runtime.SetFinalizer(c, func(c *Monitor) { c.Close() })
	return c, nil
}

// Close prevents any further queries through this connection.
func (c *Monitor) Close() error {
	if !c.IsClosed() {
		c.isClosed = true
		c.ref = nil // Explicitly clear the ref
		runtime.SetFinalizer(c, nil)
	}
	return nil
}

// IsClosed returns true iff the connection is closed.
func (c *Monitor) IsClosed() bool {
	return c == nil || c.isClosed
}

// GetTimeout returns the timeout.
func (c *Monitor) GetTimeout() time.Duration {
	if c == nil {
		return 0
	}
	return c.timeout
}

// SetTimeout sets the timeout. A negative timeout is interpreted to mean no timeout.
func (c *Monitor) SetTimeout(timeout time.Duration) {
	if c != nil {
		c.timeout = timeout
	}
}

// IsLogName returns true iff the given string is a log name.
func (c *Monitor) IsLogName(ctx context.Context, name string) (bool, error) {
	if c.IsClosed() {
		return false, errors.New("connection closed")
	}
	if timeout := c.GetTimeout(); timeout >= 0 {
		var cancel context.CancelFunc
		ctx, cancel = context.WithTimeout(ctx, timeout)
		defer cancel()
	}
	return monitor.IsLogName(ctx, c.conn, name)
}

// LogNames returns the log names.
func (c *Monitor) LogNames(ctx context.Context) ([]string, error) {
	if c.IsClosed() {
		return nil, errors.New("connection closed")
	}
	if timeout := c.GetTimeout(); timeout >= 0 {
		var cancel context.CancelFunc
		ctx, cancel = context.WithTimeout(ctx, timeout)
		defer cancel()
	}
	return c.conn.LogNames(ctx)
}

// LogNamesWithMatches is the same as LogNames, with the exception that log names are filtered against the conditions 'cond'.
func (c *Monitor) LogNamesWithMatches(ctx context.Context, cond monitor.NameMatches) ([]string, error) {
	if c.IsClosed() {
		return nil, errors.New("connection closed")
	}
	if timeout := c.GetTimeout(); timeout >= 0 {
		var cancel context.CancelFunc
		ctx, cancel = context.WithTimeout(ctx, timeout)
		defer cancel()
	}
	return monitor.LogNamesWithMatches(ctx, c.conn, cond)
}

// Stream returns an iterator down which entries from the log 'name' are passed. Only entries falling into the range determined by start and finish will be returned. If start is unassigned then the start of the log will be used; if end is unassigned then no end is used. The caller should ensure that the returned Iterator is closed, otherwise a resource leak will result.
func (c *Monitor) Stream(ctx context.Context, name string, start *monitor.Time, finish *monitor.Time) (monitor.Iterator, error) {
	if c.IsClosed() {
		return nil, errors.New("connection closed")
	}
	if timeout := c.GetTimeout(); timeout >= 0 {
		var cancel context.CancelFunc
		ctx, cancel = context.WithTimeout(ctx, timeout)
		defer cancel()
	}
	return c.conn.Stream(ctx, name, start, finish)
}

// StreamWithMatches is the same as Stream, with the exception that entries are filtered against the conditions 'cond'.
func (c *Monitor) StreamWithMatches(ctx context.Context, name string, start *monitor.Time, finish *monitor.Time, cond monitor.Matches) (monitor.Iterator, error) {
	if c.IsClosed() {
		return nil, errors.New("connection closed")
	}
	if timeout := c.GetTimeout(); timeout >= 0 {
		var cancel context.CancelFunc
		ctx, cancel = context.WithTimeout(ctx, timeout)
		defer cancel()
	}
	return monitor.StreamWithMatches(ctx, c.conn, name, start, finish, cond)
}

// String returns a string description of the connection.
func (c *Monitor) String() string {
	if c == nil {
		return "monitor(nil)"
	}
	return "monitor(\"" + c.uri + "\")"
}

/////////////////////////////////////////////////////////////////////////
// Lua bindings
/////////////////////////////////////////////////////////////////////////

// createMonitorMetatable creates the metatable for a monitor.
func createMonitorMetatable() *rt.Table {
	// Create the methods
	methods := rt.NewTable()
	rt.SetEnvGoFunc(methods, "close", monitorMethod(iobindings.Close), 1, false)
	rt.SetEnvGoFunc(methods, "isclosed", monitorMethod(iobindings.IsClosed), 1, false)
	rt.SetEnvGoFunc(methods, "settimeout", monitorMethod(iobindings.SetTimeout), 2, false)
	rt.SetEnvGoFunc(methods, "gettimeout", monitorMethod(iobindings.GetTimeout), 1, false)
	rt.SetEnvGoFuncContext(methods, "islog", islog, 2, false)
	rt.SetEnvGoFuncContext(methods, "logs", logs, 2, false)
	rt.SetEnvGoFuncContext(methods, "stream", stream, 5, false)
	// Create the metatable
	meta := rt.NewTable()
	rt.SetEnv(meta, "__index", methods)
	rt.SetEnvGoFunc(meta, "__tostring", monitorMethod(iobindings.ToString), 1, false)
	return meta
}

// connect returns a new monitor connection to the monitord server. If config is nil then the default configuration settings will be used. The optional table config can be used to modify these defaults; in particular, config can be used to specify values for:
//	'address'	a string specifying the address to connect to;
//	'sslcert'	the SSL certificate (if any).
//
// Args: [config]
func connect(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	// Read the config
	cfg, err := defaultConfig(t)
	if err != nil {
		return nil, err
	}
	if c.NArgs() > 0 {
		v, err := c.TableArg(0)
		if err != nil {
			return nil, err
		}
		if cfg, err = tableToConfig(cfg, v); err != nil {
			return nil, err
		}
	}
	// Create the connection
	m, err := connectToMonitordServer(t, cfg)
	if err != nil {
		return iobindings.PushingError(c, err)
	}
	return c.PushingNext(rt.NewUserData(m, getRegistryData(t).monitorMt)), nil
}

// islog returns true iff the given log name exists.
// Args: monitor name
func islog(ctx context.Context, _ *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	// Sanity check
	if err := c.CheckNArgs(2); err != nil {
		return nil, err
	}
	// Recover the monitor and the name
	m, err := MonitorArg(c, 0)
	if err != nil {
		return nil, err
	}
	name, err := c.StringArg(1)
	if err != nil {
		return nil, err
	}
	// Is this a log name?
	ok, err := m.IsLogName(ctx, string(name))
	return iobindings.PushingNextResult(c, rt.Bool(ok), err)
}

// logs returns the available logs for a monitor. An optional matches value may be provided to filter the log names. If the value of matches is a string, then only those log names containing the string will be returned. If the value of matches is a table, then log names will be filtered as described below. The returned log names will be sorted in lex order.
//
// An optional matches table can be provided to filter for specific log names, with empty or nil values interpreted as meaning no condition. The matches table may set zero or more of the following:
//	'substring'		A substring all log names must contain;
//	'not_substring'	A substring all log names must not contain;
//	'regexp'		A regular expression all log names must match;
//	'not_regexp'	A regular expression all log names must not match.
//
// Note: A matches string value is equivalent to a matches table value with the 'substring' value set.
// Args: monitor [matches]
func logs(ctx context.Context, _ *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	if err := c.Check1Arg(); err != nil {
		return nil, err
	}
	m, err := MonitorArg(c, 0)
	if err != nil {
		return nil, err
	}
	cond := monitor.NameMatches{}
	if c.NArgs() >= 2 {
		if cond, err = parseNameMatches(c, 1); err != nil {
			return nil, err
		}
	}
	names, err := monitor.LogNamesWithMatches(ctx, m, cond)
	if err != nil {
		return iobindings.PushingError(c, err)
	}
	sort.Strings(names)
	tab := rt.NewTable()
	for i, name := range names {
		tab.Set(rt.Int(i+1), rt.String(name))
	}
	return c.PushingNext(tab), nil
}

// stream returns an iterator down which entries from the named log are passed. Only log entries falling into the range determined by 'start' and 'end' will be returned. An optional matches value may be provided to filter the entries. If the value of matches is a string, then only those entries whose messages containing the string will be returned. If the value of matches is a table, then entries will be filtered as described below.
//
// If 'start' is nil then the start of the log will be used; if 'start' is a table containing an integer value for 'offset' then is it interpreted as an offset from the most recent entry in the log, with a positive offset representing an offset into the past and a negative offset in the future; if 'start' is a string representing a timestamp, an integer (interpreted as a UNIX time), or a table describing a time (as generated by os.date, for example) then only log entries from that timestamp will be streamed.
//
// Similarly, if 'end' is nil then no end is used; if 'end' is a table containing an integer value for 'offset' then is it interpreted as an offset from the most recent entry in the log; if 'end' is a string representing a timestamp, an integer (a UNIX time), or a table describing a time, then only log entries before that timestamp will be streamed.
//
// The conversion of a string into a timestamp is very permissive: see the documentation at:
//	https://godoc.org/bitbucket.org/pcas/logger/monitor#ParseTime
//	https://godoc.org/bitbucket.org/pcas/logger/monitor#ParseStartTime
//	https://godoc.org/bitbucket.org/pcas/logger/monitor#ParseEndTime
//
// An optional matches table can be provided to filter for specific entries, with empty or nil values interpreted as meaning no condition. The matches table may set zero or more of the following:
//	'message_substring'		A substring all messages must contain;
//	'message_not_substring' A substring all messages must not contain;
//	'message_regexp'   		A regular expression all messages must match;
//	'message_not_regexp'   	A regular expression all messages must not match;
//	'identifier_substring' 	A substring all identifiers must contain;
//	'identifier_not_substring' A substring all identifiers must not contain;
//	'identifier_regexp'   	A regular expression all identifiers must match;
//	'identifier_not_regexp'	A regular expression all identifiers must not match.
//
// Note: A matches string value is equivalent to a matches table value with the 'message_substring' value set.
// Args: monitor logname [start [end [matches]]]
func stream(ctx context.Context, t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	// Recover the monitor
	if err := c.CheckNArgs(2); err != nil {
		return nil, err
	}
	m, err := MonitorArg(c, 0)
	if err != nil {
		return nil, err
	}
	// Recover the log name
	name, err := c.StringArg(1)
	if err != nil {
		return nil, err
	}
	// Recover the start and finish times, and the matches table
	var start, finish *monitor.Time
	cond := monitor.Matches{}
	if nargs := c.NArgs(); nargs >= 3 {
		if start, err = parseStartTime(t, c, 2); err != nil {
			return nil, err
		}
		if nargs >= 4 {
			if finish, err = parseEndTime(t, c, 3); err != nil {
				return nil, err
			}
			if nargs >= 5 {
				if cond, err = parseMatches(c, 4); err != nil {
					return nil, err
				}
			}
		}
	}
	// Open the stream iterator
	itr, err := monitor.StreamWithMatches(ctx, m, string(name), start, finish, cond)
	if err != nil {
		return iobindings.PushingError(c, err)
	}
	// Wrap the iterator up as a stream and set the timeout
	s := NewStream(itr)
	s.SetTimeout(m.GetTimeout())
	return c.PushingNext(rt.NewUserData(s, getRegistryData(t).streamMt)), nil
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// MonitorArg turns a continuation argument into a *Monitor.
func MonitorArg(c *rt.GoCont, n int) (*Monitor, error) {
	m, ok := ValueToMonitor(c.Arg(n))
	if !ok {
		return nil, fmt.Errorf("#%d must be a monitor", n+1)
	}
	return m, nil
}

// ValueToMonitor turns a lua value to a *Monitor, if possible.
func ValueToMonitor(v rt.Value) (*Monitor, bool) {
	u, ok := v.(*rt.UserData)
	if !ok {
		return nil, false
	}
	m, ok := u.Value().(*Monitor)
	return m, ok
}
