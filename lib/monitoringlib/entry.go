// Entry describes a monitor log entry.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.

You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package monitoringlib

import (
	"bitbucket.org/pcas/golua/lib/iobindings"
	"bitbucket.org/pcas/golua/lib/oslib"
	rt "bitbucket.org/pcas/golua/runtime"
	"bitbucket.org/pcas/logger/monitor"
	"bitbucket.org/pcas/lua/lib/jsonlib"
	"errors"
	"fmt"
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// entryMethod wraps the GoFunction f with a check that arg(0) is a monitor.Entry.
func entryMethod(f func(*rt.Thread, *rt.GoCont) (rt.Cont, error)) func(*rt.Thread, *rt.GoCont) (rt.Cont, error) {
	return func(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
		if err := c.Check1Arg(); err != nil {
			return nil, err
		} else if _, err := EntryArg(c, 0); err != nil {
			return nil, err
		}
		return f(t, c)
	}
}

/////////////////////////////////////////////////////////////////////////
// Lua bindings
/////////////////////////////////////////////////////////////////////////

// createEntryMetatable creates the metatable for an entry.
func createEntryMetatable() *rt.Table {
	// Create the methods
	methods := rt.NewTable()
	rt.SetEnvGoFunc(methods, "time", timef, 1, false)
	rt.SetEnvGoFunc(methods, "date", date, 2, false)
	rt.SetEnvGoFunc(methods, "identifier", identifier, 1, false)
	rt.SetEnvGoFunc(methods, "logname", logname, 1, false)
	rt.SetEnvGoFunc(methods, "message", message, 1, false)
	rt.SetEnvGoFunc(methods, "data", entrydata, 1, false)
	//	rt.SetEnvGoFunc(methods, "data", entrydata, 1, false)
	// Create the metatable
	meta := rt.NewTable()
	rt.SetEnv(meta, "__index", methods)
	rt.SetEnvGoFunc(meta, "__tostring", entryMethod(iobindings.ToString), 1, false)
	return meta
}

// timef returns the UNIX timestamp associated with an entry.
// Args: entry
func timef(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	if err := c.Check1Arg(); err != nil {
		return nil, err
	}
	e, err := EntryArg(c, 0)
	if err != nil {
		return nil, err
	}
	return c.PushingNext(rt.Int(e.T.UTC().Unix())), nil
}

// date returns a string or a table containing the timestamp associated with an entry, formatted according to the given string format.
//
// If format starts with '!', then the timestamp is formatted in Coordinated Universal Time. After this optional character, if format is the string "*t", then date returns a table with the following fields: year, month (1–12), day (1–31), hour (0–23), min (0–59), sec (0–61), millisec (0-999), wday (weekday, 1–7, Sunday is 1), and yday (day of the year, 1–366).
//
// If format is not "*t", then date returns the timestamp as a string, formatted according to the same rules os.date. If no formatting string is provided, then the default format "%c" is used.
// Args: entry [format]
func date(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	// Recover the entry
	if err := c.Check1Arg(); err != nil {
		return nil, err
	}
	e, err := EntryArg(c, 0)
	if err != nil {
		return nil, err
	}
	// Is a format string provided?
	format := "%c"
	if c.NArgs() > 1 && !rt.IsNil(c.Arg(1)) {
		s, err := c.StringArg(1)
		if err != nil {
			return nil, err
		} else if len(s) == 0 {
			return nil, errors.New("#2 must be a non-empty string")
		}
		format = string(s)
	}
	// Format the timestamp
	return c.PushingNext(oslib.Date(format, e.T, t)), nil
}

// identifier returns the identifier for an entry.
// Args: entry
func identifier(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	if err := c.Check1Arg(); err != nil {
		return nil, err
	}
	e, err := EntryArg(c, 0)
	if err != nil {
		return nil, err
	}
	return c.PushingNext(rt.String(e.Msg.Identifier)), nil
}

// logname returns the log name for an entry.
// Args: entry
func logname(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	if err := c.Check1Arg(); err != nil {
		return nil, err
	}
	e, err := EntryArg(c, 0)
	if err != nil {
		return nil, err
	}
	return c.PushingNext(rt.String(e.Msg.LogName)), nil
}

// message returns the message for an entry.
// Args: entry
func message(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	if err := c.Check1Arg(); err != nil {
		return nil, err
	}
	e, err := EntryArg(c, 0)
	if err != nil {
		return nil, err
	}
	return c.PushingNext(rt.String(e.Msg.Message)), nil
}

// entrydata returns the data for an entry, if any.
// Args: entry
func entrydata(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	if err := c.Check1Arg(); err != nil {
		return nil, err
	}
	e, err := EntryArg(c, 0)
	if err != nil {
		return nil, err
	}
	if e.Msg.Data == nil {
		return c.PushingNext(nil), nil
	}
	return c.PushingNext(jsonlib.ToValue(e.Msg.Data)), nil
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// EntryArg turns a continuation argument into a monitor.Entry.
func EntryArg(c *rt.GoCont, n int) (monitor.Entry, error) {
	e, ok := ValueToEntry(c.Arg(n))
	if !ok {
		return monitor.Entry{}, fmt.Errorf("#%d must be an entry", n+1)
	}
	return e, nil
}

// ValueToEntry turns a lua value to a monitor.Entry, if possible.
func ValueToEntry(v rt.Value) (monitor.Entry, bool) {
	u, ok := v.(*rt.UserData)
	if !ok {
		return monitor.Entry{}, false
	}
	e, ok := u.Value().(monitor.Entry)
	return e, ok
}
