// Stream describes a monitor stream.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.

You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package monitoringlib

import (
	"bitbucket.org/pcas/golua/lib/iobindings"
	rt "bitbucket.org/pcas/golua/runtime"
	"bitbucket.org/pcas/logger/monitor"
	"context"
	"fmt"
	"io"
	"runtime"
	"time"
)

// iteratorBufferSize is the size of the iterator buffer.
const iteratorBufferSize = 1024

// Stream describes a stream of entries as given by a monitor.Iterator.
type Stream struct {
	itr      monitor.BufferedIterator // The underlying iterator
	timeout  time.Duration            // The timeout
	isClosed bool                     // Are we closed?
	err      error                    // The error on close (if any)
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// streamMethod wraps the GoFunction f with a check that arg(0) is a *Stream.
func streamMethod(f func(*rt.Thread, *rt.GoCont) (rt.Cont, error)) func(*rt.Thread, *rt.GoCont) (rt.Cont, error) {
	return func(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
		if err := c.Check1Arg(); err != nil {
			return nil, err
		} else if _, err := StreamArg(c, 0); err != nil {
			return nil, err
		}
		return f(t, c)
	}
}

// nextEntry reads the next entry from the stream.
func nextEntry(ctx context.Context, t *rt.Thread, c *rt.GoCont, s *Stream) (rt.Cont, error) {
	// Advance the stream
	e, err := s.Advance(ctx)
	// Handle any errors
	if err != nil {
		if err == io.EOF {
			err = nil
		}
		return iobindings.PushingError(c, err)
	}
	// Return the entry
	return c.PushingNext(rt.NewUserData(e, getRegistryData(t).entryMt)), nil
}

/////////////////////////////////////////////////////////////////////////
// Stream functions
/////////////////////////////////////////////////////////////////////////

// NewStream wraps the given monitor.Iterator as a stream. A finaliser is set of the stream to ensure that it is closed.
func NewStream(itr monitor.Iterator) *Stream {
	s := &Stream{
		itr:     monitor.NewBufferedIterator(itr, iteratorBufferSize),
		timeout: iobindings.NilTimeout,
	}
	runtime.SetFinalizer(s, func(s *Stream) { s.Close() })
	return s
}

// Close closes the stream.
func (s *Stream) Close() error {
	// Sanity check
	if s == nil {
		return nil
	}
	// Is there anything to do?
	if !s.isClosed {
		s.isClosed = true
		err := s.itr.Close()
		if err == nil {
			err = s.itr.Err()
		}
		s.err = err
		runtime.SetFinalizer(s, nil)
	}
	return s.err
}

// IsClosed returns true iff the stream is closed.
func (s *Stream) IsClosed() bool {
	return s == nil || s.isClosed
}

// GetTimeout returns the timeout.
func (s *Stream) GetTimeout() time.Duration {
	if s == nil {
		return 0
	}
	return s.timeout
}

// SetTimeout sets the timeout. A negative timeout is interpreted to mean no timeout.
func (s *Stream) SetTimeout(timeout time.Duration) {
	if s != nil {
		s.timeout = timeout
	}
}

// Buffered returns true iff the next call to Advance is guaranteed not to block.
func (s *Stream) Buffered() bool {
	return s.IsClosed() || s.itr.Buffered()
}

// Advance advances the stream. If the end of the stream is reached, this will return io.EOF.
func (s *Stream) Advance(ctx context.Context) (e monitor.Entry, err error) {
	// Is there anything to do?
	if s.IsClosed() {
		err = io.EOF
		return
	}
	// Defer closing the stream
	defer func() {
		if err != nil {
			if err == io.EOF {
				if closeErr := s.Close(); closeErr != nil {
					err = closeErr
				}
			} else if err != context.DeadlineExceeded && err != context.Canceled {
				s.Close()
			}
		}
	}()
	// Is the next entry already in the buffer?
	var ok bool
	if s.itr.Buffered() {
		ok = s.itr.Next()
	} else {
		// If we have a non-negative timeout, create a context with timeout
		if timeout := s.GetTimeout(); timeout >= 0 {
			var cancel context.CancelFunc
			ctx, cancel = context.WithTimeout(ctx, timeout)
			defer cancel()
		}
		// Fetch the next entry
		ok, err = s.itr.NextContext(ctx)
		if err != nil {
			return
		}
	}
	// Handle the end of the stream
	if !ok {
		err = io.EOF
		return
	}
	// Read in the next entry
	err = s.itr.Scan(&e)
	return
}

// String returns a string representation of a stream.
func (s *Stream) String() string {
	if s == nil {
		return "stream(nil)"
	}
	return "stream"
}

/////////////////////////////////////////////////////////////////////////
// Lua bindings
/////////////////////////////////////////////////////////////////////////

// createStreamMetatable creates the metatable for a stream.
func createStreamMetatable() *rt.Table {
	// Create the methods
	methods := rt.NewTable()
	rt.SetEnvGoFunc(methods, "close", streamMethod(iobindings.Close), 1, false)
	rt.SetEnvGoFunc(methods, "isclosed", streamMethod(iobindings.IsClosed), 1, false)
	rt.SetEnvGoFunc(methods, "settimeout", streamMethod(iobindings.SetTimeout), 2, false)
	rt.SetEnvGoFunc(methods, "gettimeout", streamMethod(iobindings.GetTimeout), 1, false)
	rt.SetEnvGoFunc(methods, "buffered", buffered, 1, false)
	rt.SetEnvGoFuncContext(methods, "read", read, 1, false)
	rt.SetEnvGoFunc(methods, "entries", entries, 1, false)
	// Create the metatable
	meta := rt.NewTable()
	rt.SetEnv(meta, "__index", methods)
	rt.SetEnvGoFunc(meta, "__tostring", streamMethod(iobindings.ToString), 1, false)
	return meta
}

// buffered returns true iff the next call to read is guaranteed not to block.
// Args: stream
func buffered(_ *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	if err := c.Check1Arg(); err != nil {
		return nil, err
	}
	s, err := StreamArg(c, 0)
	if err != nil {
		return nil, err
	}
	return c.PushingNext(rt.Bool(s.Buffered())), nil
}

// read reads the next entry from the stream.
// Args: stream
func read(ctx context.Context, t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	if err := c.Check1Arg(); err != nil {
		return nil, err
	}
	s, err := StreamArg(c, 0)
	if err != nil {
		return nil, err
	}
	return nextEntry(ctx, t, c, s)
}

// entries returns a function providing iteration over all entries in the stream.
// Args: stream
func entries(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	// Recover the stream
	if err := c.Check1Arg(); err != nil {
		return nil, err
	}
	s, err := StreamArg(c, 0)
	if err != nil {
		return nil, err
	}
	// Create the iterator
	iterator := func(ctx context.Context, _ *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
		return nextEntry(ctx, t, c, s)
	}
	// Return the iterator
	return c.PushingNext(
		rt.NewGoFunctionContext(iterator, "entriesiterator", 0, false),
	), nil
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// StreamArg turns a continuation argument into a *Stream.
func StreamArg(c *rt.GoCont, n int) (*Stream, error) {
	s, ok := ValueToStream(c.Arg(n))
	if !ok {
		return nil, fmt.Errorf("#%d must be a stream", n+1)
	}
	return s, nil
}

// ValueToStream turns a lua value to a *Stream, if possible.
func ValueToStream(v rt.Value) (*Stream, bool) {
	u, ok := v.(*rt.UserData)
	if !ok {
		return nil, false
	}
	s, ok := u.Value().(*Stream)
	return s, ok
}
