// Logger describes a Lua logger.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.

You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package logginglib

import (
	"bitbucket.org/pcas/golua/lib/base"
	"bitbucket.org/pcas/golua/lib/iobindings"
	rt "bitbucket.org/pcas/golua/runtime"
	"bitbucket.org/pcas/logger"
	"bitbucket.org/pcas/logger/logd"
	"bitbucket.org/pcastools/cleanup"
	"bitbucket.org/pcastools/contextutil"
	"bitbucket.org/pcastools/log"
	"bitbucket.org/pcastools/refcount"
	"context"
	"fmt"
	"io"
	"strings"
	"time"
)

// A cache of logd client connections.
var cache = &refcount.Cache{}

// deletionDelay is the delay before removing an unused client connection from the cache.
const deletionDelay = 10 * time.Second

// Logger wraps a logger.Logger.
type Logger struct {
	*logger.Logger
	prefixFormat string              // The prefix format string
	prefix       []interface{}       // The prefix values
	suffixFormat string              // The suffix format string
	suffix       []interface{}       // The suffix values
	timeout      time.Duration       // The timeout
	ref          *refcount.Reference // The associated reference
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// init sets up the cache of logd client connections.
func init() {
	// Set the Delete function on the cache
	cache.Delete = func(x interface{}) {
		if c, ok := x.(io.Closer); ok {
			c.Close()
		}
	}
	// Set a Delay on deleting elements from the cache
	cache.Delay = deletionDelay
	// Register a cleanup function to ensure that every element remaining in the
	// cache is closed
	cleanup.Add(func() error {
		cache.Range(func(key interface{}, ref *refcount.Reference) bool {
			if c, ok := ref.Value().(io.Closer); ok {
				c.Close()
			}
			return true
		})
		return nil
	})
}

// loggerMethod wraps the GoFunction f with a check that arg(0) is a *Logger.
func loggerMethod(f func(*rt.Thread, *rt.GoCont) (rt.Cont, error)) func(*rt.Thread, *rt.GoCont) (rt.Cont, error) {
	return func(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
		if err := c.Check1Arg(); err != nil {
			return nil, err
		} else if _, err := LoggerArg(c, 0); err != nil {
			return nil, err
		}
		return f(t, c)
	}
}

// createConnection returns a refcount.CreateFunc that will connection to the logd server with the given logd.ClientConfig and timeout.
func createConnection(r *rt.Runtime, c *logd.ClientConfig, timeout time.Duration) refcount.CreateFunc {
	return func() (interface{}, error) {
		// Create the context
		var cancel context.CancelFunc
		ctx := context.Background()
		if timeout < 0 {
			ctx, cancel = context.WithCancel(ctx)
		} else {
			ctx, cancel = context.WithTimeout(ctx, timeout)
		}
		defer cancel()
		// Link the context to the interrupt channel
		defer contextutil.NewChannelLink(r.Interrupt(), cancel).Stop()
		// Build a client connection
		return logd.NewClient(ctx, c)
	}
}

// wrapMessageFunc returns a message function wrapping the given message function that will use the timeout from the given logger.
func wrapMessageFunc(f logger.MessageFunc, l *Logger) logger.MessageFunc {
	return func(ctx context.Context, m logger.Message) error {
		// Set the timeout
		timeout := l.GetTimeout()
		if timeout >= 0 {
			var cancel context.CancelFunc
			ctx, cancel = context.WithTimeout(ctx, timeout)
			defer cancel()
		}
		// If necessary, add the prefix to the message
		if len(l.prefixFormat) != 0 {
			m.Message = fmt.Sprintf(l.prefixFormat, l.prefix...) + " " + m.Message
		}
		// If necessary, add the suffix to the message
		if len(l.suffixFormat) != 0 {
			m.Message = m.Message + " " + fmt.Sprintf(l.suffixFormat, l.suffix...)
		}
		// Log the message
		return f(ctx, m)
	}
}

// connectToLogdServer returns a Logger reporting to the logd server.
func connectToLogdServer(t *rt.Thread, logName string, cfg *config) (*Logger, error) {
	// Recover the timeout value
	timeout := getRegistryData(t).GetTimeout()
	// Fetch (or establish) the connection from the cache
	ref, err := cache.LoadOrCreate(
		cfg.URI(),
		createConnection(t.Runtime, cfg.ClientConfig, timeout),
	)
	if err != nil {
		return nil, err
	}
	// Create a new logger
	l := &Logger{
		timeout: timeout,
		ref:     ref,
	}
	// Wrap the connection up as a logger.Logger and return
	l.Logger = logger.New(cfg.identifier, logName, wrapMessageFunc(
		ref.Value().(logger.LogMessager).LogMessage,
		l,
	))
	return l, nil
}

// prepareValuesForPrinting returns a slice ready for printing the given values.
func prepareValuesForPrinting(t *rt.Thread, vs []rt.Value) ([]interface{}, error) {
	S := make([]interface{}, 0, len(vs))
	for _, v := range vs {
		switch x := v.(type) {
		case rt.String:
			S = append(S, string(x))
		case rt.Int:
			S = append(S, int64(x))
		case rt.Float:
			S = append(S, float64(x))
		case rt.Bool:
			S = append(S, bool(x))
		default:
			s, err := base.ToString(t, v)
			if err != nil {
				return nil, err
			}
			S = append(S, string(s))
		}
	}
	return S, nil
}

// createFormatString returns a format string of the given length.
func createFormatString(N int) string {
	s := make([]string, 0, N)
	for i := 0; i < N; i++ {
		s = append(s, "%v")
	}
	return strings.Join(s, " ")
}

/////////////////////////////////////////////////////////////////////////
// Logger functions
/////////////////////////////////////////////////////////////////////////

// GetTimeout returns the timeout.
func (l *Logger) GetTimeout() time.Duration {
	if l == nil {
		return 0
	}
	return l.timeout
}

// SetTimeout sets the timeout. A negative timeout is interpreted to mean no timeout.
func (l *Logger) SetTimeout(timeout time.Duration) {
	if l != nil {
		l.timeout = timeout
	}
}

// PrefixWith returns a new logger, wrapping the given logger l, that automatically prepends the given data to all log messages.
func (l *Logger) PrefixWith(format string, v ...interface{}) *Logger {
	// Is there anything to do?
	if l == nil || len(format) == 0 {
		return l
	}
	// If l has existing prefix data, make a copy
	prefix := make([]interface{}, len(l.prefix)+len(v))
	copy(prefix, l.prefix)
	copy(prefix[len(l.prefix):], v)
	if len(l.prefixFormat) != 0 {
		format = l.prefixFormat + " " + format
	}
	// Create a new logger
	lnew := &Logger{
		prefixFormat: format,
		prefix:       prefix,
		suffixFormat: l.suffixFormat,
		suffix:       l.suffix,
		timeout:      l.timeout,
		ref:          l.ref,
	}
	// Wrap the connection up as a logger.Logger and return
	lnew.Logger = logger.New(l.Identifier(), l.LogName(), wrapMessageFunc(
		lnew.ref.Value().(logger.LogMessager).LogMessage,
		lnew,
	))
	return lnew
}

// With returns a new logger, wrapping the given logger l, that automatically appends the given data to all log messages.
func (l *Logger) With(format string, v ...interface{}) *Logger {
	// Is there anything to do?
	if l == nil || len(format) == 0 {
		return l
	}
	// If l has existing suffix data, make a copy
	suffix := make([]interface{}, len(v)+len(l.suffix))
	copy(suffix, v)
	copy(suffix[len(v):], l.suffix)
	if len(l.suffixFormat) != 0 {
		format += " " + l.suffixFormat
	}
	// Create a new logger
	lnew := &Logger{
		prefixFormat: l.prefixFormat,
		prefix:       l.prefix,
		suffixFormat: format,
		suffix:       suffix,
		timeout:      l.timeout,
		ref:          l.ref,
	}
	// Wrap the connection up as a logger.Logger and return
	lnew.Logger = logger.New(l.Identifier(), l.LogName(), wrapMessageFunc(
		lnew.ref.Value().(logger.LogMessager).LogMessage,
		lnew,
	))
	return lnew
}

/////////////////////////////////////////////////////////////////////////
// Lua bindings
/////////////////////////////////////////////////////////////////////////

// createLoggerMetatable creates the metatable for a logger.
func createLoggerMetatable() *rt.Table {
	// Create the methods for a logger
	methods := rt.NewTable()
	rt.SetEnvGoFunc(methods, "settimeout", loggerMethod(iobindings.SetTimeout), 2, false)
	rt.SetEnvGoFunc(methods, "gettimeout", loggerMethod(iobindings.GetTimeout), 1, false)
	rt.SetEnvGoFunc(methods, "identifier", identifier, 1, false)
	rt.SetEnvGoFunc(methods, "logname", logname, 1, false)
	rt.SetEnvGoFuncContext(methods, "print", loggerprint, 1, true)
	rt.SetEnvGoFuncContext(methods, "data", loggerdata, 2, true)
	rt.SetEnvGoFunc(methods, "prefixwith", prefixwith, 1, true)
	rt.SetEnvGoFunc(methods, "suffixwith", suffixwith, 1, true)
	// Create the metatable for a logger
	meta := rt.NewTable()
	rt.SetEnv(meta, "__index", methods)
	rt.SetEnvGoFunc(meta, "__tostring", loggerMethod(iobindings.ToString), 1, false)
	return meta
}

// identifier returns the identifier for a logger.
// Args: logger
func identifier(_ *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	if err := c.Check1Arg(); err != nil {
		return nil, err
	}
	l, err := LoggerArg(c, 0)
	if err != nil {
		return nil, err
	}
	return c.PushingNext(rt.String(l.Identifier())), nil
}

// logname returns the log name for a logger.
// Args: logger
func logname(_ *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	if err := c.Check1Arg(); err != nil {
		return nil, err
	}
	l, err := LoggerArg(c, 0)
	if err != nil {
		return nil, err
	}
	return c.PushingNext(rt.String(l.LogName())), nil
}

// loggerprint prints a message to a logger. Returns the object so that it can be reused in nested calls.
// Args: logger [v ...]
func loggerprint(ctx context.Context, t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	// Recover the logger
	if err := c.Check1Arg(); err != nil {
		return nil, err
	}
	l, err := LoggerArg(c, 0)
	if err != nil {
		return nil, err
	}
	// Convert the arguments into more familiar go types
	S, err := prepareValuesForPrinting(t, c.Etc())
	if err != nil {
		return nil, err
	}
	// Forward the arguments to the logger
	e := l.PrintContext(ctx, S...)
	return iobindings.PushingNextResult(c, c.Arg(0), e)
}

// loggerdata prints the data, plus any message, to a logger. Returns the object so that it can be reused in nested calls.
//
// JSON cannot represent cyclic data structures and data does not handle them. Passing cyclic structures to data will result in an infinite recursion.
// Args: logger data [v ...]
func loggerdata(ctx context.Context, t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	// Recover the logger
	if err := c.CheckNArgs(2); err != nil {
		return nil, err
	}
	l, err := LoggerArg(c, 0)
	if err != nil {
		return nil, err
	}
	// Convert the arguments into more familiar go types
	S, err := prepareValuesForPrinting(t, c.Etc())
	if err != nil {
		return nil, err
	}
	// Create the format string
	format := createFormatString(len(S))
	// Forward the arguments to the logger
	err = l.JSONContext(ctx, c.Arg(1), format, S...)
	return iobindings.PushingNextResult(c, c.Arg(0), err)
}

// prefixwith returns a new PrefixWith logger.
// Args: logger [v...]
func prefixwith(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	// Sanity check
	if err := c.Check1Arg(); err != nil {
		return nil, err
	}
	// Recover the logger
	l, err := LoggerArg(c, 0)
	if err != nil {
		return nil, err
	}
	S, err := prepareValuesForPrinting(t, c.Etc())
	if err != nil {
		return nil, err
	}
	// Create the prefix logger
	l = l.PrefixWith(createFormatString(len(S)), S...)
	return c.PushingNext(rt.NewUserData(l, getRegistryData(t).metatable)), nil
}

// suffixwith returns a new With logger.
// Args: logger [v...]
func suffixwith(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	// Sanity check
	if err := c.Check1Arg(); err != nil {
		return nil, err
	}
	// Reco ver the logger
	l, err := LoggerArg(c, 0)
	if err != nil {
		return nil, err
	}
	S, err := prepareValuesForPrinting(t, c.Etc())
	if err != nil {
		return nil, err
	}
	// Create the suffix logger
	l = l.With(createFormatString(len(S)), S...)
	return c.PushingNext(rt.NewUserData(l, getRegistryData(t).metatable)), nil
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// LoggerArg turns a continuation argument into a *Logger.
func LoggerArg(c *rt.GoCont, n int) (*Logger, error) {
	l, ok := ValueToLogger(c.Arg(n))
	if !ok {
		return nil, fmt.Errorf("#%d must be a logger", n+1)
	}
	return l, nil
}

// ValueToLogger turns a lua value to a *Logger, if possible.
func ValueToLogger(v rt.Value) (*Logger, bool) {
	u, ok := v.(*rt.UserData)
	if !ok {
		return nil, false
	}
	l, ok := u.Value().(*Logger)
	return l, ok
}

// ValueToSetLoggerer turns a lua value to a log.SetLoggerer, if possible.
func ValueToSetLoggerer(v rt.Value) (log.SetLoggerer, bool) {
	u, ok := v.(*rt.UserData)
	if !ok {
		return nil, false
	}
	l, ok := u.Value().(log.SetLoggerer)
	return l, ok
}

// SetLogger sets the logger on a lua value, if the value supports the log.SetLoggerer interface. Returns true on success, false otherwise.
func SetLogger(v rt.Value, lg log.Interface) bool {
	l, ok := ValueToSetLoggerer(v)
	if ok {
		l.SetLogger(lg)
	}
	return ok
}
