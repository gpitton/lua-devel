// Keyvaluelib contains Lua glue for the keyvalue package.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.

You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package keyvaluelib

import (
	"bitbucket.org/pcas/golua/lib/iobindings"
	"bitbucket.org/pcas/golua/module"
	rt "bitbucket.org/pcas/golua/runtime"
	"bitbucket.org/pcas/keyvalue"
	"sync"
	"time"
)

// registryKeyType is a private struct used to define a unique key in the registry.
type registryKeyType struct{}

var registryKey = registryKeyType{}

// registryData is the data we store in the registry, indexed by registryKey.
type registryData struct {
	timeout      time.Duration // The default timeout
	connectionMt *rt.Table     // The metatable for a connection
	tableMt      *rt.Table     // The metatable for a table
	cursorMt     *rt.Table     // The metatable for a cursor
}

// A map of registered package function loaders and controlling mutex.
var (
	regm = sync.Mutex{}
	reg  = make(map[string]func() *rt.Table)
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// createDriverArray returns an array of the registered keyvalue drivers.
func createDriverArray() *rt.Table {
	t := rt.NewTable()
	for i, name := range keyvalue.Drivers() {
		t.Set(rt.Int(i+1), rt.String(name))
	}
	return t
}

/////////////////////////////////////////////////////////////////////////
// registryData functions
/////////////////////////////////////////////////////////////////////////

// getRegistryData returns the package data stored in the registry.
func getRegistryData(t *rt.Thread) *registryData {
	return t.Registry(registryKey).(*registryData)
}

// SetTimeout sets the default timeout.
func (r *registryData) SetTimeout(timeout time.Duration) {
	r.timeout = timeout
}

// GetTimeout returns the default timeout.
func (r *registryData) GetTimeout() time.Duration {
	return r.timeout
}

/////////////////////////////////////////////////////////////////////////
// Lua bindings
/////////////////////////////////////////////////////////////////////////

// init registers the module ready for loading into a Lua runtime.
func init() {
	// Register this module
	module.Register(module.Info{
		Name:        "keyvalue",
		Description: "Provides keyvalue database functionality.",
		Load: func(r *rt.Runtime) (*rt.Table, error) {
			// Save the registry data
			r.SetRegistry(registryKey, &registryData{
				timeout:      iobindings.NilTimeout,
				connectionMt: createConnectionMetatable(),
				tableMt:      createTableMetatable(),
				cursorMt:     createCursorMetatable(),
			})
			// Create the package table
			pkg := rt.NewTable()
			// Add the registered tables
			regm.Lock()
			defer regm.Unlock()
			for name, f := range reg {
				rt.SetEnv(pkg, name, f())
			}
			// Add our package values
			rt.SetEnv(pkg, "drivers", createDriverArray())
			rt.SetEnvGoFunc(pkg, "settimeout", settimeout, 1, false)
			rt.SetEnvGoFunc(pkg, "gettimeout", gettimeout, 0, false)
			rt.SetEnvGoFunc(pkg, "connect", WrapConnectFunc(connect), 1, false)
			rt.SetEnvGoFunc(pkg, "reader", reader, 2, false)
			rt.SetEnvGoFunc(pkg, "open", open, 2, false)
			return pkg, nil
		},
	})
}

// connect returns a new connection to the given data source. The caller must call close on the connection when finished.
// Args: datasource
func connect(t *rt.Thread, c *rt.GoCont) (string, error) {
	if err := c.Check1Arg(); err != nil {
		return "", err
	}
	dataSource, err := c.StringArg(0)
	if err != nil {
		return "", err
	}
	return string(dataSource), nil
}

// settimeout sets the default timeout.
func settimeout(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	return iobindings.SetTimeoutWithSetTimeouter(getRegistryData(t), c)
}

// gettimeout returns the default I/O timeout, in seconds.
func gettimeout(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	return iobindings.GetTimeoutWithGetTimeouter(getRegistryData(t), c)
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// WrapConnectFunc wraps the given function so that the returned datasource is opened and returned as a Connection.
func WrapConnectFunc(f func(*rt.Thread, *rt.GoCont) (string, error)) func(*rt.Thread, *rt.GoCont) (rt.Cont, error) {
	return func(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
		dataSource, err := f(t, c)
		if err != nil {
			return nil, err
		}
		v, err := openConnectionAsValue(dataSource, t)
		return iobindings.PushingNextResult(c, v, err)
	}
}

// Register registers the given function that returns a table to be added to the package table using the given name (typically a name of a keyvalue driver). If a function is already registered with the given name then it will be replaced; if the name coincides with one of the package functions or values then it will be overwritten. Note that any function registered after the package is loaded into the Lua runtime will not appear in that instance of the package.
func Register(name string, f func() *rt.Table) {
	regm.Lock()
	defer regm.Unlock()
	reg[name] = f
}
