// Table describes a Lua view of a keyvalue.Table.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.

You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package keyvaluelib

import (
	"bitbucket.org/pcas/golua/lib/iobindings"
	rt "bitbucket.org/pcas/golua/runtime"
	"bitbucket.org/pcas/keyvalue"
	"bitbucket.org/pcas/keyvalue/condition"
	"bitbucket.org/pcas/keyvalue/parse"
	"bitbucket.org/pcas/keyvalue/record"
	"bitbucket.org/pcas/keyvalue/sort"
	"bitbucket.org/pcas/metrics"
	"bitbucket.org/pcastools/log"
	"context"
	"errors"
	"fmt"
	"runtime"
	"time"
)

// Table wraps a keyvalue.Table.
type Table struct {
	log.Logger
	metrics.Metricser
	t        *keyvalue.Table // The underlying table
	timeout  time.Duration   // The timeout
	isClosed bool            // Is the table closed?
	closeErr error           // The error on close (if any)
}

// maxInt defines the maximum size of an int.
const maxInt = int64(int(^uint(0) >> 1))

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// tableMethod wraps the GoFunction f with a check that arg(0) is a *Table.
func tableMethod(f func(*rt.Thread, *rt.GoCont) (rt.Cont, error)) func(*rt.Thread, *rt.GoCont) (rt.Cont, error) {
	return func(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
		if err := c.Check1Arg(); err != nil {
			return nil, err
		} else if _, err := TableArg(c, 0); err != nil {
			return nil, err
		}
		return f(t, c)
	}
}

// tableFunc wraps the given function f to return a GoFunction with context.
func tableFunc(f func(context.Context, *Table, *rt.Thread, *rt.GoCont) (rt.Cont, error)) func(context.Context, *rt.Thread, *rt.GoCont) (rt.Cont, error) {
	return func(ctx context.Context, t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
		if err := c.Check1Arg(); err != nil {
			return nil, err
		}
		tab, err := TableArg(c, 0)
		if err != nil {
			return nil, err
		}
		return f(ctx, tab, t, c)
	}
}

// parseCondition attempts to parse the i-th argument into a condition.
func parseCondition(c *rt.GoCont, i int) (condition.Condition, error) {
	// Extract the condition
	var cond condition.Condition
	if c.NArgs() > i && !rt.IsNil(c.Arg(i)) {
		if s, ok := c.Arg(i).(rt.String); ok {
			// Parse the SQL statement
			var err error
			cond, err = parse.SQLWhere(string(s))
			if err != nil {
				return nil, fmt.Errorf("#%d fails to parse: %w", i+1, err)
			}
		} else {
			// Parse the record
			r, err := RecordArg(c, i)
			if err != nil {
				return nil, err
			}
			cond = condition.FromRecord(r)
		}
	}
	return cond, nil
}

// parseOrder attempts to parse the given table into a sort.Order.
func parseOrder(t *rt.Table) (sort.Order, bool) {
	// Index 1 must be set and equal to a string
	var key string
	if x := t.Get(rt.Int(1)); rt.IsNil(x) {
		return sort.Order{}, false
	} else if s, ok := x.(rt.String); !ok {
		return sort.Order{}, false
	} else {
		key = string(s)
	}
	// Index 2 may or may not be set, but if it is it must be a non-zero int
	direction := sort.Ascending
	if x := t.Get(rt.Int(2)); !rt.IsNil(x) {
		if n, ok := x.(rt.Int); !ok {
			return sort.Order{}, false
		} else if n == 0 {
			return sort.Order{}, false
		} else if n < 0 {
			direction = sort.Descending
		}
	}
	// Looks good
	return sort.Order{
		Key:       key,
		Direction: direction,
	}, true
}

// orderByArg attempts to convert the i-th argument to a sort.OrderBy.
func orderByArg(c *rt.GoCont, i int) (sort.OrderBy, error) {
	// Recover the table
	t, err := c.TableArg(i)
	if err != nil {
		return nil, err
	}
	// Start walking the array part of the table
	order := make(sort.OrderBy, 0)
	for idx := 1; ; idx++ {
		x := t.Get(rt.Int(idx))
		if rt.IsNil(x) {
			// Validate the order before returning
			if _, err = order.IsValid(); err != nil {
				return nil, fmt.Errorf("#%d does not describe a valid sort order: %w", i+1, err)
			}
			return order, nil
		}
		switch v := x.(type) {
		case rt.String:
			// Append the key
			order = append(order, sort.Order{
				Key:       string(v),
				Direction: sort.Ascending,
			})
		case *rt.Table:
			// Parse the order
			o, ok := parseOrder(v)
			if !ok {
				return nil, fmt.Errorf("#%d does not describe a valid sort order: index %d cannot be parsed", i+1, idx)
			}
			order = append(order, o)
		default:
			// Unsupported type
			return nil, fmt.Errorf("#%d does not describe a valid sort order: index %d is an invalid type", i+1, idx)
		}
	}
}

// parseQuery attempts to parse the user arguments into a query, starting with argument i.
func parseQuery(c *rt.GoCont, i int) (*parse.Query, error) {
	// Create an empty query
	q := &parse.Query{
		Cond: condition.True,
	}
	// Has the user provided an SQL statement? Or a selector?
	if c.NArgs() > i && !rt.IsNil(c.Arg(i)) {
		if s, ok := c.Arg(i).(rt.String); ok {
			// Parse the SQL statement
			var err error
			q, err = parse.SQL(string(s))
			if err != nil {
				return nil, fmt.Errorf("#%d fails to parse: %w", i+1, err)
			}
			return q, nil
		}
		// Recover the selector
		r, err := RecordArg(c, i)
		if err != nil {
			return nil, err
		}
		q.Cond = condition.FromRecord(r)
	}
	// Has the user provided an order?
	if c.NArgs() > i+1 && !rt.IsNil(c.Arg(i+1)) {
		order, err := orderByArg(c, i+1)
		if err != nil {
			return nil, err
		}
		q.Order = order
	}
	// Has the user provided a limit?
	if c.NArgs() > i+2 && !rt.IsNil(c.Arg(i+2)) {
		n, err := c.IntArg(i + 2)
		if err != nil {
			return nil, err
		} else if n < 0 {
			return nil, fmt.Errorf("#%d must be a non-negative integer", i+3)
		}
		q.HasLimit = true
		q.Limit = int64(n)
	}
	// Return the query
	return q, nil
}

/////////////////////////////////////////////////////////////////////////
// Table functions
/////////////////////////////////////////////////////////////////////////

// NewTable wraps the given keyvalue.Table as a Table. A finaliser is set on the table to ensure that it is closed.
func NewTable(t *keyvalue.Table) *Table {
	tt := &Table{
		Logger:    t,
		Metricser: t,
		t:         t,
		timeout:   iobindings.NilTimeout,
	}
	runtime.SetFinalizer(tt, func(tt *Table) { tt.Close() })
	return tt
}

// Name return the table name.
func (t *Table) Name() string {
	if t == nil {
		return ""
	}
	return t.t.Name()
}

// Close closes the connection to the table.
func (t *Table) Close() error {
	if !t.IsClosed() {
		t.isClosed = true
		t.closeErr = t.t.Close()
		runtime.SetFinalizer(t, nil)
	}
	return nil
}

// IsClosed returns true iff the connection to the table is closed.
func (t *Table) IsClosed() bool {
	return t == nil || t.t == nil || t.isClosed
}

// GetTimeout returns the timeout.
func (t *Table) GetTimeout() time.Duration {
	if t == nil {
		return 0
	}
	return t.timeout
}

// SetTimeout sets the timeout. A negative timeout is interpreted to mean no timeout.
func (t *Table) SetTimeout(timeout time.Duration) {
	if t != nil {
		t.timeout = timeout
	}
}

// Describe returns a best-guess template for the data in this table.
//
// Note that the accuracy of this template depends on the underlying storage engine. It might not be possible to return an exact description (for example, in a schema-less database), and in this case we return a good guess based on a sample of the data available.
func (t *Table) Describe(ctx context.Context) (record.Record, error) {
	if timeout := t.GetTimeout(); timeout >= 0 {
		var cancel context.CancelFunc
		ctx, cancel = context.WithTimeout(ctx, timeout)
		defer cancel()
	}
	return t.t.Describe(ctx)
}

// Count returns the number of records in the table that match "selector".
func (t *Table) Count(ctx context.Context, selector record.Record) (int64, error) {
	if timeout := t.GetTimeout(); timeout >= 0 {
		var cancel context.CancelFunc
		ctx, cancel = context.WithTimeout(ctx, timeout)
		defer cancel()
	}
	return t.t.Count(ctx, selector)
}

// CountWhere returns the number of records in the table that satisfy condition "cond" (which may be nil if there are no conditions).
func (t *Table) CountWhere(ctx context.Context, cond condition.Condition) (int64, error) {
	if timeout := t.GetTimeout(); timeout >= 0 {
		var cancel context.CancelFunc
		ctx, cancel = context.WithTimeout(ctx, timeout)
		defer cancel()
	}
	return t.t.CountWhere(ctx, cond)
}

// InsertRecords inserts the given records into the table.
func (t *Table) InsertRecords(ctx context.Context, records ...record.Record) error {
	if timeout := t.GetTimeout(); timeout >= 0 {
		var cancel context.CancelFunc
		ctx, cancel = context.WithTimeout(ctx, timeout)
		defer cancel()
	}
	return t.t.InsertRecords(ctx, records...)
}

// Insert inserts the records in the given iterator into the table.
func (t *Table) Insert(ctx context.Context, itr record.Iterator) error {
	if timeout := t.GetTimeout(); timeout >= 0 {
		var cancel context.CancelFunc
		ctx, cancel = context.WithTimeout(ctx, timeout)
		defer cancel()
	}
	return t.t.Insert(ctx, itr)
}

// Update updates all records in the table that match "selector" by setting all keys present in "replacement" to the given values. Returns the number of records updated.
func (t *Table) Update(ctx context.Context, replacement record.Record, selector record.Record) (int64, error) {
	if timeout := t.GetTimeout(); timeout >= 0 {
		var cancel context.CancelFunc
		ctx, cancel = context.WithTimeout(ctx, timeout)
		defer cancel()
	}
	return t.t.Update(ctx, replacement, selector)
}

// UpdateWhere updates all records in the table that satisfy condition "cond" (which may be nil if there are no conditions) by setting all keys present in "replacement" to the given values. Returns the number of records updated.
func (t *Table) UpdateWhere(ctx context.Context, replacement record.Record, cond condition.Condition) (int64, error) {
	if timeout := t.GetTimeout(); timeout >= 0 {
		var cancel context.CancelFunc
		ctx, cancel = context.WithTimeout(ctx, timeout)
		defer cancel()
	}
	return t.t.UpdateWhere(ctx, replacement, cond)
}

// Select returns the records matching "selector", sorted as specified by "order" (which may be nil if there is no sort order required). The returned records will be in the form specified by "template".
func (t *Table) Select(ctx context.Context, template record.Record, selector record.Record, order sort.OrderBy) (record.Iterator, error) {
	if timeout := t.GetTimeout(); timeout >= 0 {
		var cancel context.CancelFunc
		ctx, cancel = context.WithTimeout(ctx, timeout)
		defer cancel()
	}
	return t.t.Select(ctx, template, selector, order)
}

// SelectWhere returns the results satisfying condition "cond" (which may be nil if there are no conditions), sorted as specified by "order" (which may be nil if there is no sort order required). The returned records will be in the form specified by "template".
func (t *Table) SelectWhere(ctx context.Context, template record.Record, cond condition.Condition, order sort.OrderBy) (record.Iterator, error) {
	if timeout := t.GetTimeout(); timeout >= 0 {
		var cancel context.CancelFunc
		ctx, cancel = context.WithTimeout(ctx, timeout)
		defer cancel()
	}
	return t.t.SelectWhere(ctx, template, cond, order)
}

// SelectLimit returns at most n records matching "selector", sorted as specified by "order" (which may be nil if there is no sort order required). The returned records will be in the form specified by "template".
func (t *Table) SelectLimit(ctx context.Context, template record.Record, selector record.Record, order sort.OrderBy, n int64) (record.Iterator, error) {
	if timeout := t.GetTimeout(); timeout >= 0 {
		var cancel context.CancelFunc
		ctx, cancel = context.WithTimeout(ctx, timeout)
		defer cancel()
	}
	return t.t.SelectLimit(ctx, template, selector, order, n)
}

// SelectWhereLimit returns at most n results satisfying condition "cond" (which may be nil if there are no conditions), sorted as specified by "order" (which may be nil if there is no sort order required). The returned records will be in the form specified by "template".
func (t *Table) SelectWhereLimit(ctx context.Context, template record.Record, cond condition.Condition, order sort.OrderBy, n int64) (record.Iterator, error) {
	if timeout := t.GetTimeout(); timeout >= 0 {
		var cancel context.CancelFunc
		ctx, cancel = context.WithTimeout(ctx, timeout)
		defer cancel()
	}
	return t.t.SelectWhereLimit(ctx, template, cond, order, n)
}

// SelectOne returns the first record matching "selector", sorted as specified by "order" (which may be nil if there is no sort order required). The returned record will be in the form specified by "template". If no records match, then a nil record will be returned.
func (t *Table) SelectOne(ctx context.Context, template record.Record, selector record.Record, order sort.OrderBy) (record.Record, error) {
	if timeout := t.GetTimeout(); timeout >= 0 {
		var cancel context.CancelFunc
		ctx, cancel = context.WithTimeout(ctx, timeout)
		defer cancel()
	}
	return t.t.SelectOne(ctx, template, selector, order)
}

// SelectOneWhere returns the first record satisfying condition "cond" (which may be nil if there are no conditions), sorted as specified by "order" (which may be nil if there is no sort order required). The returned record will be in the form specified by "template". If no records match, then a nil record will be returned.
func (t *Table) SelectOneWhere(ctx context.Context, template record.Record, cond condition.Condition, order sort.OrderBy) (record.Record, error) {
	if timeout := t.GetTimeout(); timeout >= 0 {
		var cancel context.CancelFunc
		ctx, cancel = context.WithTimeout(ctx, timeout)
		defer cancel()
	}
	return t.t.SelectOneWhere(ctx, template, cond, order)
}

// Delete deletes those records in the table that match "selector". Returns the number of records deleted.
func (t *Table) Delete(ctx context.Context, selector record.Record) (int64, error) {
	if timeout := t.GetTimeout(); timeout >= 0 {
		var cancel context.CancelFunc
		ctx, cancel = context.WithTimeout(ctx, timeout)
		defer cancel()
	}
	return t.t.Delete(ctx, selector)
}

// DeleteWhere deletes those records in the table that satisfy condition "cond" (which may be nil if there are no conditions). Returns the number of records deleted.
func (t *Table) DeleteWhere(ctx context.Context, cond condition.Condition) (int64, error) {
	if timeout := t.GetTimeout(); timeout >= 0 {
		var cancel context.CancelFunc
		ctx, cancel = context.WithTimeout(ctx, timeout)
		defer cancel()
	}
	return t.t.DeleteWhere(ctx, cond)
}

// String returns a string description of the table.
func (t *Table) String() string {
	return "table(\"" + t.Name() + "\")"
}

/////////////////////////////////////////////////////////////////////////
// Lua bindings
/////////////////////////////////////////////////////////////////////////

// createTableMetatable creates the metatable for a table.
func createTableMetatable() *rt.Table {
	// Create the methods
	methods := rt.NewTable()
	rt.SetEnvGoFunc(methods, "settimeout", tableMethod(iobindings.SetTimeout), 2, false)
	rt.SetEnvGoFunc(methods, "gettimeout", tableMethod(iobindings.GetTimeout), 1, false)
	rt.SetEnvGoFunc(methods, "close", tableMethod(iobindings.Close), 1, false)
	rt.SetEnvGoFunc(methods, "isclosed", tableMethod(iobindings.IsClosed), 1, false)
	rt.SetEnvGoFunc(methods, "name", tablename, 1, false)
	rt.SetEnvGoFuncContext(methods, "describe", tableFunc(describe), 1, false)
	rt.SetEnvGoFuncContext(methods, "count", tableFunc(count), 2, false)
	rt.SetEnvGoFuncContext(methods, "insert", tableFunc(insert), 1, true)
	rt.SetEnvGoFuncContext(methods, "update", tableFunc(update), 3, false)
	rt.SetEnvGoFuncContext(methods, "select", tableFunc(selectf), 3, false)
	rt.SetEnvGoFuncContext(methods, "selectone", tableFunc(selectone), 3, false)
	rt.SetEnvGoFuncContext(methods, "delete", tableFunc(delete), 2, false)
	// Create the metatable
	meta := rt.NewTable()
	rt.SetEnv(meta, "__index", methods)
	rt.SetEnvGoFunc(meta, "__tostring", tableMethod(iobindings.ToString), 1, false)
	return meta
}

// tablename returns the table name.
// Args: tab
func tablename(_ *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	if err := c.Check1Arg(); err != nil {
		return nil, err
	}
	tab, err := TableArg(c, 0)
	if err != nil {
		return nil, err
	}
	return c.PushingNext(rt.String(tab.Name())), nil
}

// describe returns a template describing the contents of the table.
// Args: tab
func describe(ctx context.Context, tab *Table, _ *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	template, err := tab.Describe(ctx)
	if err != nil {
		return iobindings.PushingError(c, err)
	}
	return c.PushingNext(RecordToTable(template)), nil
}

// count returns the number of records in the table that match the record "selector". If "selector" is not specified then the total number of records in the table will be returned.
// Args: tab [selector]
//
// count returns the number of records in the table that match the given SQL WHERE statement. If no WHERE statement is specified then the total number of records in the table will be returned.
// Args: tab [sql]
func count(ctx context.Context, tab *Table, _ *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	// Parse the condition
	cond, err := parseCondition(c, 1)
	if err != nil {
		return nil, err
	}
	// Perform the count
	n, err := tab.CountWhere(ctx, cond)
	return iobindings.PushingNextResult(c, rt.Int(n), err)
}

// insert inserts the given records into the table.
// Args: tab [rec1 [... recn]]
func insert(ctx context.Context, tab *Table, _ *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	etc := c.Etc()
	rs := make([]record.Record, 0, len(etc))
	for i, v := range etc {
		r, ok := ValueToRecord(v)
		if !ok {
			return nil, fmt.Errorf("#%d does not describe a record", i+2)
		}
		rs = append(rs, r)
	}
	return iobindings.PushingNextResult(c, rt.Bool(true), tab.InsertRecords(ctx, rs...))
}

// update updates all records in the table that match the record "selector" by setting all keys present in "replacement" to the given values. Returns the number of records updated.
// Args: tab replacement [selector]
//
// update updates all records in the table that match the given SQL WHERE statement by setting all keys present in "replacement" to the given values. Returns the number of records updated.
// Args: tab replacement [sql]
func update(ctx context.Context, tab *Table, _ *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	// Sanity check
	if err := c.CheckNArgs(2); err != nil {
		return nil, err
	}
	// Recover the replacement
	replacement, err := RecordArg(c, 1)
	if err != nil {
		return nil, err
	}
	// Parse the condition
	cond, err := parseCondition(c, 2)
	if err != nil {
		return nil, err
	}
	// Perform the update
	n, err := tab.UpdateWhere(ctx, replacement, cond)
	return iobindings.PushingNextResult(c, rt.Int(n), err)
}

// selectf returns a cursor iterating over all records matching "selector". The records returned by the cursor will be in the form specified by "template". The caller must call close on the cursor when finished. "selector" can be a record to match against (that is, a lua table) or an SQL WHERE string. The SQL string can also specify the sort order ("ORDER BY") and a limit on the number of records returned ("LIMIT").
// Args: tab template [selector:string]
// Args: tab template [selector:table] [limit:int]
func selectf(ctx context.Context, tab *Table, t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	// Sanity check
	if err := c.CheckNArgs(2); err != nil {
		return nil, err
	}
	// Recover the template
	template, err := RecordArg(c, 1)
	if err != nil {
		return nil, err
	}
	// Recover the condition, sort order, and limit
	var q *parse.Query
	if c.NArgs() >= 3 && !rt.IsNil(c.Arg(2)) {
		var err error
		if q, err = QueryArg(c, 2); err != nil {
			return nil, err
		}

	}
	if c.NArgs() >= 4 && !rt.IsNil(c.Arg(3)) {
		// The third argument should have been a table or nil
		if _, ok := c.Arg(2).(*rt.Table); !ok && !rt.IsNil(c.Arg(2)) {
			return nil, errors.New("#3 must be a table")
		}
		n, ok := rt.ToInt(c.Arg(3))
		if !ok {
			return nil, errors.New("#4 must be an integer")
		}
		q.HasLimit = true
		q.Limit = int64(n)
	}
	// Perform the select
	itr, err := tab.SelectWhere(ctx, template, q.Cond, q.Order)
	if err != nil {
		return iobindings.PushingError(c, err)
	}
	// Create and return the cursor
	cur := NewCursor(itr)
	cur.SetTimeout(tab.GetTimeout())
	return c.PushingNext(rt.NewUserData(cur, getRegistryData(t).cursorMt)), nil
}

// selectone returns the first record matching "selector". The returned record will be in the form specified by "template". If no records match, then returns nil. "selector" can be a record to match against (that is, a lua table) or an SQL WHERE string.
// Args: tab template [selector]
func selectone(ctx context.Context, tab *Table, _ *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	if err := c.CheckNArgs(2); err != nil {
		return nil, err
	}
	template, err := RecordArg(c, 1)
	if err != nil {
		return nil, err
	}
	var q *parse.Query
	if c.NArgs() >= 3 && !rt.IsNil(c.Arg(2)) {
		var err error
		if q, err = QueryArg(c, 2); err != nil {
			return nil, err
		} else if q.HasLimit {
			return nil, errors.New("#3 does not describe a condition: unexpected LIMIT")
		}
	}
	r, err := tab.SelectOneWhere(ctx, template, q.Cond, q.Order)
	if err != nil {
		return iobindings.PushingError(c, err)
	} else if r == nil {
		return c.PushingNext(nil), nil
	}
	return c.PushingNext(RecordToTable(r)), nil
}

// delete deletes those records in the table that match the record "selector". Returns the number of records deleted.
// Args: tab [selector]
//
// delete deletes those records in the table that match the given SQL statement. Returns the number of records deleted.
// Args: tab [sql]
func delete(ctx context.Context, tab *Table, _ *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	// Parse the condition
	cond, err := parseCondition(c, 1)
	if err != nil {
		return nil, err
	}
	// Perform the delete
	n, err := tab.DeleteWhere(ctx, cond)
	return iobindings.PushingNextResult(c, rt.Int(n), err)
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// TableArg turns a continuation argument into a *Table.
func TableArg(c *rt.GoCont, n int) (*Table, error) {
	t, ok := ValueToTable(c.Arg(n))
	if !ok {
		return nil, fmt.Errorf("#%d must be a keyvalue table", n+1)
	}
	return t, nil
}

// ValueToTable turns a lua value to a *Table, if possible.
func ValueToTable(v rt.Value) (*Table, bool) {
	u, ok := v.(*rt.UserData)
	if !ok {
		return nil, false
	}
	t, ok := u.Value().(*Table)
	return t, ok
}

// QueryArg turns a continuation argument into a parse.Query.
func QueryArg(c *rt.GoCont, n int) (*parse.Query, error) {
	if v, ok := c.Arg(n).(rt.String); ok {
		cond, err := parse.SQL(string(v))
		if err != nil {
			return nil, fmt.Errorf("#%d does not describe a condition: %w", n+1, err)
		}
		return cond, nil
	}
	r, err := RecordArg(c, n)
	if err != nil {
		return nil, err
	}
	return &parse.Query{Cond: condition.FromRecord(r)}, nil
}

// ConditionArg turns a continuation argument into a condition.Condition.
func ConditionArg(c *rt.GoCont, n int) (condition.Condition, error) {
	q, err := QueryArg(c, n)
	if err != nil {
		return nil, err
	} else if q.HasLimit {
		return nil, fmt.Errorf("#%d does not describe a condition: unexpected LIMIT", n+1)
	} else if len(q.Order) != 0 {
		return nil, fmt.Errorf("#%d does not describe a condition: unexpected ORDER BY", n+1)
	}
	return q.Cond, nil
}

// ValueToCondition turns a lua value to a condition.Condition, if possible.
func ValueToCondition(v rt.Value) (condition.Condition, bool) {
	q, ok := ValueToQuery(v)
	if !ok || q.HasLimit || len(q.Order) != 0 {
		return nil, false
	}
	return q.Cond, true
}

// ValueToQuery turns a lua value to a *parse.Query, if possible.
func ValueToQuery(v rt.Value) (*parse.Query, bool) {
	if x, ok := v.(rt.String); ok {
		cond, e := parse.SQL(string(x))
		if e != nil {
			return nil, false
		}
		return cond, true
	}
	r, ok := ValueToRecord(v)
	if !ok {
		return nil, false
	}
	return &parse.Query{Cond: condition.FromRecord(r)}, true
}
