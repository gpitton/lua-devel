// Kvdb contains Lua helpers for the keyvalue/kvdb datasource.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.

You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package kvdb

import (
	"bitbucket.org/pcas/golua/lib/packagelib"
	rt "bitbucket.org/pcas/golua/runtime"
	"bitbucket.org/pcas/keyvalue/kvdb"
	"bitbucket.org/pcas/keyvalue/kvdb/kvdbdriver"
	"bitbucket.org/pcas/lua/lib/keyvaluelib"
	"bitbucket.org/pcas/lua/luautil"
	"bitbucket.org/pcastools/address"
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// configToTable returns a table representing the given config.
func configToTable(cfg *kvdb.ClientConfig) *rt.Table {
	t := rt.NewTable()
	luautil.SetString(t, "address", cfg.URI())
	luautil.SetString(t, "appname", cfg.AppName)
	luautil.SetString(t, "sslcert", string(cfg.SSLCert))
	return t
}

// tableToConfig returns a copy of the given config modified with the values in t.
func tableToConfig(cfg *kvdb.ClientConfig, t *rt.Table) (*kvdb.ClientConfig, error) {
	// Move to a copy
	if cfg == nil {
		cfg = kvdb.DefaultConfig()
	} else {
		cfg = cfg.Copy()
	}
	// Modify the values
	if s, ok, err := luautil.GetString(t, "address"); ok {
		if err != nil {
			return nil, err
		}
		a, err := address.New(s)
		if err != nil {
			return nil, err
		}
		cfg.Address = a
	}
	if s, ok, err := luautil.GetString(t, "appname"); ok {
		if err != nil {
			return nil, err
		}
		cfg.AppName = s
	}
	if s, ok, err := luautil.GetString(t, "sslcert"); ok {
		if err != nil {
			return nil, err
		}
		cfg.SSLCert = []byte(s)
	}
	// Return the config settings
	return cfg, nil
}

// defaultConfig returns the default config as specified by keyvalue.<DriverName>.defaults.
func defaultConfig(t *rt.Thread) (*kvdb.ClientConfig, error) {
	// Recover the package table
	pkg, err := packagelib.CachedLoadedTable(t.Runtime, rt.String("keyvalue"))
	if err != nil {
		return nil, err
	}
	// Recover the defaults table
	defaults, err := luautil.Subtable(pkg, kvdbdriver.DriverName, "defaults")
	if err != nil {
		return nil, err
	}
	// Convert the values
	return tableToConfig(nil, defaults)
}

/////////////////////////////////////////////////////////////////////////
// Lua bindings
/////////////////////////////////////////////////////////////////////////

// init registers us with the keyvalue package.
func init() {
	// Register our package table with the keyvalue package
	keyvaluelib.Register(kvdbdriver.DriverName, func() *rt.Table {
		// Create the package table
		pkg := rt.NewTable()
		rt.SetEnv(pkg, "defaults", configToTable(kvdb.DefaultConfig()))
		rt.SetEnvGoFunc(pkg, "connect", keyvaluelib.WrapConnectFunc(connect), 2, false)
		// Return the table
		return pkg
	})
}

// connect returns a new connection to the kvdb server. The caller must call close on the connection when finished. If config is nil then the default configuration settings in keyvalue.kvdb.defaults will be used. The optional table config can be used to modify these defaults; in particular, config can be used to specify values for:
//	'address'	a string specifying the address to connect to;
//	'appname'	a string specifying the app name (used in logging);
//	'sslcert'	the SSL certificate (if any).
//
// Args: dbname [config]
func connect(t *rt.Thread, c *rt.GoCont) (string, error) {
	// The user has to provide the database name
	if err := c.Check1Arg(); err != nil {
		return "", err
	}
	dbName, err := c.StringArg(0)
	if err != nil {
		return "", err
	}
	// Read the config
	cfg, err := defaultConfig(t)
	if err != nil {
		return "", err
	}
	if c.NArgs() > 1 {
		v, err := c.TableArg(1)
		if err != nil {
			return "", err
		}
		if cfg, err = tableToConfig(cfg, v); err != nil {
			return "", err
		}
	}
	// Validate the settings
	if err = cfg.Validate(); err != nil {
		return "", err
	}
	// Return the data source
	return cfg.URL(string(dbName)), nil
}
