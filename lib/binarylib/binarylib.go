// Binarylib provides ways to manipulate binary data in Lua.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.

You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package binarylib

import (
	"bitbucket.org/pcas/golua/module"
	rt "bitbucket.org/pcas/golua/runtime"
	"encoding/binary"
	"fmt"
	endian "github.com/virtao/GoEndian"
	"math"
	"strconv"
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// init registers the module ready for loading into a Lua runtime.
func init() {
	module.Register(module.Info{
		Name:        "binary",
		Description: "Convert data to and from byte-level representations.",
		Load: func(r *rt.Runtime) (*rt.Table, error) {
			pkg := rt.NewTable()
			rt.SetEnvGoFunc(pkg, "endian", endianf, 0, false)

			rt.SetEnvGoFunc(pkg, "fromint16le",
				fromFunc(binary.LittleEndian, math.MinInt16, math.MaxInt16, 2),
				1, false)
			rt.SetEnvGoFunc(pkg, "fromuint16le",
				fromFunc(binary.LittleEndian, 0, math.MaxUint16, 2),
				1, false)
			rt.SetEnvGoFunc(pkg, "fromint32le",
				fromFunc(binary.LittleEndian, math.MinInt32, math.MaxInt32, 4),
				1, false)
			rt.SetEnvGoFunc(pkg, "fromuint32le",
				fromFunc(binary.LittleEndian, 0, math.MaxUint32, 4),
				1, false)
			rt.SetEnvGoFunc(pkg, "fromint64le",
				fromFunc(binary.LittleEndian, math.MinInt64, math.MaxInt64, 8),
				1, false)
			rt.SetEnvGoFunc(pkg, "fromuint64le",
				fromFunc(binary.LittleEndian, 0, math.MaxInt64, 8),
				1, false)

			rt.SetEnvGoFunc(pkg, "fromint16be",
				fromFunc(binary.BigEndian, math.MinInt16, math.MaxInt16, 2),
				1, false)
			rt.SetEnvGoFunc(pkg, "fromuint16be",
				fromFunc(binary.BigEndian, 0, math.MaxUint16, 2),
				1, false)
			rt.SetEnvGoFunc(pkg, "fromint32be",
				fromFunc(binary.BigEndian, math.MinInt32, math.MaxInt32, 4),
				1, false)
			rt.SetEnvGoFunc(pkg, "fromuint32be",
				fromFunc(binary.BigEndian, 0, math.MaxUint32, 4),
				1, false)
			rt.SetEnvGoFunc(pkg, "fromint64be",
				fromFunc(binary.BigEndian, math.MinInt64, math.MaxInt64, 8),
				1, false)
			rt.SetEnvGoFunc(pkg, "fromuint64be",
				fromFunc(binary.BigEndian, 0, math.MaxInt64, 8),
				1, false)

			rt.SetEnvGoFunc(pkg, "toint16le",
				toFunc(binary.LittleEndian, 2, true),
				1, false)
			rt.SetEnvGoFunc(pkg, "touint16le",
				toFunc(binary.LittleEndian, 2, false),
				1, false)
			rt.SetEnvGoFunc(pkg, "toint32le",
				toFunc(binary.LittleEndian, 4, true),
				1, false)
			rt.SetEnvGoFunc(pkg, "touint32le",
				toFunc(binary.LittleEndian, 4, false),
				1, false)
			rt.SetEnvGoFunc(pkg, "toint64le",
				toFunc(binary.LittleEndian, 8, true),
				1, false)
			rt.SetEnvGoFunc(pkg, "touint64le",
				toFunc(binary.LittleEndian, 8, false),
				1, false)

			rt.SetEnvGoFunc(pkg, "toint16be",
				toFunc(binary.BigEndian, 2, true),
				1, false)
			rt.SetEnvGoFunc(pkg, "touint16be",
				toFunc(binary.BigEndian, 2, false),
				1, false)
			rt.SetEnvGoFunc(pkg, "toint32be",
				toFunc(binary.BigEndian, 4, true),
				1, false)
			rt.SetEnvGoFunc(pkg, "touint32be",
				toFunc(binary.BigEndian, 4, false),
				1, false)
			rt.SetEnvGoFunc(pkg, "toint64be",
				toFunc(binary.BigEndian, 8, true),
				1, false)
			rt.SetEnvGoFunc(pkg, "touint64be",
				toFunc(binary.BigEndian, 8, false),
				1, false)
			return pkg, nil
		},
	})
}

// endianf returns 'le' if the CPU is little endian, and 'be' if the CPU is big endian.
// Args:
func endianf(_ *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	s := "le"
	if endian.IsBigEndian() {
		s = "be"
	}
	return c.PushingNext(rt.String(s)), nil
}

// toTable returns the given slice as a table.
func toTable(bs []byte) *rt.Table {
	t := rt.NewTable()
	for i, b := range bs {
		t.Set(rt.Int(i+1), rt.Int(b))
	}
	return t
}

// fromFunc returns the from function for the given binary data.
func fromFunc(ord binary.ByteOrder, min int64, max int64, N int) func(*rt.Thread, *rt.GoCont) (rt.Cont, error) {
	return func(_ *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
		if err := c.Check1Arg(); err != nil {
			return nil, err
		}
		n, err := c.IntArg(0)
		if err != nil {
			return nil, err
		}
		if int64(n) < min || int64(n) > max {
			return nil, fmt.Errorf("#1 must be in the range %d to %d (inclusive)", min, max)
		}
		bs := make([]byte, N)
		switch N {
		case 2:
			ord.PutUint16(bs, uint16(n))
		case 4:
			ord.PutUint32(bs, uint32(n))
		case 8:
			ord.PutUint64(bs, uint64(n))
		}
		return c.PushingNext(toTable(bs)), nil
	}
}

// fromTable returns the given table as a slice.
func fromTable(t *rt.Table, N int) []byte {
	bs := make([]byte, 0, N)
	for i := 1; i <= N; i++ {
		v := t.Get(rt.Int(i))
		if rt.IsNil(v) {
			return bs
		}
		n, ok := rt.ToInt(v)
		if !ok {
			return bs
		} else if n < 0 || n > math.MaxUint8 {
			return bs
		}
		bs = append(bs, byte(n))
	}
	return bs
}

// toFunc returns the to function for the given binary data.
func toFunc(ord binary.ByteOrder, N int, signed bool) func(*rt.Thread, *rt.GoCont) (rt.Cont, error) {
	return func(_ *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
		if err := c.Check1Arg(); err != nil {
			return nil, err
		}
		t, err := c.TableArg(0)
		if err != nil {
			return nil, err
		}
		bs := fromTable(t, N)
		if len(bs) != N {
			return nil, fmt.Errorf("#1 must have at least %d bytes set", N)
		}
		switch N {
		case 2:
			k := ord.Uint16(bs)
			if signed {
				return c.PushingNext(rt.Int(int16(k))), nil
			}
			return c.PushingNext(rt.Int(k)), nil
		case 4:
			k := ord.Uint32(bs)
			if signed {
				return c.PushingNext(rt.Int(int32(k))), nil
			}
			return c.PushingNext(rt.Int(k)), nil
		case 8:
			k := ord.Uint64(bs)
			if signed {
				return c.PushingNext(rt.Int(int64(k))), nil
			} else if k <= math.MaxInt64 {
				return c.PushingNext(rt.Int(int64(k))), nil
			}
			return c.PushingNext(rt.String(strconv.FormatUint(k, 10))), nil
		}
		panic("Impossible to get here")
	}
}
