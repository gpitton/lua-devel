// Fslib contains Lua glue for the fs package.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.

You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package fslib

import (
	"bitbucket.org/pcas/fs"
	"bitbucket.org/pcas/golua/lib/iobindings"
	"bitbucket.org/pcas/golua/module"
	rt "bitbucket.org/pcas/golua/runtime"
	"context"
	"sync"
	"time"
)

// registryKeyType is a private struct used to define a unique key in the registry.
type registryKeyType struct{}

var registryKey = registryKeyType{}

// registryData is the data we store in the registry, indexed by registryKey.
type registryData struct {
	timeout  time.Duration // The default timeout
	fsMt     *rt.Table     // The metatable for a connection
	readerMt *rt.Table     // The metatable for a reader
	writerMt *rt.Table     // The metatable for a writer
}

// A map of registered package function loaders and controlling mutex.
var (
	regm = sync.Mutex{}
	reg  = make(map[string]func() *rt.Table)
)

// ConnectFunc defines a function that will connect to a file store.
type ConnectFunc func(context.Context) (fs.Interface, error)

/////////////////////////////////////////////////////////////////////////
// registryData functions
/////////////////////////////////////////////////////////////////////////

// getRegistryData returns the package data stored in the registry.
func getRegistryData(t *rt.Thread) *registryData {
	return t.Registry(registryKey).(*registryData)
}

// SetTimeout sets the default timeout.
func (r *registryData) SetTimeout(timeout time.Duration) {
	r.timeout = timeout
}

// GetTimeout returns the default timeout.
func (r *registryData) GetTimeout() time.Duration {
	return r.timeout
}

/////////////////////////////////////////////////////////////////////////
// Lua bindings
/////////////////////////////////////////////////////////////////////////

// init registers the module ready for loading into a Lua runtime.
func init() {
	// Register this module
	module.Register(module.Info{
		Name:        "fs",
		Description: "Provides pcas file system access.",
		Load: func(r *rt.Runtime) (*rt.Table, error) {
			// Save the registry data
			r.SetRegistry(registryKey, &registryData{
				timeout:  iobindings.NilTimeout,
				fsMt:     createFsMetatable(),
				readerMt: createReaderMetatable(),
				writerMt: createWriterMetatable(),
			})
			// Create the package table
			pkg := rt.NewTable()
			// Add the registered tables
			regm.Lock()
			defer regm.Unlock()
			for name, f := range reg {
				rt.SetEnv(pkg, name, f())
			}
			// Add our package values
			rt.SetEnvGoFunc(pkg, "settimeout", settimeout, 1, false)
			rt.SetEnvGoFunc(pkg, "gettimeout", gettimeout, 0, false)
			return pkg, nil
		},
	})
}

// settimeout sets the default timeout.
func settimeout(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	return iobindings.SetTimeoutWithSetTimeouter(getRegistryData(t), c)
}

// gettimeout returns the default I/O timeout, in seconds.
func gettimeout(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	return iobindings.GetTimeoutWithGetTimeouter(getRegistryData(t), c)
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// WrapConnectFunc wraps the given function, converting it into a full connection function for Lua.
func WrapConnectFunc(fileStoreName string, g func(*rt.Thread, *rt.GoCont) (string, ConnectFunc, error)) func(*rt.Thread, *rt.GoCont) (rt.Cont, error) {
	return func(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
		uri, f, err := g(t, c)
		if err != nil {
			return nil, err
		}
		s, err := connectToFileStore(t, fileStoreName, uri, f)
		if err != nil {
			return iobindings.PushingError(c, err)
		}
		return c.PushingNext(rt.NewUserData(s, getRegistryData(t).fsMt)), nil
	}
}

// Register registers the given function that returns a table to be added to the package table using the given name (typically a name of a file store). If a function is already registered with the given name then it will be replaced; if the name coincides with one of the package functions or values then it will be overwritten. Note that any function registered after the package is loaded into the Lua runtime will not appear in that instance of the package.
func Register(name string, f func() *rt.Table) {
	regm.Lock()
	defer regm.Unlock()
	reg[name] = f
}
