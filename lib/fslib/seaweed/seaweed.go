// Seaweed contains Lua helpers for the fs/seaweed file store.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.

You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package fsd

import (
	"bitbucket.org/pcas/fs"
	"bitbucket.org/pcas/fs/seaweed"
	"bitbucket.org/pcas/golua/lib/packagelib"
	rt "bitbucket.org/pcas/golua/runtime"
	"bitbucket.org/pcas/lua/lib/fslib"
	"bitbucket.org/pcas/lua/luautil"
	"bitbucket.org/pcastools/stringsbuilder"
	"context"
	"net/url"
)

// fileStoreName is the name of the file store.
const fileStoreName = "seaweed"

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// configToTable returns a table representing the given client configuration.
func configToTable(cfg *seaweed.ClientConfig) *rt.Table {
	t := rt.NewTable()
	luautil.SetString(t, "master", cfg.Master.String())
	luautil.SetString(t, "filer", cfg.Filer.String())
	luautil.SetString(t, "appname", cfg.AppName)
	return t
}

// tableToConfig returns a copy of the given client configuration modified with the values in t.
func tableToConfig(cfg *seaweed.ClientConfig, t *rt.Table) (*seaweed.ClientConfig, error) {
	// Move to a copy
	if cfg == nil {
		cfg = seaweed.DefaultConfig()
	} else {
		cfg = cfg.Copy()
	}
	// Modify the values
	if s, ok, err := luautil.GetString(t, "master"); ok {
		if err != nil {
			return nil, err
		}
		u, err := url.Parse(s)
		if err != nil {
			return nil, err
		}
		cfg.Master = u
	}
	if s, ok, err := luautil.GetString(t, "filer"); ok {
		if err != nil {
			return nil, err
		}
		u, err := url.Parse(s)
		if err != nil {
			return nil, err
		}
		cfg.Filer = u
	}
	if s, ok, err := luautil.GetString(t, "appname"); ok {
		if err != nil {
			return nil, err
		}
		cfg.AppName = s
	}
	// Return the config settings
	return cfg, nil
}

// defaultConfig returns the default config as specified by fs.<fileStoreName>.defaults.
func defaultConfig(t *rt.Thread) (*seaweed.ClientConfig, error) {
	// Recover the package table
	pkg, err := packagelib.CachedLoadedTable(t.Runtime, rt.String("fs"))
	if err != nil {
		return nil, err
	}
	// Recover the defaults table
	defaults, err := luautil.Subtable(pkg, fileStoreName, "defaults")
	if err != nil {
		return nil, err
	}
	// Convert the values
	return tableToConfig(nil, defaults)
}

// configToString returns a string representation of the config data. This is intended to be used in place of a URI for the config data.
func configToString(cfg *seaweed.ClientConfig) string {
	b := stringsbuilder.New()
	defer stringsbuilder.Reuse(b)
	b.WriteString("master:")           // Ignore any error
	b.WriteString(cfg.Master.String()) // Ignore any error
	b.WriteString(" filer:")           // Ignore any error
	b.WriteString(cfg.Filer.String())  // Ignore any error
	b.WriteString(" appname:")         // Ignore any error
	b.WriteString(cfg.AppName)         // Ignore any error
	return b.String()
}

/////////////////////////////////////////////////////////////////////////
// Lua bindings
/////////////////////////////////////////////////////////////////////////

// init registers us with the fs package.
func init() {
	// Register our package table with the fs package
	fslib.Register(fileStoreName, func() *rt.Table {
		// Create the package table
		pkg := rt.NewTable()
		rt.SetEnv(pkg, "defaults", configToTable(seaweed.DefaultConfig()))
		rt.SetEnvGoFunc(pkg, "connect", fslib.WrapConnectFunc(fileStoreName, connect), 1, false)
		// Return the table
		return pkg
	})
}

// connect returns a new connection to the seaweed server. The caller must call close on the connection when finished. If config is nil then the default configuration settings will be used. The optional table config can be used to modify these defaults; in particular, config can be used to specify values for:
//	'master'	a string specifying the address of the master server;
//	'filer'		a string specifying the address of the filer server;
//	'appname'	the application name to identify ourselves as.
//
// Args: [config]
func connect(t *rt.Thread, c *rt.GoCont) (string, fslib.ConnectFunc, error) {
	// Read the config
	cfg, err := defaultConfig(t)
	if err != nil {
		return "", nil, err
	}
	if c.NArgs() > 0 {
		v, err := c.TableArg(0)
		if err != nil {
			return "", nil, err
		}
		if cfg, err = tableToConfig(cfg, v); err != nil {
			return "", nil, err
		}
	}
	// Validate the settings
	if err = cfg.Validate(); err != nil {
		return "", nil, err
	}
	// Return the URI and connection function
	return configToString(cfg), func(ctx context.Context) (fs.Interface, error) {
		return seaweed.New(ctx, cfg)
	}, nil
}
