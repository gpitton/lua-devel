// File presents fs.Readers and fs.Writers in Lua.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.

You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package fslib

import (
	"bitbucket.org/pcas/fs"
	"bitbucket.org/pcas/golua/lib/iobindings"
	rt "bitbucket.org/pcas/golua/runtime"
	"errors"
	"fmt"
	"io"
	"runtime"
	"sync"
	"time"
)

// Reader wraps an fs.Reader with a rolling timeout.
type Reader struct {
	r        fs.Reader     // The underlying fs.Reader
	isClosed bool          // Are we closed?
	closeErr error         // The error on close
	m        sync.Mutex    // Controls access to the following
	timeout  time.Duration // The timeout
}

// Writer wraps an fs.Writer with a rolling timeout.
type Writer struct {
	w        fs.Writer     // The underlying fs.Writer
	isClosed bool          // Are we closed?
	closeErr error         // The error on close
	m        sync.Mutex    // Controls access to the following
	timeout  time.Duration // The timeout
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// readerMethod wraps the GoFunction f with a check that arg(0) is a *Reader.
func readerMethod(f func(*rt.Thread, *rt.GoCont) (rt.Cont, error)) func(*rt.Thread, *rt.GoCont) (rt.Cont, error) {
	return func(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
		if err := c.Check1Arg(); err != nil {
			return nil, err
		} else if _, err := ReaderArg(c, 0); err != nil {
			return nil, err
		}
		return f(t, c)
	}
}

// writerMethod wraps the GoFunction f with a check that arg(0) is a *Writer.
func writerMethod(f func(*rt.Thread, *rt.GoCont) (rt.Cont, error)) func(*rt.Thread, *rt.GoCont) (rt.Cont, error) {
	return func(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
		if err := c.Check1Arg(); err != nil {
			return nil, err
		} else if _, err := WriterArg(c, 0); err != nil {
			return nil, err
		}
		return f(t, c)
	}
}

// createReaderMetatable creates the metatable for a Reader.
func createReaderMetatable() *rt.Table {
	// Create the methods
	methods := rt.NewTable()
	rt.SetEnvGoFunc(methods, "read", readerMethod(iobindings.Read), 1, true)
	rt.SetEnvGoFunc(methods, "lines", readerMethod(iobindings.Lines), 1, true)
	rt.SetEnvGoFunc(methods, "close", readerMethod(iobindings.Close), 1, false)
	rt.SetEnvGoFunc(methods, "isclosed", readerMethod(iobindings.IsClosed), 1, false)
	rt.SetEnvGoFunc(methods, "settimeout", readerMethod(iobindings.SetTimeout), 1, false)
	rt.SetEnvGoFunc(methods, "gettimeout", readerMethod(iobindings.GetTimeout), 1, false)
	// Create the metatable
	meta := rt.NewTable()
	rt.SetEnv(meta, "__index", methods)
	rt.SetEnvGoFunc(meta, "__tostring", readerMethod(iobindings.ToString), 1, false)
	return meta
}

// createWriterMetatable creates the metatable for a Writer.
func createWriterMetatable() *rt.Table {
	// Create the methods
	methods := rt.NewTable()
	rt.SetEnvGoFunc(methods, "write", writerMethod(iobindings.Write), 1, true)
	rt.SetEnvGoFunc(methods, "close", writerMethod(iobindings.Close), 1, false)
	rt.SetEnvGoFunc(methods, "isclosed", writerMethod(iobindings.IsClosed), 1, false)
	rt.SetEnvGoFunc(methods, "settimeout", writerMethod(iobindings.SetTimeout), 1, false)
	rt.SetEnvGoFunc(methods, "gettimeout", writerMethod(iobindings.GetTimeout), 1, false)
	// Create the metatable
	meta := rt.NewTable()
	rt.SetEnv(meta, "__index", methods)
	rt.SetEnvGoFunc(meta, "__tostring", writerMethod(iobindings.ToString), 1, false)
	return meta
}

/////////////////////////////////////////////////////////////////////////
// Reader functions
/////////////////////////////////////////////////////////////////////////

// newReader wraps the given fs.Reader as a *Reader. A finaliser is set on the reader to ensure that it is closed.
func newReader(r fs.Reader, timeout time.Duration) *Reader {
	fr := &Reader{
		r:       r,
		timeout: timeout,
	}
	runtime.SetFinalizer(fr, func(fr *Reader) { fr.Close() })
	return fr
}

// Close closes the reader.
func (r *Reader) Close() error {
	if r == nil {
		return nil
	} else if !r.isClosed {
		r.closeErr = r.r.Close()
		r.isClosed = true
		runtime.SetFinalizer(r, nil)
	}
	return r.closeErr
}

// IsClosed returns true iff the file is known to be closed.
func (r *Reader) IsClosed() bool {
	return r == nil || r.isClosed
}

// Read reads up to len(p) bytes into p. It returns the number of bytes read and any error encountered.
func (r *Reader) Read(p []byte) (int, error) {
	if r == nil {
		return 0, io.EOF
	}
	if d := r.GetTimeout(); d < 0 {
		r.r.SetDeadline(time.Time{})
	} else {
		r.r.SetDeadline(time.Now().Add(d))
	}
	n, err := r.r.Read(p)
	if err == io.EOF {
		r.Close() // Ignore any error
	}
	return n, err
}

// Name returns the name of the file as presented to Download.
func (r *Reader) Name() string {
	if r == nil {
		return ""
	}
	return r.r.Name()
}

// SetTimeout sets the timeout duration.
func (r *Reader) SetTimeout(d time.Duration) {
	if r != nil {
		r.m.Lock()
		r.timeout = d
		r.m.Unlock()
	}
}

// GetTimeout returns the current timeout duration.
func (r *Reader) GetTimeout() time.Duration {
	if r == nil {
		return iobindings.NilTimeout
	}
	r.m.Lock()
	defer r.m.Unlock()
	if r.timeout < 0 {
		return iobindings.NilTimeout
	}
	return r.timeout
}

// String returns a string description of the reader.
func (r *Reader) String() string {
	if r == nil {
		return "fsreader(nil)"
	}
	return "fsreader(\"" + r.Name() + "\")"
}

/////////////////////////////////////////////////////////////////////////
// Writer functions
/////////////////////////////////////////////////////////////////////////

// newWriter wraps the given fs.Writer as a *Writer. A finaliser is set on the writer to ensure that it is closed.
func newWriter(w fs.Writer, timeout time.Duration) *Writer {
	fw := &Writer{
		w:       w,
		timeout: timeout,
	}
	runtime.SetFinalizer(fw, func(fw *Writer) { fw.Close() })
	return fw
}

// Close closes the writer.
func (w *Writer) Close() error {
	if w == nil {
		return nil
	} else if !w.isClosed {
		w.closeErr = w.w.Close()
		w.isClosed = true
		runtime.SetFinalizer(w, nil)
	}
	return w.closeErr
}

// IsClosed returns true iff the file is known to be closed.
func (w *Writer) IsClosed() bool {
	return w == nil || w.isClosed
}

//  Write writes len(p) bytes from p to the underlying data stream. It returns the number of bytes written from p and any error encountered that caused the write to stop early.
func (w *Writer) Write(p []byte) (int, error) {
	if w == nil {
		return 0, errors.New("uninitialized writer")
	}
	if d := w.GetTimeout(); d < 0 {
		w.w.SetDeadline(time.Time{})
	} else {
		w.w.SetDeadline(time.Now().Add(d))
	}
	return w.w.Write(p)
}

// Name returns the name of the file as presented to Upload.
func (w *Writer) Name() string {
	if w == nil {
		return ""
	}
	return w.w.Name()
}

// SetTimeout sets the timeout duration.
func (w *Writer) SetTimeout(d time.Duration) {
	if w != nil {
		w.m.Lock()
		w.timeout = d
		w.m.Unlock()
	}
}

// GetTimeout returns the current timeout duration.
func (w *Writer) GetTimeout() time.Duration {
	if w == nil {
		return iobindings.NilTimeout
	}
	w.m.Lock()
	defer w.m.Unlock()
	if w.timeout < 0 {
		return iobindings.NilTimeout
	}
	return w.timeout
}

// String returns a string description of the writer.
func (w *Writer) String() string {
	if w == nil {
		return "fswriter(nil)"
	}
	return "fswriter(\"" + w.Name() + "\")"
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// ReaderArg turns a continuation argument into a *Reader.
func ReaderArg(c *rt.GoCont, n int) (*Reader, error) {
	f, ok := ValueToReader(c.Arg(n))
	if !ok {
		return nil, fmt.Errorf("#%d must be a reader", n+1)
	}
	return f, nil
}

// ValueToReader turns a Lua value to a *Reader if possible.
func ValueToReader(v rt.Value) (*Reader, bool) {
	u, ok := v.(*rt.UserData)
	if !ok {
		return nil, false
	}
	f, ok := u.Value().(*Reader)
	return f, ok
}

// WriterArg turns a continuation argument into a *Writer.
func WriterArg(c *rt.GoCont, n int) (*Writer, error) {
	f, ok := ValueToWriter(c.Arg(n))
	if !ok {
		return nil, fmt.Errorf("#%d must be a writer", n+1)
	}
	return f, nil
}

// ValueToWriter turns a Lua value to a *Writer if possible.
func ValueToWriter(v rt.Value) (*Writer, bool) {
	u, ok := v.(*rt.UserData)
	if !ok {
		return nil, false
	}
	f, ok := u.Value().(*Writer)
	return f, ok
}
