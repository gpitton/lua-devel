// Encoder provides a Lua-view of a base64 encoder and decoder.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.

You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package base64lib

import (
	"bitbucket.org/pcas/golua/lib/iobindings"
	rt "bitbucket.org/pcas/golua/runtime"
	"encoding/base64"
	"errors"
	"fmt"
	"io"
	"runtime"
)

// Encoder describes a base64 encoder.
type Encoder struct {
	io.WriteCloser
	enc      *base64.Encoding // The encoding used
	isClosed bool             // Is the compressor closed?
	closeErr error            // The error on close (if any)
}

// Decoder describes a base64 decoder.
type Decoder struct {
	io.Reader
	enc *base64.Encoding // The encoding used
}

// encodinger is the interface satisfied by the Encoding method.
type encodinger interface {
	Encoding() *base64.Encoding
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// encoderMethod wraps the GoFunction f with a check that arg(0) is a *Encoder.
func encoderMethod(f func(*rt.Thread, *rt.GoCont) (rt.Cont, error)) func(*rt.Thread, *rt.GoCont) (rt.Cont, error) {
	return func(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
		if err := c.Check1Arg(); err != nil {
			return nil, err
		} else if _, err := EncoderArg(c, 0); err != nil {
			return nil, err
		}
		return f(t, c)
	}
}

// decoderMethod wraps the GoFunction f with a check that arg(0) is a *Decoder.
func decoderMethod(f func(*rt.Thread, *rt.GoCont) (rt.Cont, error)) func(*rt.Thread, *rt.GoCont) (rt.Cont, error) {
	return func(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
		if err := c.Check1Arg(); err != nil {
			return nil, err
		} else if _, err := DecoderArg(c, 0); err != nil {
			return nil, err
		}
		return f(t, c)
	}
}

// encodingName attempts to recover a name for the given encoding. If no name is found, returns "unknown".
func encodingName(enc *base64.Encoding) string {
	for k, v := range encodingMap {
		if v == enc {
			return k
		}
	}
	return "unknown"
}

/////////////////////////////////////////////////////////////////////////
// Encoder functions
/////////////////////////////////////////////////////////////////////////

// NewEncoder returns a new base64 encoder, using the encoding enc and wrapping w. A finaliser is set on the returned encoder (via runtime.SetFinalizer) so that it is automatically closed when garbage collected.
func NewEncoder(enc *base64.Encoding, w io.Writer) *Encoder {
	if enc == nil {
		enc = base64.StdEncoding
	}
	e := &Encoder{
		WriteCloser: base64.NewEncoder(enc, w),
		enc:         enc,
	}
	runtime.SetFinalizer(e, func(e *Encoder) { e.Close() })
	return e
}

// Close closes the encoder.
func (e *Encoder) Close() error {
	if e == nil {
		return nil
	} else if !e.isClosed {
		e.closeErr = e.WriteCloser.Close()
		e.isClosed = true
		runtime.SetFinalizer(e, nil)
	}
	return e.closeErr
}

// IsClosed returns true iff the encoder is known to be closed.
func (e *Encoder) IsClosed() bool {
	return e == nil || e.isClosed
}

// Encoding returns the encoding used.
func (e *Encoder) Encoding() *base64.Encoding {
	if e == nil || e.enc == nil {
		return base64.StdEncoding
	}
	return e.enc
}

// String returns a string description of the encoder.
func (*Encoder) String() string {
	return "base64encoder"
}

/////////////////////////////////////////////////////////////////////////
// Decoder functions
/////////////////////////////////////////////////////////////////////////

// NewDecoder returns a new base64 decoder, using the encoding enc and wrapping r.
func NewDecoder(enc *base64.Encoding, r io.Reader) *Decoder {
	if enc == nil {
		enc = base64.StdEncoding
	}
	return &Decoder{
		Reader: base64.NewDecoder(enc, r),
		enc:    enc,
	}
}

// Encoding returns the encoding used.
func (d *Decoder) Encoding() *base64.Encoding {
	if d == nil || d.enc == nil {
		return base64.StdEncoding
	}
	return d.enc
}

// String returns a string description of the decoder.
func (*Decoder) String() string {
	return "base64decoder"
}

/////////////////////////////////////////////////////////////////////////
// Lua bindings
/////////////////////////////////////////////////////////////////////////

// createEncoderMetatable creates the metatable for an encoder.
func createEncoderMetatable() *rt.Table {
	// Create the methods
	methods := rt.NewTable()
	rt.SetEnvGoFunc(methods, "close", encoderMethod(iobindings.Close), 1, false)
	rt.SetEnvGoFunc(methods, "isclosed", encoderMethod(iobindings.IsClosed), 1, false)
	rt.SetEnvGoFunc(methods, "write", encoderMethod(iobindings.Write), 1, true)
	rt.SetEnvGoFunc(methods, "encoding", encoderMethod(encoding), 1, false)
	// Create the metatable
	meta := rt.NewTable()
	rt.SetEnv(meta, "__index", methods)
	rt.SetEnvGoFunc(meta, "__tostring", encoderMethod(iobindings.ToString), 1, false)
	return meta
}

// createDecoderMetatable creates the metatable for a decoder.
func createDecoderMetatable() *rt.Table {
	// Create the methods
	methods := rt.NewTable()
	rt.SetEnvGoFunc(methods, "read", decoderMethod(iobindings.Read), 1, true)
	rt.SetEnvGoFunc(methods, "lines", decoderMethod(iobindings.Lines), 1, true)
	rt.SetEnvGoFunc(methods, "encoding", decoderMethod(encoding), 1, false)
	// Create the metatable
	meta := rt.NewTable()
	rt.SetEnv(meta, "__index", methods)
	rt.SetEnvGoFunc(meta, "__tostring", decoderMethod(iobindings.ToString), 1, false)
	return meta
}

// encoding returns the base64 encoding used.
func encoding(t *rt.Thread, c *rt.GoCont) (rt.Cont, error) {
	if err := c.Check1Arg(); err != nil {
		return nil, err
	}
	u, ok := c.Arg(0).(*rt.UserData)
	if !ok {
		return nil, errors.New("#1 must have an encoding")
	}
	e, ok := u.Value().(encodinger)
	if !ok {
		return nil, errors.New("#1 must have an encoding")
	}
	return c.PushingNext(rt.String(encodingName(e.Encoding()))), nil
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// EncoderArg turns a continuation argument into an *Encoder.
func EncoderArg(c *rt.GoCont, n int) (*Encoder, error) {
	w, ok := ValueToEncoder(c.Arg(n))
	if !ok {
		return nil, fmt.Errorf("#%d must be a base64 encoder", n+1)
	}
	return w, nil
}

// ValueToEncoder turns a Lua value to an *Encoder if possible.
func ValueToEncoder(v rt.Value) (*Encoder, bool) {
	u, ok := v.(*rt.UserData)
	if !ok {
		return nil, false
	}
	w, ok := u.Value().(*Encoder)
	return w, ok
}

// DecoderArg turns a continuation argument into a *Decoder.
func DecoderArg(c *rt.GoCont, n int) (*Decoder, error) {
	r, ok := ValueToDecoder(c.Arg(n))
	if !ok {
		return nil, fmt.Errorf("#%d must be a base64 decoder", n+1)
	}
	return r, nil
}

// ValueToDecoder turns a Lua value to a *Decoder if possible.
func ValueToDecoder(v rt.Value) (*Decoder, bool) {
	u, ok := v.(*rt.UserData)
	if !ok {
		return nil, false
	}
	r, ok := u.Value().(*Decoder)
	return r, ok
}
