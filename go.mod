module bitbucket.org/pcas/lua

go 1.13

require (
	bitbucket.org/pcas/fs v0.1.8
	bitbucket.org/pcas/golua v0.1.4
	bitbucket.org/pcas/keyvalue v0.1.21
	bitbucket.org/pcas/logger v0.1.23
	bitbucket.org/pcas/metrics v0.1.20
	bitbucket.org/pcas/sslflag v0.0.11
	bitbucket.org/pcastools/address v0.1.3
	bitbucket.org/pcastools/bytesbuffer v1.0.2
	bitbucket.org/pcastools/cleanup v1.0.3
	bitbucket.org/pcastools/compress v1.0.2
	bitbucket.org/pcastools/contextutil v1.0.2
	bitbucket.org/pcastools/convert v1.0.4
	bitbucket.org/pcastools/file v1.0.2
	bitbucket.org/pcastools/flag v0.0.16
	bitbucket.org/pcastools/log v1.0.3
	bitbucket.org/pcastools/refcount v1.0.3
	bitbucket.org/pcastools/stringsbuilder v1.0.2
	bitbucket.org/pcastools/ulid v0.1.3
	bitbucket.org/pcastools/version v0.0.4
	github.com/aws/aws-sdk-go v1.36.22 // indirect
	github.com/chzyer/logex v1.1.10 // indirect
	github.com/chzyer/readline v0.0.0-20180603132655-2972be24d48e
	github.com/chzyer/test v0.0.0-20180213035817-a1ea475d72b1 // indirect
	github.com/fatih/color v1.10.0
	github.com/juju/ansiterm v0.0.0-20180109212912-720a0952cc2a
	github.com/klauspost/compress v1.11.5 // indirect
	github.com/lunixbochs/vtclean v1.0.0 // indirect
	github.com/mitchellh/go-wordwrap v1.0.1
	github.com/pkg/errors v0.9.1
	github.com/shirou/gopsutil v3.20.12+incompatible
	github.com/virtao/GoEndian v0.0.0-20140331034613-586fa83c856a
	google.golang.org/genproto v0.0.0-20210106152847-07624b53cd92 // indirect
	mvdan.cc/xurls/v2 v2.2.0
)
