// Lua is the pcas Lua runtime.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
    
You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package lua

import (
	"bitbucket.org/pcas/golua/module"
	rt "bitbucket.org/pcas/golua/runtime"
	_ "bitbucket.org/pcas/lua/lib"
	"io"
)

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// New returns a new Lua runtime, with the pcas modules installed, and with the given stdout.
func New(stdout io.Writer) (*rt.Runtime, error) {
	r := rt.New(stdout)
	if err := module.Init(r); err != nil {
		return nil, err
	}
	return r, nil
}
