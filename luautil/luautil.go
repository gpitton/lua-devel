// Luautil contains useful utilities for working with golua.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.

You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package luautil

import (
	rt "bitbucket.org/pcas/golua/runtime"
	"fmt"
	"strings"
)

// The minimum and maximum values of an int.
const (
	maxInt = int64(int(^uint(0) >> 1))
	minInt = -maxInt - 1
)

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// Subtable returns the table in t referred to by name. More precisely, it returns t[<name[0]>][<name[1]>][...], or an error if any of the values are not a table.
func Subtable(t *rt.Table, name ...string) (*rt.Table, error) {
	for i, n := range name {
		var ok bool
		if t, ok = t.Get(rt.String(n)).(*rt.Table); !ok {
			return nil, fmt.Errorf("'%s' is not a table", strings.Join(name[:i], "."))
		}
	}
	return t, nil
}

// SetString sets the value for the given key in the given table to be s.
func SetString(t *rt.Table, key string, s string) {
	rt.SetEnv(t, key, rt.String(s))
}

// SetInt64 sets the value for the given key in the given table to be n.
func SetInt64(t *rt.Table, key string, n int64) {
	rt.SetEnv(t, key, rt.Int(n))
}

// SetInt sets the value for the given key in the given table to be n.
func SetInt(t *rt.Table, key string, n int) {
	rt.SetEnv(t, key, rt.Int(n))
}

// SetFloat64 sets the value for the given key in the given table to be f.
func SetFloat64(t *rt.Table, key string, f float64) {
	rt.SetEnv(t, key, rt.Float(f))
}

// SetBool sets the value for the given key in the given table to be b.
func SetBool(t *rt.Table, key string, b bool) {
	rt.SetEnv(t, key, rt.Bool(b))
}

// GetString returns the value for the given key in the given table as a string. The boolean wasSet will be true iff the value was non-nil (regardless of type). If the value was non-nil but could not be converted to a string then err will describe an error.
func GetString(t *rt.Table, key ...string) (s string, wasSet bool, err error) {
	k := len(key)
	if k == 0 {
		return
	} else if k > 1 {
		t, err = Subtable(t, key[:k-1]...)
		if err != nil {
			return
		}
	}
	if v := t.Get(rt.String(key[k-1])); !rt.IsNil(v) {
		wasSet = true
		if vs, ok := rt.AsString(v); !ok {
			err = fmt.Errorf("'%s' is not a string", strings.Join(key, "."))
		} else {
			s = string(vs)
		}
	}
	return
}

// GetInt64 returns the value for the given key in the given table as an int64. The boolean wasSet will be true iff the value was non-nil (regardless of type). If the value was non-nil but could not be converted to an int64 then err will describe an error.
func GetInt64(t *rt.Table, key ...string) (n int64, wasSet bool, err error) {
	k := len(key)
	if k == 0 {
		return
	} else if k > 1 {
		t, err = Subtable(t, key[:k-1]...)
		if err != nil {
			return
		}
	}
	if v := t.Get(rt.String(key[k-1])); !rt.IsNil(v) {
		wasSet = true
		if vn, ok := rt.ToInt(v); !ok {
			err = fmt.Errorf("'%s' is not an int", strings.Join(key, "."))
		} else {
			n = int64(vn)
		}
	}
	return
}

// GetInt returns the value for the given key in the given table as an int. The boolean wasSet will be true iff the value was non-nil (regardless of type). If the value was non-nil but could not be converted to an int then err will describe an error.
func GetInt(t *rt.Table, key ...string) (n int, wasSet bool, err error) {
	var m int64
	if m, wasSet, err = GetInt64(t, key...); err == nil && wasSet {
		if m < minInt || m > maxInt {
			err = fmt.Errorf("'%s' is out-of-range", strings.Join(key, "."))
		} else {
			n = int(m)
		}
	}
	return
}

// GetFloat64 returns the value for the given key in the given table as a float64. The boolean wasSet will be true iff the value was non-nil (regardless of type). If the value was non-nil but could not be converted to a float64 then err will describe an error.
func GetFloat64(t *rt.Table, key ...string) (f float64, wasSet bool, err error) {
	k := len(key)
	if k == 0 {
		return
	} else if k > 1 {
		t, err = Subtable(t, key[:k-1]...)
		if err != nil {
			return
		}
	}
	if v := t.Get(rt.String(key[k-1])); !rt.IsNil(v) {
		wasSet = true
		if vf, ok := rt.ToFloat(v); !ok {
			err = fmt.Errorf("'%s' is not a float", strings.Join(key, "."))
		} else {
			f = float64(vf)
		}
	}
	return
}

// GetBool returns the value for the given key in the given table as a bool. The boolean wasSet will be true iff the value was non-nil (regardless of type). If the value was non-nil but could not be converted to a bool then err will describe an error.
func GetBool(t *rt.Table, key ...string) (b bool, wasSet bool, err error) {
	k := len(key)
	if k == 0 {
		return
	} else if k > 1 {
		t, err = Subtable(t, key[:k-1]...)
		if err != nil {
			return
		}
	}
	if v := t.Get(rt.String(key[k-1])); !rt.IsNil(v) {
		wasSet = true
		if vb, ok := v.(rt.Bool); !ok {
			err = fmt.Errorf("'%s' is not a bool", strings.Join(key, "."))
		} else {
			b = bool(vb)
		}
	}
	return
}

// GetTable returns the value for the given key in the given table as a table. The boolean wasSet will be true iff the value was non-nil (regardless of type). If the value was non-nil but could not be converted to a table then err will describe an error.
func GetTable(t *rt.Table, key ...string) (tab *rt.Table, wasSet bool, err error) {
	k := len(key)
	if k == 0 {
		return
	} else if k > 1 {
		t, err = Subtable(t, key[:k-1]...)
		if err != nil {
			return
		}
	}
	if v := t.Get(rt.String(key[k-1])); !rt.IsNil(v) {
		wasSet = true
		var ok bool
		if tab, ok = v.(*rt.Table); !ok {
			err = fmt.Errorf("'%s' is not a table", strings.Join(key, "."))
		}
	}
	return
}
