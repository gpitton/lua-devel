// Config handles configuration

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.

You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package main

import (
	"bitbucket.org/pcas/fs/fsd"
	"bitbucket.org/pcas/fs/seaweed/seaweedflag"
	"bitbucket.org/pcas/keyvalue/kvdb"
	"bitbucket.org/pcas/keyvalue/mongodb/mongodbflag"
	"bitbucket.org/pcas/keyvalue/postgres/postgresflag"
	"bitbucket.org/pcas/logger/logd"
	"bitbucket.org/pcas/logger/monitor/monitord"
	"bitbucket.org/pcas/sslflag"
	"bitbucket.org/pcastools/address"
	"bitbucket.org/pcastools/file"
	"bitbucket.org/pcastools/flag"
	"bitbucket.org/pcastools/version"
	"fmt"
	"os"
	"path/filepath"
)

// Options describes the options.
type Options struct {
	Args     []string // The args to be passed to Lua
	Rc       string   // The rc file to load
	NoBanner bool     // Suppress welcome and goodbye
	NoColor  bool     // Suppress colourised output
}

// Name is the name of the executable.
const Name = "pcas-lua"

// defaultRc is the default rc file name.
const defaultRc = ".pcas-luarc"

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// setOptions returns the parsed and validated configuration information and command-line arguments.
func setOptions() *Options {
	// Create the default values
	opts := defaultOptions()
	// Parse the configuration information
	parseArgs(opts)
	return opts
}

// defaultOptions returns a new Options struct initialised to the default values.
func defaultOptions() *Options {
	// Set the rc default file
	var rcpath string
	if dir, err := os.UserHomeDir(); err == nil {
		path := filepath.Join(dir, defaultRc)
		if ok, err := file.IsRegular(path); err == nil && ok {
			rcpath = path
		} else if ok, err := file.IsRegular(path + ".lua"); err == nil && ok {
			rcpath = path + ".lua"
		}

	}
	// Return the default options
	return &Options{
		Rc: rcpath,
	}
}

// makeOptionsSet registers, to the standard flag set, a set of command-line flags to configure Options.
func makeOptionsSet(opts *Options) {
	var norc bool
	// prepare some usage messages
	rcfileMessage := fmt.Sprintf("Execute commands from the given file instead of ~/%s", defaultRc)
	norcMessage := fmt.Sprintf("Do not read and execute the ~/%s file", defaultRc)
	// define the command-line flags
	flag.SetName("Options")
	flag.Add(
		boolFlagNoDefault("nocolor", &opts.NoColor, opts.NoColor, "Do not colourise output in an interactive session", ""),
		boolFlagNoDefault("norc", &norc, norc, norcMessage, ""),
		boolFlagNoDefault("b", &opts.NoBanner, opts.NoBanner, "Suppress the opening banner and all other introductory messages", "If the opening banner is suppressed using the flag -b then the final \"total time\" message is also suppressed."),
		stringFlagNoDefault("rcfile", &opts.Rc, opts.Rc, rcfileMessage, ""),
		&version.Flag{AppName: Name},
	)
	// install a validate function (for the standard flag set) that suppresses reading the rc file if -norc is present
	flag.SetValidate(func() error {
		if norc {
			opts.Rc = ""
		}
		return nil
	})
}

// addressEnvUsage returns a standard usage message for the address flag -f that reads its default value from the environment variable env.
func addressEnvUsage(f string, env string) string {
	var status string
	// read the environment variable and try to parse it
	val, ok := os.LookupEnv(env)
	if ok {
		_, err := address.New(val)
		if err == nil {
			status = fmt.Sprintf("current value \"%s\", which is valid", val)
		} else {
			status = fmt.Sprintf("current value \"%s\", which is invalid", val)
		}
	} else {
		status = "currently unset"
	}
	return fmt.Sprintf("The default value of the flag -%s can be specified using the environment variable %s (%s).", f, env, status)
}

// servicesSet creates and returns the services set.
func servicesSet() flag.Set {
	// Create the standard SSL flag
	sslFlag := &sslflag.Flag{}
	// Fetch the default configs
	fsdc := fsd.DefaultConfig()
	kvdbc := kvdb.DefaultConfig()
	logdc := logd.DefaultConfig()
	monitordc := monitord.DefaultConfig()
	// Create the set
	s := flag.NewBasicSet("Service options")
	s.Add(
		address.NewFlag("fs-address", &fsdc.Address, fsdc.Address,
			"The address of the fs server",
			addressEnvUsage("fs-address", "PCAS_FS_ADDRESS"),
		),
		address.NewFlag("kvdb-address", &kvdbc.Address, kvdbc.Address,
			"The address of the kvdb server",
			addressEnvUsage("kvdb-address", "PCAS_KVDB_ADDRESS"),
		),
		address.NewFlag("log-address", &logdc.Address, logdc.Address,
			"The address of the log server",
			addressEnvUsage("log-address", "PCAS_LOG_ADDRESS"),
		),
		address.NewFlag("monitor-address", &monitordc.Address, monitordc.Address,
			"The address of the monitor server",
			addressEnvUsage("monitor-address", "PCAS_MONITOR_ADDRESS"),
		),
		sslFlag,
	)
	s.SetUsageFooter("The value of the flags -*-address should either take the form \"hostname[:port]\" or be a web-socket URI \"ws://host/path\".")
	// Update the defaults on validate
	s.SetValidate(func() error {
		// Recover the SSL certificate
		cert, err := sslFlag.Certificate()
		if err != nil {
			return err
		}
		// Update the fsd defaults
		fsdc.SSLCert = cert
		if err := fsdc.Validate(); err != nil {
			return err
		}
		fsd.SetDefaultConfig(fsdc)
		// Update the kvdb defaults
		kvdbc.AppName = Name
		kvdbc.SSLCert = cert
		if err := kvdbc.Validate(); err != nil {
			return err
		}
		kvdb.SetDefaultConfig(kvdbc)
		// Update the logd defaults
		logdc.SSLCert = cert
		if err := logdc.Validate(); err != nil {
			return err
		}
		logd.SetDefaultConfig(logdc)
		// Update the monitord defaults
		monitordc.SSLCert = cert
		if err := monitordc.Validate(); err != nil {
			return err
		}
		monitord.SetDefaultConfig(monitordc)
		// Looks good
		return nil

	})
	// Return the set
	return s
}

// argumentsSet returns a dummy set of command-line flags, with a usage message about arguments
func argumentsSet() flag.Set {
	result := flag.NewBasicSet("Arguments")
	result.SetUsageFooter(fmt.Sprintf("If arguments remain after option processing, the first argument is assumed to be the name of a file containing Lua commands; the file will be loaded and executed, and then %s will exit. The name of the file is available via osutil.args[1]; any remaining arguments are available via osutil.args[2] onwards. If the commands in the file are executed without error then %s's exit status is 0. Otherwise execution will terminate on the first error encountered, a description of the error will be printed to stderr, and %s will exit with a non-zero exit status.", Name, Name, Name))
	return result
}

// invocationSet returns a dummy set of command-line flags, with a usage message about program invocation
func invocationSet() flag.Set {
	result := flag.NewBasicSet("Invocation")
	result.SetUsageFooter(fmt.Sprintf("An interactive session is one started without non-option arguments. If non-option arguments are provided, then the session is non-interactive (see the section on Arguments). Regardless of whether the session is interactive or not, %s will first read and execute commands from the file ~/%s, if that file exists. The -rcfile option may be used to change the target file; the -norc option may be used to inhibit this behavior.", Name, defaultRc))
	return result
}

// notesSet returns a dummy set of command-line flags, with a usage message containins miscellaneous notes
func notesSet() flag.Set {
	result := flag.NewBasicSet("Notes")
	result.SetUsageFooter(fmt.Sprintf(`If the first line of a file begins with a shebang (#!) then that line will be skipped. This allows for easy scripting of %s.

If the file ~/%s is not found on start-up, and if neither the -norc or -rcfile options are set, then %s will fall back to loading and executing the file ~/%s.lua (if it exists).`, Name, defaultRc, Name, defaultRc))
	return result
}

// parseArgs parses the command-line flags.
func parseArgs(opts *Options) {
	// Define the command-line flags
	flag.SetGlobalHeader(fmt.Sprintf("%s is the pcas Lua interpreter.\n\nUsage:  %s [options] [filename [args]]", Name, Name))
	makeOptionsSet(opts)
	// Create and add the services flag set
	flag.AddSet(servicesSet())
	// Create and add the mongodb flag set
	mongodbSet := mongodbflag.NewSet(nil)
	flag.AddSet(mongodbSet)
	// Create and add the postgres flag set
	postgresSet := postgresflag.NewSet(nil)
	flag.AddSet(postgresSet)
	// Create and add the seaweed flag set
	seaweedSet := seaweedflag.NewSet(nil)
	flag.AddSet(seaweedSet)
	// Add the documentation
	flag.AddSets(
		argumentsSet(),
		invocationSet(),
		notesSet(),
	)
	// Parse the flags
	flag.Parse()
	// Are there any arguments remaining?
	opts.Args = flag.Args()
	// Set the default values
	mongodbSet.SetDefault(Name)
	postgresSet.SetDefault(Name)
	seaweedSet.SetDefault(Name)
}
