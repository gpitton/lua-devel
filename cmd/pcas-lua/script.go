// Script runs the session in non-interactive mode.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.

You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package main

import (
	rt "bitbucket.org/pcas/golua/runtime"
	"bitbucket.org/pcastools/cleanup"
	"bitbucket.org/pcastools/log"
	"bytes"
	"context"
	"io/ioutil"
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// loadAndExecFile loads and executes the given file in the given Lua runtime.
func loadAndExecFile(r *rt.Runtime, path string) error {
	// Read in the source
	source, err := ioutil.ReadFile(path)
	if err != nil {
		return err
	}
	// If the first line of the source begins with a #!, remove it
	if bytes.HasPrefix(source, []byte("#!")) {
		if idx := bytes.IndexByte(source, '\n'); idx != -1 {
			source = source[idx:]
		}
	}
	// Compile and load the source into the runtime
	clos, err := rt.CompileAndLoadLuaChunk(path, source, r.GlobalEnv())
	if err != nil {
		return err
	}
	// Run the closure
	if err := rt.Call(r.MainThread(), clos, nil, rt.NewTerminationWith(0, true)); err != nil {
		return err
	}
	return nil
}

// runNonInteractive runs Lua in non-interactive mode. It executes the file at path given by opts.Args[0] in the given vm. Assumes that opts.Args[0] is set.
func runNonInteractive(vm *rt.Runtime, opts *Options) error {
	// Make a context to cancel everything
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	// Install the signal handler
	cleanup.CancelOnSignal(cancel, log.Stderr)
	// Send an interrupt to the runtime when the context fires
	go func() {
		<-ctx.Done()
		rt.Interrupt(vm)
	}()
	// Load and execute the rc file, if any
	if len(opts.Rc) != 0 {
		if err := loadAndExecFile(vm, opts.Rc); err != nil {
			return err
		}
	}
	// Load and execute the script file
	return loadAndExecFile(vm, opts.Args[0])
}
